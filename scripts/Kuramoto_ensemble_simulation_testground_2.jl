"""
Testground, too.
Simulation of dynamics on networks generated with Kuramoto_ensemble_generation.jl
"""
#
using FileIO
using JLD
using Distributed
# using OrdinaryDiffEq
# using PowerDynamics

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3


topology_data = JLD.load(joinpath(ensemble_path,"topology_data.jld"))
generation_parameters = JLD.load(joinpath(ensemble_path,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 10
n_initial_ω = 11
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -10.
ω_max_incl = 10.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

node_idx_range = 74:75 # 1:N in the end

# can't put "\\" or "/" in front of the filename here

# start with one grid
grid_idx = 1
pg, RHS, op = find_power_grid_path(ensemble_path, 1)

# dimensions: node perturbed, dϕ, dω, node_idx (of node with property, e.g. mean ω); make one list per grid per statistic
# mean_ωs = Vector{Vector{Vector{Vector{Float64}}}}(undef, N)
# std_ωs = Vector{Vector{Vector{Vector{Float64}}}}(undef, N)
mean_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))
std_ωs = Array{Float64}(undef, (N, n_initial_ϕ, n_initial_ω, N))

plot_idxs = collect(1:N)

# @distributed
@time for (dϕ_idx,  dω_idx, node_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    dϕ = perturbation_range_ϕ[dϕ_idx]
    dω = perturbation_range_ω[dω_idx] + 0.5 * pg.nodes[node_idx].P / pg.nodes[node_idx].D # this is SwingEqLVS specific!
    solution = simulate_from_perturbation_var(pg, op, [dϕ, dω], single_node_perturbation_ϕ_ω, node_idx, t_sim, abstol=abstol, reltol=reltol)
    pg_solution = PowerGridSolution(solution, pg)
    myplot = Plots.plot(pg_solution, plot_idxs, :ω, title="node $node_idx, dω=$(round(dω)), dϕ=$(round(dϕ, sigdigits=3))", legend=false)
    display(myplot)
    mean_ωs[node_idx, dϕ_idx, dω_idx, :], std_ωs[node_idx, dϕ_idx, dω_idx, :] = calculate_statistics_ω(solution, t_stat_iter)
end


# careful with what is for all grids and for one grid
# nodes 74 and 75 of grid_1 have been simulated
# weak dependance on dϕ (only at higher or lower dω then ω_solitary)
# save factor 10 by only looking at one perturbation in ϕ (close to pi seems to be good)
# make phase space plot (better for 2 osc system first)
where_dense_sprouts[grid_idx]
mean_ωs[74]

mean_ωs

JLD.save(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*".jld"), "mean_ωs", mean_ωs, "std_ωs", std_ωs)
# abstol= 1e-6, reltol = 1e-6, t_sim = (0. 300.), t_stat_iter for 100. seconds in 997 steps:        480 s for 8 calculations -> 60 s for one
# abstol= 1e-3, reltol = 1e-3, t_sim = (0. 200.), t_stat_iter for 20. seconds w/o specified steps:  56 s for 8 -> 8 s for one (most difference comes from the tolerances)
# 100 * 10 * 11 initial conditions take this amount of hours:
8 * 100 * 10 * 11 / 60/ 60
# 15 min for one node ?
# 4905 s ~ 80 min with plots (w/o saving plots)

JLD.save(joinpath(data_path, "simulation_parameters_grid_"*string(grid_idx)*".jld"),
    "perturbation_range_ϕ", perturbation_range_ϕ, "perturbation_range_ω", perturbation_range_ω,
    "t_sim", t_sim, "t_stat_iter", t_stat_iter, "node_idx_range", node_idx_range, "abstol", abstol, "reltol", reltol,
    "mean_ωs", mean_ωs, "std_ωs", std_ωs)

