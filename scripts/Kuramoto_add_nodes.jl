"""
Work in progress.
Tune root degree by adding nodes to the network.
"""
#
using Random
using TreeNodeClassification
using FileIO
using JLD
using StatsBase
using Plots

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path_origin = joinpath(@__DIR__,"../data/ensemble")
ensemble_path = joinpath(@__DIR__,"../data/ensemble/one_grid")
data_path_origin = "c:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/wo_distr"# joinpath(@__DIR__,"../data/")
data_path = "c:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/one_grid"# joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

topology_data = JLD.load(joinpath(ensemble_path,"topology_data.jld"))
where_dense_sprouts = topology_data["where_dense_sprouts"]
dense_root_degrees = topology_data["dense_root_degrees"]
classes = topology_data["classes"]
generation_parameters = JLD.load(joinpath(ensemble_path,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]
node_idx_range = collect(1:N)

# use homogeneous simulation parameters to circumvene savinf problem on cluster
simulation_parameters = JLD.load("c:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/simulation_parameters_"*"grid_"*string(1)*".jld")
# need to fix this!
perturbation_range_ϕ = simulation_parameters["perturbation_range_ϕ"]
perturbation_range_ω = simulation_parameters["perturbation_range_ω"]
dϕ_idx_range = eachindex(perturbation_range_ϕ)
dω_idx_range = eachindex(perturbation_range_ω)

where_one_dense_sprout = findall(i->isequal(i,1), n_dense_sprouts)
where_dense_sprouts[where_one_dense_sprout]

# N = 100
# node_type = SwingEqLVS
# H = .5
# P = 1.
# D = .1
# Ω = 1.
# Γ = 1.
# V = 1.
# node_parameters = [H, P, D, Ω, Γ, V]
# K = 6.

# # generate n_grids = M (50 in Nitzbon paper) power grids and store them
# n_grids = 100

# generation_parameters = Dict(
#     :"n_grids"              => n_grids,
#     :"N"                    => N,
#     :"node_type"            => node_type,
#     :"node_parameters"      => node_parameters,
#     :"K"                    => K
#     )
# # maybe add a unique name for an ensemble later 
# JLD.save(joinpath(ensemble_path, "generation_parameters.jld"), "generation_parameters", generation_parameters)

grid_idx = 50
node_idx = 67

n_dense_sprouts[node_idx]

origin_pg, origin_RHS, origin_op = find_power_grid_path(ensemble_path_origin, grid_idx)

tail_statistics = FileIO.load(joinpath(data_path_origin, "tail_statistics_grid_"*string(grid_idx)*"_dphi_wo_distr.jld")) 
mean_ωs = tail_statistics["mean_ωs"]
mean_ωs[node_idx,:,:,node_idx]

n_range = 6:30

for n in n_range
    # add bus
    # save
end
# simulate
# data analysis

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
@time for grid_idx in 1:n_grids
    # this makes the grid generation reproducable (tested it!)
    Random.seed!(grid_idx)
    pg, op = random_PD_grid(N, node_type, node_parameters, K; )
    write_powergrid(pg, joinpath( ensemble_path, "grid_"*string(grid_idx)*".json"), Json)
    pg_graph = graph_convert(pg.graph)
    classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
    where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
    length_dense_sprouts = length(where_dense_sprouts[grid_idx])
    n_dense_sprouts[grid_idx] = length_dense_sprouts
    where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    position = 1
    for node_idx in where_dense_sprouts[grid_idx]
        root = LightGraphs.neighbors(pg_graph, node_idx)[1]
        where_dense_roots[grid_idx][position] = root
        root_neighbors = LightGraphs.neighbors(pg_graph, root)
        dense_root_degrees[grid_idx][position] = length(root_neighbors)
        position += 1
    end
    # gplot = my_graph_plot(pg,[])
    # FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)
end

FileIO.save(joinpath(ensemble_path, "topology_data.jld"), "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots, "dense_root_degrees", dense_root_degrees)

## make histograms of the ensembles topology data

# histogram of dense root degrees

topology_data = FileIO.load(joinpath(ensemble_path, "topology_data.jld"))
dense_root_degrees = topology_data["dense_root_degrees"]
# manually:
# bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
# Plots.histogram(vcat(dense_root_degrees...), bins = bins)

# built-in (but uses manual bins from above): 
# hist_dense_root_degrees = StatsBase.fit(StatsBase.Histogram, vcat(dense_root_degrees...), bins)
# hist.edges
# hist.weights
# Plots.histogram(vcat(dense_root_degrees...), bins = hist_dense_root_degrees.edges)

dense_root_degrees = topology_data["dense_root_degrees"]
bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
Plots.histogram(vcat(dense_root_degrees...), bins = bins, legend = false);
Plots.xlabel!(L"root degree $n+1$");
Plots.ylabel!("frequency")

n_dense_sprouts = topology_data["n_dense_sprouts"]
bins = minimum(n_dense_sprouts)-0.5:maximum(n_dense_sprouts)+0.5 # careful with doubled name bins
hist_dense_sprouts = StatsBase.fit(StatsBase.Histogram, n_dense_sprouts, bins)
Plots.histogram(hist_dense_sprouts, bins = hist_dense_sprouts.edges, )
Plots.bar(hist_dense_sprouts.edges, hist_dense_sprouts.weights, x_ticks = 0:10, legend=false )
# or
bins_2 = minimum(n_dense_sprouts) : maximum(n_dense_sprouts)+1
hist_dense_sprouts_2 = StatsBase.fit(StatsBase.Histogram, n_dense_sprouts, bins_2)
hist_dense_sprouts_2.edges == bins_2 # not same but almost
hist_dense_sprouts_2.edges[1] === bins_2 # but like this!
Plots.bar(hist_dense_sprouts_2.edges[1] .- 0.5, hist_dense_sprouts_2.weights, x_ticks=bins_2[1:end-1], legend=false)
# bar_edges does not do anything?! But you can correct x data manually

mean(n_dense_sprouts)
# grid 3 is an average grid


