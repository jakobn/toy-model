"""
Generation of an ensemble of networks using synthetic_networks.jl
Plots of ensemble properties
"""
#
using Random
using TreeNodeClassification
using FileIO
using JLD
using StatsBase
using Plots
using DataStructures
# using FixedPointNumbers imported in Images afaik
using Images

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

### test generation function random_PD_grid with SwingEqLVS
N = 100
node_type = SwingEqLVS
H = .5
P = 1.
D = .1
Ω = 1.
Γ = 1.
V = 1.
node_parameters = [H, P, D, Ω, Γ, V]
K = 6.

### generate n_grids = M (50 in Nitzbon paper) power grids and store them
n_grids = 100

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters,
    :"K"                    => K
    )
# maybe add a unique name for an ensemble later 
JLD.save(joinpath(ensemble_path, "generation_parameters.jld"), "generation_parameters", generation_parameters)

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
@time for grid_idx in 1:n_grids
    # this makes the grid generation reproducable (tested it!)
    Random.seed!(grid_idx)
    pg, op = random_PD_grid(N, node_type, node_parameters, K; )
    write_powergrid(pg, joinpath( ensemble_path, "grid_"*string(grid_idx)*".json"), Json)
    pg_graph = graph_convert(pg.graph)
    classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
    where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
    length_dense_sprouts = length(where_dense_sprouts[grid_idx])
    n_dense_sprouts[grid_idx] = length_dense_sprouts
    where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    position = 1
    for node_idx in where_dense_sprouts[grid_idx]
        root = LightGraphs.neighbors(pg_graph, node_idx)[1]
        where_dense_roots[grid_idx][position] = root
        root_neighbors = LightGraphs.neighbors(pg_graph, root)
        dense_root_degrees[grid_idx][position] = length(root_neighbors)
        position += 1
    end
    # gplot = my_graph_plot(pg,[])
    # FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)
end

FileIO.save(joinpath(ensemble_path, "topology_data.jld"), "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots, "dense_root_degrees", dense_root_degrees)

### make histograms of the ensembles topology data

## histogram of dense root degrees
# now with corrected counting if several dense sprouts at one root

topology_data = FileIO.load(joinpath(ensemble_path, "topology_data_v3.jld"))
dense_root_degrees = topology_data["dense_root_degrees"]
# manually:
# bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
# Plots.histogram(vcat(dense_root_degrees...), bins = bins)

# built-in (but uses manual bins from above): 
# hist_dense_root_degrees = StatsBase.fit(StatsBase.Histogram, vcat(dense_root_degrees...), bins)
# hist.edges
# hist.weights
# Plots.histogram(vcat(dense_root_degrees...), bins = hist_dense_root_degrees.edges)

fontsize=12

dense_root_degrees = topology_data["dense_root_degrees"]
bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
plt_hist_root_degree = Plots.histogram(vcat(dense_root_degrees...),
    bins = bins, legend = false, size=(350,250),
    labelfontsize=fontsize, tickfontsize=fontsize);
Plots.xlabel!(L"root degree $n+1$");
Plots.ylabel!("frequency")

## histogram of number of dense sprouts
n_dense_sprouts = topology_data["n_dense_sprouts"]
sum(n_dense_sprouts)
bins = minimum(n_dense_sprouts)-0.5:maximum(n_dense_sprouts)+0.5 # careful with doubled name bins
hist_dense_sprouts = StatsBase.fit(StatsBase.Histogram, n_dense_sprouts, bins)
# Plots.histogram(hist_dense_sprouts, bins = hist_dense_sprouts.edges, ) #what is this?!
Plots.bar(hist_dense_sprouts.edges, hist_dense_sprouts.weights, x_ticks = 0:10, legend=false );
# or in more complicated
bins_2 = minimum(n_dense_sprouts) : maximum(n_dense_sprouts)+1
hist_dense_sprouts_2 = StatsBase.fit(StatsBase.Histogram, n_dense_sprouts, bins_2)
hist_dense_sprouts_2.edges == bins_2 # not same but almost
hist_dense_sprouts_2.edges[1] === bins_2 # but like this!
plt_hist_dense_sprouts = Plots.bar(hist_dense_sprouts_2.edges[1] .- 0.5,
    hist_dense_sprouts_2.weights, x_ticks=bins_2[1:end-1],
    legend=false, size=(350,250),
    labelfontsize=fontsize, tickfontsize=fontsize);
Plots.xlabel!("number of dense sprouts");
Plots.ylabel!("frequency")
# bar_edges does not do anything?! But you can correct x data manually

# if you want both in one figure. You have to bugfix the margins though.
plt_both = Plots.plot(plt_hist_dense_sprouts, plt_hist_root_degree,
    layout=(1,2), size=(700,250))

save("ensemble_hist_number_dense_sprouts.pdf",plt_hist_dense_sprouts, )
save("ensemble_hist_roots_degree_v2.pdf",plt_hist_root_degree)

#
mean(n_dense_sprouts)
# grid 3 is an average grid


## mean degree
using Graphs
stdevs = Vector{Float64}(undef,100)
mean_degrees = Vector{Float64}(undef,100)
for grid_idx in 1:n_grids
    pg, RHS, op = find_power_grid_path(ensemble_path, grid_idx)
    pg_graph = pg.graph
    degrees = Graphs.degree(pg_graph)
    stdevs[grid_idx] = std(degrees)
    mean_degrees[grid_idx] = mean(degrees)
end

mean_degrees
mean(mean_degrees)
std(mean_degrees)
minimum(mean_degrees)
maximum(mean_degrees)

mean(stdevs)
std(stdevs)
minimum(stdevs)
maximum(stdevs)

# bins = minimum(vcat(dense_root_degrees...))-0.5 : maximum(vcat(dense_root_degrees...))+0.5
plt_hist_degree_dist = Plots.histogram(mean_degrees, #bins = bins,
    legend = false, size=(350,250),
    labelfontsize=fontsize, tickfontsize=fontsize);
Plots.xlabel!("mean degree");
Plots.ylabel!("frequency")

save("ensemble_hist_degree_dist.pdf", plt_hist_degree_dist)




# add to data
topology_data = FileIO.load(joinpath(ensemble_path, "topology_data.jld"))
classes = topology_data["classes"]
n_dense_sprouts = topology_data["n_dense_sprouts"]
where_dense_sprouts = topology_data["where_dense_sprouts"]
where_dense_roots = topology_data["where_dense_roots"]
dense_root_degrees = topology_data["dense_root_degrees"]

FileIO.save(joinpath(ensemble_path, "topology_data_v2.jld"), "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots, "dense_root_degrees", dense_root_degrees, "mean_degrees", mean_degrees)



### correct double occurances of entries dense_root_degrees for roots with several dense sprouts (probably there is a more elegant way: calling dense_root_degrees at the unique indices from where_dense_roots)
where_dense_roots
where_dense_roots_unique = OrderedSet.(where_dense_roots)
dense_roots_degrees_unique = Vector{Vector{Int}}(undef, n_grids)

for grid_idx in 1:n_grids    
    pg, RHS, op = find_power_grid_path(ensemble_path, grid_idx)
    pg_graph = pg.graph
    degrees = Graphs.degree(pg_graph)
    length_dense_roots_unique = length(where_dense_roots_unique[grid_idx])
    dense_roots_degrees_unique[grid_idx] = Vector{Int}(undef, length_dense_roots_unique)
    position = 1
    for node_idx in where_dense_roots_unique[grid_idx]
        dense_roots_degrees_unique[grid_idx][position] = degrees[node_idx]
        position += 1
    end
end

dense_root_degrees
dense_roots_degrees_unique
#change name
dense_root_degrees_unique = dense_roots_degrees_unique

FileIO.save(joinpath(ensemble_path, "topology_data_v3.jld"), "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots, "dense_root_degrees", dense_roots_degrees, "dense_roots_degrees_unique", dense_roots_degrees_unique, "mean_degrees", mean_degrees)


# you should rather save both the dense root degrees and unique degrees!
# Make a v4
topology_data = FileIO.load(joinpath(ensemble_path, "topology_data_v2.jld"))
classes = topology_data["classes"]
n_dense_sprouts = topology_data["n_dense_sprouts"]
where_dense_sprouts = topology_data["where_dense_sprouts"]
where_dense_roots = topology_data["where_dense_roots"]
dense_root_degrees = topology_data["dense_root_degrees"]
mean_degrees = topology_data["mean_degrees"]


FileIO.save(joinpath(ensemble_path, "topology_data_v4.jld"), "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots, "dense_root_degrees", dense_root_degrees, "dense_root_degrees_unique", dense_root_degrees_unique, "mean_degrees", mean_degrees)

# Version 5 includes node marker shapes and colors
topology_data = FileIO.load(joinpath(ensemble_path, "topology_data_v4.jld"))
classes = topology_data["classes"]
n_dense_sprouts = topology_data["n_dense_sprouts"]
where_dense_sprouts = topology_data["where_dense_sprouts"]
where_dense_roots = topology_data["where_dense_roots"]
dense_root_degrees = topology_data["dense_root_degrees"]
dense_root_degrees_unique = topology_data["dense_root_degrees_unique"]
mean_degrees = topology_data["mean_degrees"]


node_colors = Vector{Any}(undef, n_grids) #fill(fill(colorant"red", N), n_grids)
node_markers = Vector{Any}(undef, n_grids) 
for grid_idx in 1:n_grids    
    pg, RHS, op = find_power_grid_path(ensemble_path, grid_idx)
    node_colors[grid_idx] = color_nodes(pg)
    node_markers[grid_idx] = power_shape_nodes(pg)
end

find_power_grid_path(ensemble_path, grid_idx)
find_power_grid_path(ensemble_path, grid_idx)[1]

node_markers_comprehension = [power_shape_nodes( find_power_grid_path(ensemble_path, grid_idx)[1] ) for grid_idx=1:n_grids]
node_colors_comprehension = [color_nodes( find_power_grid_path(ensemble_path, grid_idx)[1] ) for grid_idx=1:n_grids]

node_markers == node_markers_comprehension
node_colors == node_colors_comprehension
# equivalent and more elegant, and circumvents the problem of initialization

FileIO.save(joinpath(ensemble_path, "topology_data_v5.jld"),
    "classes", classes, "n_dense_sprouts", n_dense_sprouts,
    "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots,
    "dense_root_degrees", dense_root_degrees, "dense_root_degrees_unique", dense_root_degrees_unique,
    "mean_degrees", mean_degrees, "node_colors", node_colors_comprehension, "node_markers", node_markers_comprehension)
