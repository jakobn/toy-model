"""
Testground.
Analyze ensemble of networks generated with Kuramoto_ensemble_generation.jl
"""
#
using FileIO
using JLD
using Plots
using StatsPlots
using LaTeXStrings
using CairoMakie
using FixedPointNumbers
using Images # from now on call Makie.Axis()

include(joinpath(@__DIR__,"../src/helper_functions.jl"))

# default(grid = false, foreground_color_legend = nothing, bar_edges = false, framestyle =:box, msc = :auto, dpi=300, legendfontsize = 11, labelfontsize = 15, tickfontsize = 10)

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = "C:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/Julia/toy-model/data" # Julia is new ## joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")


#    # plot for one node and only one dϕ
grid_idx = 1 #3

topology_data = JLD.load(joinpath(ensemble_path,"topology_data_v5.jld"))
where_dense_sprouts = topology_data["where_dense_sprouts"]
dense_root_degrees = topology_data["dense_root_degrees"]
classes = topology_data["classes"]

generation_parameters = JLD.load(joinpath(ensemble_path,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

simulation_parameters = JLD.load(joinpath(data_path, "simulation_parameters_"*"grid_"*string(grid_idx)*".jld"))
# perturbation_range_ω = simulation_parameters["perturbation_range_ω"]
# n_initial_ω = length(perturbation_range_ω)
# perturbation_range_ϕ = simulation_parameters["perturbation_range_ϕ"]
# n_initial_ϕ = length(perturbation_range_ϕ)
# dϕ_idx_range = eachindex(perturbation_range_ϕ)
# dω_idx_range = eachindex(perturbation_range_ω)

# for some reason cluster does not save ranges well

n_initial_ϕ = 1
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

tail_statistics = FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*".jld")) 
mean_ωs = tail_statistics["mean_ωs"]

## scatter of all mean_ωs over node_i_idx

node_idx_range = collect(1:N)

# for all nodes perturbed , here only dense sprouts
node_k_idx_range = where_dense_sprouts[grid_idx]

mean_ωs_scatter_i = Plots.scatter([],[], legend = false, size=(1400,1000),
    axislabelfontsize=12, tickfontsize=12);
for (dϕ_idx,  dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_k_idx_range))
    Plots.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], legend=false, markersize = 3, markeralpha = 0.5, markercolor = node_k_idx)
end
xaxis!(L"$i$");
yaxis!(L"$\langle\omega_i\rangle$");
# Plots.legend()...how did this work again?
Plots.display(mean_ωs_scatter_i)

# for one node_k_idx = 74 (perturbed)

mean_ωs_scatter_74 = Plots.scatter([],[], legend = false);
for (dϕ_idx,  dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, 74))
    Plots.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], legend=false, markersize = 3, markeralpha = 0.5, markercolor = node_k_idx)
end
# Plots.legend()
Plots.display(mean_ωs_scatter_74)

# for one node_k_idx = 75 (perturbed)

mean_ωs_scatter_75 = Plots.scatter([],[], legend = false);
for (dϕ_idx,  dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, 75))
    Plots.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], legend=false, markersize = 3, markeralpha = 0.5, markercolor = node_k_idx)
end
# Plots.legend()
Plots.display(mean_ωs_scatter_75)

## different but close solitary velocities?
# was there for different dϕ (10 steps)

mean_ωs_74 = mean_ωs[74, :, :, :]
mean_ωs_74_74 = mean_ωs_74[:, :, 74]

mean_ωs_75 = mean_ωs[75, :, :, :]
# mean_ωs_75[mean_ωs_75 .> 4.5 && mean_ωs_75.< 5.]
# does not work 
# custom; better use built-in one
function in_range(value, lower, upper)
    is_in_range = false
    if value >= lower
        if value <= upper
            is_in_range = true
        end
    end
    return is_in_range
end

findall(i -> in_range(i, 4., 5.), mean_ωs_75)
# more elegant
findall(in(4. ..5.), mean_ωs_75)

# this is from grid 3?
# there are different solitary frequencies close to each other
perturbation_range_ϕ[3]
perturbation_range_ϕ[8]
5 + perturbation_range_ω[7]


### What are the values at unperturbed nodes?

# consider grid 100, node 1
FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(100)*".jld")) 
# apparently not undef, but some fantasy numbers (mostly very small?)
# careful to only call statistocs that you have generated








### all nodes perturbed for grid 3 where all of them have been perturbed
## scatter mean_ωs over node_k_idx
grid_idx = 3

n_initial_ϕ = 10
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

tail_statistics = FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*"_all_nodes_dphi"*".jld")) 
mean_ωs = tail_statistics["mean_ωs"]
node_colors =  topology_data["node_colors"][grid_idx]
node_markers =  topology_data["node_markers"][grid_idx]

node_idx_range = collect(1:N)
# only use one dϕ for now to not crash the machine
dϕ_idx_range = 1
fontsize = 12
size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
size_pt = 72 .* size_inches
marker_size = 4

mean_ωs_scatter_k = Figure(resolution=size_pt, fontsize=12);
# from now on call Makie.Axis()
ax = Makie.Axis(mean_ωs_scatter_k[1, 1], xlabel = L"node perturbed $l$", ylabel = "⟨ωᵢ⟩",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_i_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[:, dϕ_idx, dω_idx, node_i_idx], color=node_colors[node_i_idx], markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_k
save("mean_omegas_scatter_pert_index_l_v2.png", mean_ωs_scatter_k, px_per_unit=4)


mean_ωs_scatter_i = Figure(resolution=size_pt, fontsize=12);
# from now on call Makie.Axis()
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = "⟨ωᵢ⟩",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], color=node_colors, markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_i
save("mean_omegas_scatter_pert_index_i_v2.png", mean_ωs_scatter_i, px_per_unit=4)

"""
old Plots.jl plots
mean_ωs_scatter_k = Plots.scatter([],[], legend = false, size=(1050,750),
    tickfontsize=fontsize, axislabelfontsize=fontsize, margin=5Plots.mm);
for (dϕ_idx,  dω_idx, node_i_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Plots.scatter!(node_idx_range, mean_ωs[:, dϕ_idx, dω_idx, node_i_idx], legend=false, markersize = 3, markeralpha = 0.5, markercolor = node_i_idx)
end
xaxis!(L"$l$");
yaxis!(L"$\langle\omega_i\rangle$");
# Plots.legend()...how did this work again?
Plots.display(mean_ωs_scatter_k)
#better call it l instead of k
save("mean_omegas_scatter_pert_index_l.png", mean_ωs_scatter_k)

node_k_idx_range = node_idx_range

mean_ωs_scatter_i = Plots.scatter([],[], legend = false, size=(1050,750),
tickfontsize=fontsize, axislabelfontsize=fontsize, margin=5Plots.mm);
for (dϕ_idx,  dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_k_idx_range))
    Plots.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], legend=false, markersize = 3, markeralpha = 0.5, markercolor = node_k_idx)
end
xaxis!(L"$i$");
yaxis!(L"$\langle\omega_i\rangle$");
# Plots.legend()...how did this work again?
Plots.display(mean_ωs_scatter_i)
save("mean_omegas_scatter_pos_index_i.png", mean_ωs_scatter_i)
"""
# same for grid 1:
grid_idx = 1
n_initial_ϕ = 1
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

tail_statistics = FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*".jld")) 
mean_ωs = tail_statistics["mean_ωs"]
node_colors =  topology_data["node_colors"][grid_idx]
node_markers =  topology_data["node_markers"][grid_idx]

node_idx_range = collect(1:N)
# only use one dϕ for now to not crash the machine
dϕ_idx_range = 1
fontsize = 12
size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
size_pt = 72 .* size_inches
marker_size = 4

mean_ωs_scatter_k = Figure(resolution=size_pt, fontsize=12);
# from now on call Makie.Axis()
ax = Makie.Axis(mean_ωs_scatter_k[1, 1], xlabel = L"node perturbed $l$", ylabel = "⟨ωᵢ⟩",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_i_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[:, dϕ_idx, dω_idx, node_i_idx], color=node_colors[node_i_idx], markersize=marker_size, marker = node_markers[node_i_idx])
end
mean_ωs_scatter_k
save("grid_"*string(grid_idx)*"_mean_omegas_scatter_pert_index_l_v2.png", mean_ωs_scatter_k, px_per_unit=4)


mean_ωs_scatter_i = Figure(resolution=size_pt, fontsize=12);
# from now on call Makie.Axis()
ax = Makie.Axis(mean_ωs_scatter_i[1, 1], xlabel = L"node location $i$", ylabel = "⟨ωᵢ⟩",
    xticks=([0,10,20,30,40,50,60,70,80,90,100]), yticks=(-10:10,["-10","","","","","-5","","","","","0","","","","","5","","","","","10"]))
for (dϕ_idx, dω_idx, node_k_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, node_idx_range))
    Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :], color=node_colors, markersize=marker_size, marker = node_markers)
end
mean_ωs_scatter_i
save("grid_"*string(grid_idx)*"_mean_omegas_scatter_pert_index_i.png", mean_ωs_scatter_i, px_per_unit=4)













where_dense_sprouts

## which position does node_idx have in where_dense_sprouts (which is just a vector (n_grids) of vectors of indices)?
grid_idx # should be 1
node_idx = 74
where_dense_sprout_pos = findall(i -> isequal(node_idx, i), where_dense_sprouts[grid_idx])

# which values are there in the frequencies at node 74 when 74 is perturbed for all initial conditions)
Plots.histogram(vcat(mean_ωs_74_74...), bins = length(perturbation_range_ω))

# scatter over root degree d = n+1
data = vcat(mean_ωs[node_idx, :,:, node_idx]...)
Plots.scatter(dense_root_degrees[grid_idx][where_dense_sprout_pos] .* ones(length(data)), data)

# does perturbation in 74 influence 75? Not to solitary.
Plots.scatter(dense_root_degrees[grid_idx][where_dense_sprout_pos .+ 1], vcat(mean_ωs[node_idx, :,:, node_idx+1]...))

# for all dense sprouts in this network^(grid_idx = 1)
# note that you only consider perturbations at the same node
scatter_idx_range = [74,75,84]
bifurcation_diagram_ensemble = Plots.scatter([],[]);
for node_idx in scatter_idx_range
    where_dense_sprout_pos = findall(i -> isequal(node_idx, i), where_dense_sprouts[grid_idx])
    data = abs.( vcat(mean_ωs[node_idx, :,:, node_idx]...) )
    Plots.scatter!(dense_root_degrees[grid_idx][where_dense_sprout_pos] .* ones(length(data)), data, legend=false)
end
xaxis!(L"root degree $n+1$")
yaxis!(L"$|\langle\omega\rangle|$")


dense_root_degrees[grid_idx][where_dense_sprout_pos]

Plots.scatter([1,2],[1,0.5,3], legend=false) # third point ignored -> you need to multiply the roots degree with a ones vector of according length to scatter all frequencies

## plot preparations for scatter plots

size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
# half the size does not work, looks stupid
size_pt = 72 .* size_inches

# Can't make alpha to small or it will round to zero when cast to RGBA.
# alpha = 2. / N > 0.02 ? 2. / N : 0.02
alpha = 1.
color = (:black, alpha)
marker_size = 2

node_idx_range = 1:N

grid_idx = 1

tail_statistics = JLD.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*".jld"))
mean_ωs = tail_statistics["mean_ωs"]

## scatter mean_ωs over node stat

f = Figure(resolution=size_pt, fontsize=12);
ax = Axis(f[1, 1], xlabel = "i", ylabel = "⟨ωᵢ⟩")

@time for (node_k_idx, dϕ_idx, dω_idx) in collect(Iterators.product(node_idx_range, 1:n_initial_ϕ, 1:n_initial_ω))
  Makie.scatter!(node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :]; color = color, markersize = marker_size)
  print("\r $node_k_idx")
end

f

# almost always: node perturbed is node where solitary ends up

## find solitaries and compare to other decoupled states
threshold = 1
desynchronized_ωs_bool = abs.(mean_ωs) .> threshold
# how many desynchronized nodes are there for each desynchronized state
how_many_desynchronized_ωs = sum(desynchronized_ωs_bool, dims=4)

Plots.histogram([how_many_desynchronized_ωs...])
# only ever solitaries or  duetts (here), but solitaries dominate -> focus on those (or plot all freqs anyway, but don't bother distinguishing)


# out of 100 x 10 perturbations (nodes x n-n_initial_ω), we have 117 desync states
# 111 solitaries, 6 duetts 
# 
where_desynchronized = findall(i -> (i>0), desynchronized_ωs_bool)
where_solitaries = findall(i -> isequal(1, i), how_many_desynchronized_ωs)
where_other = findall(i -> >(i, 1), how_many_desynchronized_ωs)
where_duetts = findall(i -> i==2, how_many_desynchronized_ωs)


# when is node perturbed = solitary node? 
# 1 of the occasions must be the other nodes in the duett
# (the one not being perturbed)
desynchronized_node_k_idxs = index_vectors(where_desynchronized)[1]
desynchronized_node_i_idxs = index_vectors(where_desynchronized)[4]
desynchronized_node_i_idxs .== desynchronized_node_k_idxs
findall(x->x==1, desynchronized_node_i_idxs .== desynchronized_node_k_idxs)
findall(x->x!=1, desynchronized_node_i_idxs .== desynchronized_node_k_idxs)
findall(x->(x[1]==x[4]), where_desynchronized) # same info
where_k_is_i = where_desynchronized[findall(x->(x[1]==x[4]), where_desynchronized) ]
where_k_ne_i = where_desynchronized[findall(x->(x[1]!=x[4]), where_desynchronized) ]
classes[grid_idx][index_vectors(where_k_is_i)[1]]
classes[grid_idx][index_vectors(where_k_ne_i)[1]]
classes[grid_idx][index_vectors(where_k_ne_i)[4]]
# the ones that are not from duetts are three pairs where k=46 (Inner Tree) -> i=97 (Porper Leaf)   

sort(index_vectors(vcat(where_duett_ωs...))[4])
sort(index_vectors(vcat(where_solitaries...))[1])
findall(index_vectors)

where_duett_ωs = conditional_cartesian_idx_vector(where_desynchronized, where_duetts, 1:3)
vcat(where_duett_ωs...)
index_vectors(vcat(where_duett_ωs...))[4]
classes[grid_idx][index_vectors(vcat(where_duett_ωs...))[4]]
# the duetts are either pairs of Inner Tree and Proper Leaf or Root and Sparse Sprout

# seems like duetts appear if you don't perturb dense sprouts. What are the node types?
print([tup[k] for tup in where_duetts, k in 1:3])
# ??? cartesian_idx_vector, stats_array, condition_cartesian_idx_vector, condition_range
index_vectors(where_duetts)
# one of the duett nodes is always the perturbed node
classes[grid_idx][index_vectors(where_duetts)[1]]
# it's roots and inner tree nodes
# the only ones with intermediate frequencies are InnerTree nodes that are perturbed

# CartesianIndex.([tup[k] for tup in where_duetts, k in 1:3])
# where_duetts = CartesianIndex.([tup[k] for tup in where_duetts, k in 1:3])
# # tuples instead of CartesianIndex, but Indexing still sucks, hence use conditional_nodes_indexer or index_vectors
# # you can do the former with Tuples.(findall(...))
# where_duetts = [tup[k] for tup in where_duetts, k in 1:4]
# view(where_duetts, :, 1:3)
# also prooves: only ever solitaries or duetts (here)
# consistency and completeness:
where_duetts == where_other
sum(length(where_solitaries) + 2 * length(where_duetts)) == length(where_desynchronized)

## obtain all omegas of all nodes i where duett state and plot omegas over i
# and plot histogram
all_ωs_duetts = all_nodes_indexer(where_duetts, mean_ωs)

for node_idx_pos in 1:length(where_duetts)
    data = all_ωs_duetts[node_idx_pos,:]
    plt = Plots.scatter(node_idx_range, data)
    Plots.title!(L"$\sum_i \langle\omega_i\rangle=$ " *string(round(sum(data)),3))
    display(plt)
    hist = Plots.histogram(data, bins=100)
    display(hist)
end
# first 2 are weird: do not look like sum ω is actually zero (even though it is)
# the offset is corrected by all synchronized nodes (hard to see)
where_duetts
all_ωs_duetts[1,:] == mean_ωs[66,1,1,:]
print(all_ωs_duetts[1,:])
print(all_ωs_duetts[3,:])

# return values of all desynchronized ωs 
mean_ωs[desynchronized_ωs_bool]

# return values of desynchronized ωs of multiplicity m (here 2)
duetts_ωs = conditional_nodes_indexer(where_desynchronized, mean_ωs, where_duetts, 1:3)

solitaries_ωs = conditional_nodes_indexer(where_desynchronized, mean_ωs, where_solitaries, 1:3)

Plots.histogram(duetts_ωs[1:3], bins = 20, legend=false, bar_position=:stack)

Plots.histogram(duetts_ωs, bins = 20, bar_position=:stack)
# bar position :stack does not work?!
StatsPlots.histogram([[1,1,1,2,3,6,8,8,9],[1,3,3,3,7,9],[1.1,3.1]], bins=9, bar_position=:stack)
# also with StatsPlots ?!
Plots.histogram(vcat(duetts_ωs...), bins = 20, legend=false) # careful: each point is its own data set if not flattened
Plots.histogram(vcat(solitaries_ωs...), bins = 20, legend=false)

# scatter duetts ωs as pairs
scatter_duetts_ωs = mapreduce(permutedims, vcat, duetts_ωs)
Plots.scatter(scatter_duetts_ωs[:,1], scatter_duetts_ωs[:,2])

mean_ωs[where_duetts]

## check sum ω convergence
sum_ωs = sum(mean_ωs, dims=4)
Plots.histogram([sum_ωs...])

## plot node_perturbed node_stat mean_ω in 3D

f_3d = Figure(resolution=size_pt, fontsize=12);
ax = Axis3(f_3d[1, 1], xlabel="k", ylabel = "i", zlabel = "⟨ωᵢ⟩")
# LaTeXStrings does not work on z_axis

# only works for only one dϕ, if there is several, you need to loop that in too
@time for (node_k_idx, dϕ_idx, dω_idx) in collect(Iterators.product(node_idx_range, 1:n_initial_ϕ, 1:n_initial_ω))
  Makie.scatter!(ones(N).*node_k_idx, node_idx_range, mean_ωs[node_k_idx, dϕ_idx, dω_idx, :]; color = color, markersize = marker_size)
  print("\r $node_k_idx")
end

f_3d

FileIO.save(joinpath(plot_path, "all_mean_freq_3d_grid_"*string(grid_idx)*".png"), f_3d, px_per_unit = 5)




##### Plot grid 1
pg, RHW, op = find_power_grid(1)
gplot = my_graph_plot(pg, node_idx_range)
save("grid_1_labeled.pdf",gplot)