"""
Testground.
Simulation of dynamics on networks generated with Kuramoto_ensemble_generation.jl
"""
#
using FileIO
using JLD
using Distributed
using OrdinaryDiffEq
using PowerDynamics
using Plots


#just for some experiments
# using NetworkDynamics: ODEVertex
# using PowerDynamics
# import PowerDynamics: dimension, symbolsof, construct_vertex 
# import PowerDynamics: showdefinition
# include("c:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/normal-form/powergrids/PhaseAmpitudeOscilator.jl")
# include(joinpath(@__DIR__,"../src/phase_amplitude_oscillators.jl"))

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

topology_data = JLD.load(joinpath(ensemble_path, "topology_data.jld") )
generation_parameters = JLD.load(joinpath(ensemble_path, "generation_parameters.jld") )["generation_parameters"]
# print(generation_parameters)
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]

n_initial_ϕ = 10
n_initial_ω = 11
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -5.
ω_max_incl = 15.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

# perturbation_range_ϕ = 0. : 2*pi/10 : 2*pi
# perturbation_range_ω = -5. : 2. : 15.

# f(x,y) = [x,y]
# g(x_vec, y) = map(x -> f(x,y), x_vec)

# perturbation_range = map(y -> g(perturbation_range_ϕ, y), perturbation_range_ω)
# # index as dϕ, dω = perturbation_range[ϕ_idx][ω_idx]
# perturbation_range[6][6]

# perturbation_range = Array{Vector{Float64}}(undef, (length(perturbation_range_ϕ),length(perturbation_range_ω) ) )

# # not necessary ?
# dϕ_idx = 1
# for dϕ in perturbation_range_ϕ
#     dω_idx = 1
#     for dω in perturbation_range_ω
#         perturbation_range[dϕ_idx, dω_idx] = [dϕ, dω]
#         dω_idx += 1
#     end
#     dϕ_idx +=1
# end

# start with 1 grid

# pmap()

grid_idx = 1
pg, RHS, op = find_power_grid_path(ensemble_path, grid_idx)
where_dense_sprouts = topology_data["where_dense_sprouts"]
print(where_dense_sprouts)
dvars = [0., -5.]
dϕ, dω = dvars
node_idx = 74
state = op
# newstate_ϕ = ChangeInitialConditions(node = node_idx, var = :φ, f = Inc(dϕ))(state)

solution = simulate_from_perturbation_var(pg, op, dvars, single_node_perturbation_ϕ_ω, node_idx, (0,200))
pg_solution = PowerDynamics.PowerGridSolution(solution, pg)
Plots.plot(pg_solution, collect(1:100), :ω, legend=false)

# pg_solution(25,1,:u) # no range possible?
# solution(150:200).u

Plots.plot(pg_solution, node_idx, :ω);
Plots.plot!(solution.t, 10 .* exp.(-0.1 .* solution.t))

ω = solution(solution.t, idxs=3:3:300).u
sum_ω = map(x -> sum(x), ω)
mean_ω =  mean(solution(150:0.1:200, idxs=3:3:300).u)

Plots.histogram(mean_ω)

Plots.plot(solution.t, sum_ω)

Plots.plot(solution.t, sum_ω, yaxis=:log10);
# Plots.plot!(0:200, 10 .* exp.(-0.1 ./ log(10) .* (0:200)) )
Plots.plot!(solution.t, 10 .* exp.(-0.1 .* (solution.t)) )

Plots.plot(solution.t, 10 .* exp.(-0.1 .* (solution.t)) .- sum_ω )

myplot = create_plot(pg_solution) #can't call it "plot" (reserved), call it myplot instead
# Plots.plot(ω[1])

t_stat_iter = 150:200
mean_ω, std_ω =  calculate_statistics_ω(solution, t_stat_iter)
Plots.histogram(mean_ω)
Plots.histogram(std_ω)
Plots.scatter(mean_ω, std_ω)

# plot graph with node colors an sizes indicating mean or std

solution_tol = simulate_from_perturbation_var(pg, op, [pi, 10.], single_node_perturbation_ϕ_ω, 1, (0,200), abstol = 1e-6, reltol = 1e-6)
pg_solution_tol = PowerGridSolution(solution_tol, pg)
myplot_tol = create_plot(pg_solution_tol)

solution.t
solution_tol.t
solution_tol(150.:200.).u

solution_5 = simulate(DummyPerturbation((0.,200.)), pg, newstate, (0.,200.))

kwargs = Dict(:abstol => 1e-6)

solution_5 = simulate(DummyPerturbation((0.,200.)), pg, newstate, (0.,200.); abstol=1e-6)

function test_fct2(a,b; kwargs... )
    println(a+b)
    println(kwargs[:x])
    println(kwargs)
end

test_kwargs = Dict(:x => 5)

test_fct2(1,2;x=5,y=8)

test_fct2(1,2;kwargs = test_kwargs)

println(test_kwargs[:x])

function test_fct_outer(a,b;kwargs_inner...)
    test_fct2(a,b; kwargs_inner...)
end

test_fct_outer(1,2;x=5)




newstate = single_node_perturbation_ϕ_ω(op, [0.,10.], 1)

print(op[1:3])
print(newstate[1:3])

problem = ODEProblem(RHS, newstate, (0.,100.))
solution = solve(problem, Rodas4())

pg_idx = 1
filename = joinpath("c:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/normal-form/data/Ensemble/grid_" * string(pg_idx)*".json")
pg = read_powergrid(filename, Json)
# for some reason (even though SchifferApprox can be imported from normal-from repo and is definded in this repo too)
# SchifferApprox can't be read in PowerDynamics.read_powergrid()

RHS = rhs(pg)
op = find_operationpoint(pg)
newstate = single_node_perturbation(op, [0.,10.], 1)
problem = ODEProblem(RHS, newstate, (0.,100.))
solution = solve(problem, Rodas4())
solution = solve(problem, Tsit5())

# power dynamics example code
fault1 = ChangeInitialConditions(node=1, var=:ω, f=Inc(0.2))
solution1 = simulate(fault1, pg, op, (0.,100.))
plot1 = create_plot(solution1)

fault2 = ChangeInitialConditions(node=1, var=:ω, f=Inc(0.0))
solution2 = simulate(fault2, pg, newstate, (0.,100.))
plot2 = create_plot(solution2)

fault3 = DummyPerturbation((0.,100.))
solution3 = simulate(fault3, pg, newstate, (0.,100.))
plot3 = create_plot(solution3)

fault4 = DummyPerturbation2()
solution4 = simulate(fault4, pg, newstate, (0.,100.))
plot4 = create_plot(solution4)

newstate2 = ChangeInitialConditions(1, :φ, Inc(pi))(op)

op[1,:φ]
newstate2[1,:φ]

newstate3 = ChangeInitialConditions([1,1], [:φ,:ω], [Inc(pi), Inc(10)])(op)

fault = ChangeInitialConditions()
solution = simulate(fault, pg, op, tspan)