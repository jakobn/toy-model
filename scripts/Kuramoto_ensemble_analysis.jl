"""
Analyze statistics from simulations of perturbations of
ensemble of networks generated with Kuramoto_ensemble_generation.jl
"""
#
using FileIO
using JLD
using Plots
using StatsPlots
using LaTeXStrings
using CairoMakie
using ImplicitEquations
using Makie
# using AbstractPlotting ?

include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = "C:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/Julia/toy-model/data/wo_distr" # Julia is new ## joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

topology_data = JLD.load(joinpath(ensemble_path,"topology_data.jld"))
where_dense_sprouts = topology_data["where_dense_sprouts"]
dense_root_degrees = topology_data["dense_root_degrees"]
classes = topology_data["classes"]
generation_parameters = JLD.load(joinpath(ensemble_path,"generation_parameters.jld") )["generation_parameters"]
n_grids = generation_parameters["n_grids"]
N = generation_parameters["N"]
node_idx_range = collect(1:N)

# use homogeneous simulation parameters to circumvene saving problem on cluster
simulation_parameters = JLD.load(joinpath(data_path, "simulation_parameters_"*"grid_"*string(1)*"_dphi_wo_distr.jld"))
# need to fix this!
perturbation_range_ϕ = simulation_parameters["perturbation_range_ϕ"]
perturbation_range_ω = simulation_parameters["perturbation_range_ω"]
dϕ_idx_range = eachindex(perturbation_range_ϕ)
dω_idx_range = eachindex(perturbation_range_ω)

###hotfix (copied values from original)

n_initial_ϕ = 1
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

### make bifurcation diagram of the ensemble
# now new with K_r -> K in label

# σ for sign +- decides tge branch
n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)

size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
# half the size does not work, looks stupid
size_pt = 72 .* size_inches

# Can't make alpha to small or it will round to zero when cast to RGBA.
# alpha = 2. / N > 0.02 ? 2. / N : 0.02
alpha = .2
color = (:black, alpha)
marker_size = 2

bifurcation_diagram_ensemble = Figure(resolution=size_pt, fontsize=12);
ax = Axis(bifurcation_diagram_ensemble[1, 1], xlabel = L"root degree $n+1$", ylabel = "|⟨ω⟩|",
    xticks=([6,8,10,12], ["6","8","10","12"]))

Makie.scatter!([NaN],[NaN], color=color, alpha = alpha, marker_size=marker_size, label="simulation")

for grid_idx in 1:100
    # simulation_parameters = JLD.load(joinpath(data_path, "simulation_parameters_"*"grid_"*string(grid_idx)*"_dphi_wo_distr.jld"))
    # perturbation_range_ω = simulation_parameters["perturbation_range_ω"]
    n_initial_ω = length(perturbation_range_ω)
    # perturbation_range_ϕ = simulation_parameters["perturbation_range_ϕ"]
    n_initial_ϕ = length(perturbation_range_ϕ)
    # dϕ_idx_range = eachindex(perturbation_range_ϕ)
    # dω_idx_range = eachindex(perturbation_range_ω)

    tail_statistics = FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*"_dphi_wo_distr.jld")) 
    mean_ωs = tail_statistics["mean_ωs"]

    for node_idx in where_dense_sprouts[grid_idx]
        where_dense_sprout_pos = findall(i -> isequal(node_idx, i), where_dense_sprouts[grid_idx])
        data = abs.(vcat(mean_ωs[node_idx, :,:, node_idx]...))
        Makie.scatter!(dense_root_degrees[grid_idx][where_dense_sprout_pos] .* ones(length(data)),
            data, color=color, alpha=alpha, marker_size=marker_size,)
        # Plots.scatter!(dense_root_degrees[grid_idx][where_dense_sprout_pos] .* ones(length(data)), data, marker_color=:black, alpha=alpha, marker_size=marker_size)
    end
end

# Plots.plot!(Eq(Ze,0), xlim=(-5,30), ylim=(-1,10), label=L"$Z=0$", linecolor=:red, fillcolor=:red)
v_range = 0.01:0.01:9.99
Makie.lines!(n_self(.1, 10., 6., v_range, 1) .+1, v_range, label="ω₊")
Makie.lines!(n_self(.1, 10., 6., v_range, -1) .+1, v_range, label="ω₋")
d_min = minimum(vcat(dense_root_degrees...)) -.5
d_max = maximum(vcat(dense_root_degrees...)) +.5
Makie.xlims!(ax, [d_min, d_max])
n_range_fine = 0:0.01:d_max
K=6.
Ω = 10.
Makie.lines!(n_range_fine.+1, sqrt.(K .*n_range_fine),
    label=L"\sqrt{K n}") # L"\sqrt{K_r n}"
Makie.lines!(n_range_fine, Ω.* ones(length(n_range_fine)),
    label=L"$|\Omega|$") #L"$\Omega_s$" ##, linecolor=:blue
# Makie.xticks!(bifurcation_diagram_ensemble,xtickrange=6:2:12)
# xtickrange(ax.scene)
# bifurcation_diagram_ensemble.scene
# ax.scene
leg = Legend(bifurcation_diagram_ensemble[1,2], ax)

display(bifurcation_diagram_ensemble)
# bifurcation_diagram_ensemble

FileIO.save(joinpath(plot_path, "bifurcation_diagram_ensemble.png") ,bifurcation_diagram_ensemble, px_per_unit = 8) #larger ## 8 good for poster I think, but thesis can be smaller!
FileIO.save(joinpath(plot_path, "bifurcation_diagram_ensemble_v3.pdf") ,bifurcation_diagram_ensemble)

# grid 3... have entries where they should not have!!!
# distributed must have broken something!!! -> stopped using it (it's the undef initialization actually)

grid_idx = 2
tail_statistics = FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*".jld")) 
mean_ωs = tail_statistics["mean_ωs"]
where_dense_sprouts[grid_idx]
node_idx = 27
where_dense_sprout_pos = findall(i -> isequal(node_idx, i), where_dense_sprouts[grid_idx])
dense_root_degrees[grid_idx][where_dense_sprout_pos] .* ones(length(data))
data = abs.(vcat(mean_ωs[node_idx, :,:, node_idx]...))
Plots.scatter(dense_root_degrees[grid_idx][where_dense_sprout_pos] .* ones(length(data)), data, legend=false)

mean_ωs[1,1,1,:]

mean_ωs[27,:,:,27]

### dϕ simulations with grid  (dense sprouts only so far)
grid_idx = 3
simulation_parameters = JLD.load(joinpath(data_path, "simulation_parameters_"*"grid_"*string(grid_idx)*"_dphi.jld"))
perturbation_range_ϕ = simulation_parameters["perturbation_range_ϕ"]
perturbation_range_ω = simulation_parameters["perturbation_range_ω"]
dϕ_idx_range = eachindex(perturbation_range_ϕ)
dω_idx_range = eachindex(perturbation_range_ω)
# why does this not work?

node_k_idx_range = where_dense_sprouts[grid_idx]
node_i_idx_range = 1:N

n_initial_ϕ = 10
n_initial_ω = 10
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

tail_statistics = FileIO.load(joinpath(data_path, "tail_statistics_grid_"*string(grid_idx)*"_dphi.jld")) 
mean_ωs = tail_statistics["mean_ωs"]

f = Figure(resolution=size_pt, fontsize=12);
ax = Axis(f[1, 1], xlabel = "dϕ", ylabel = "⟨ω⟩")

@time for (node_k_idx, node_i_idx, dω_idx) in collect(Iterators.product(node_k_idx_range, node_i_idx_range, 1:n_initial_ω))
  Makie.scatter!(perturbation_range_ϕ, mean_ωs[node_k_idx, :, dω_idx, node_i_idx]; color = color, markersize = marker_size)
  print("\r $node_k_idx")
end

f

# this shows that dϕ does not really matter and can be neglected

# 1 plot for each dω (accumulated node_k and)
for dω_idx in dω_idx_range
    dω = perturbation_range_ω[dω_idx]
    f = Figure(resolution=size_pt, fontsize=12);
    title!
    ax = Axis(f[1, 1], xlabel = "dϕ", ylabel = "⟨ω⟩", title=string(round(dω; digits = 3)))
    for (node_k_idx, node_i_idx) in collect(Iterators.product(node_k_idx_range, node_i_idx_range))
        Makie.scatter!(perturbation_range_ϕ, mean_ωs[node_k_idx, :, dω_idx, node_i_idx]; color = color, markersize = marker_size)
        print("\r $node_i_idx")
    end
    display(f)
end

# 1 plot for each node_k and dω
for node_k_idx in node_k_idx_range
    for dω_idx in dω_idx_range
        dω = perturbation_range_ω[dω_idx]
        f = Figure(resolution=size_pt, fontsize=12);
        ax = Axis(f[1, 1], xlabel = "dϕ", ylabel = "⟨ω⟩", title="k=$node_k_idx, dω=$(round(dω; digits = 3))")
        for node_i_idx in node_i_idx_range
            Makie.scatter!(perturbation_range_ϕ, mean_ωs[node_k_idx, :, dω_idx, node_i_idx]; color = color, markersize = marker_size)
        end
        display(f)
    end
end
# proves that sampling of enough dω is sufficient and you don't have to bother about dϕ

# just do it once more for all nodes then it should be settled

node_k_idx_range = 1:N # rest is same


