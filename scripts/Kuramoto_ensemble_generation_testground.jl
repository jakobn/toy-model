"""
Testground.
Generation of an ensemble of networks using synthetic_networks.jl
"""
### THIS IS FULL OF BUGS

using Random
using TreeNodeClassification
using FileIO
using JLD

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble")
data_path = joinpath(@__DIR__,"../data")
plot_path = joinpath(@__DIR__,"../plots")

# test generation function random_PD_grid with SwingEqLVS
N = 100
node_type = SwingEqLVS
H = .5
P = 1.
D = .1
Ω = 1.
Γ = 1.
V = 1.
node_parameters = [H, P, D, Ω, Γ, V]
K = 6.
node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
SwingEqLVS(;node_parameters_dict..., P=-1)

pg, op = random_PD_grid(N, node_type, node_parameters, K; )
pg.nodes[1]

# loading
pg_idx = 1
pg, RHS, op = find_power_grid_path(ensemble_path, "grid_"*string(pg_idx))
simulate_from_state(pg, op.vec, (0.,100.), abstol=1e-9)

# how to use Graphs / LightGraphs commands correctly
using LightGraphs
using Graphs
growth_parameters=[1, 1/5, 3/10, 1/3, 1/10, 0.0]
n0, p, q, r, s, u = growth_parameters
RPG = RandomPowerGrid(N, n0, p, q, r, s, u)
pg = generate_graph(RPG)
e = Graphs.edges(pg.graph)
pg_light = graph_convert(pg)
e = LightGraphs.edges(pg_light)
Graphs.nv(pg.graph)
LightGraphs.nv(pg_light)

# generate n_grids = M (in Notzbon paper) power grids and store the
n_grids = 5

generation_parameters = Dict(
    :"n_grids"              => n_grids,
    :"N"                    => N,
    :"node_type"            => node_type,
    :"node_parameters"      => node_parameters,
    :"K"                    => K
    )
# maybe add a unique name for an ensemble later 
JLD.save(ensemble_path*"generation_parameters.jld", "generation_parameters", generation_parameters)
print(generation_parameters)

pg_graph = graph_convert(pg.graph)
classes = full_node_classification(pg_graph, N, 5)
where_dense_sprouts = findall(i->i=="Dense Sprout",classes)
LightGraphs.edges(pg_graph)
vertices = LightGraphs.vertices(pg_graph)
vertices[1]
root = LightGraphs.neighbors(pg_graph, where_dense_sprouts[1])[1]
root_neighbors = LightGraphs.neighbors(pg_graph, root)
root_degree = length(root_neighbors)

for node_idx in where_dense_sprouts
    root = LightGraphs.neighbors(pg_graph, node_idx)[1]
    root_neighbors = LightGraphs.neighbors(pg_graph, root)
    root_degree = length(root_neighbors)
end

classes = Array{Any}(undef, (n_grids, N))
n_dense_sprouts = Vector{Int}(undef, n_grids)
where_dense_sprouts = Vector{Vector{Int}}(undef, n_grids)
where_dense_roots = Vector{Vector{Int}}(undef, n_grids)
dense_root_degrees = Vector{Vector{Int}}(undef, n_grids)
for grid_idx in 1:n_grids
    # this makes the grid generation reproducable (tested it!)
    Random.seed!(grid_idx)
    pg, op = random_PD_grid(N, node_type, node_parameters, K; )
    write_powergrid(pg, ensemble_path*"grid_"*string(grid_idx)*".json", Json)
    pg_graph = graph_convert(pg.graph)
    classes[grid_idx] = full_node_classification(pg_graph, N, 5) # second argument: maxiter, third argument: treshold (above a sprout is dense)
    where_dense_sprouts[grid_idx] = findall(i->i=="Dense Sprout",classes[grid_idx])
    length_dense_sprouts = length(where_dense_sprouts[grid_idx])
    n_dense_sprouts[grid_idx] = length_dense_sprouts
    where_dense_roots[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    dense_root_degrees[grid_idx] = Vector{Int}(undef, length_dense_sprouts)
    position = 1
    for node_idx in where_dense_sprouts[grid_idx]
        root = LightGraphs.neighbors(pg_graph, node_idx)[1]
        where_dense_roots[grid_idx][position] = root
        root_neighbors = LightGraphs.neighbors(pg_graph, root)
        dense_root_degrees[grid_idx][position] = length(root_neighbors)
        position += 1
    end
    # gplot = my_graph_plot(pg,[])
    # FileIO.save(plot_path*"grid_"*string(grid_idx)*".pdf", gplot)
end

grid_idx = 1
pg_1, _ = find_power_grid_path(ensemble_path, 1)
where_dense_sprouts[grid_idx]
for node_idx in where_dense_sprouts[grid_idx]
    println(node_idx)
end
node_idx = where_dense_sprouts[grid_idx][1]
pg_graph_1 = graph_convert(pg_1.graph)
root = LightGraphs.neighbors(pg_graph_1, node_idx)[1]
root_neighbors = LightGraphs.neighbors(pg_graph_1, root)


my_graph_plot(pg_1, [30, 74])
my_graph_plot(pg_1, vcat(where_dense_roots[grid_idx], where_dense_sprouts[grid_idx]))

# important to save everything in one command if they should be in one file
# JLD or FileIO does not really matter

# JLD.save(ensemble_path*"topology_data.jld", "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "dense_root_degrees",dense_root_degrees)
# topology_data = JLD.load(ensemble_path*"topology_data.jld")
# classes = JLD.load(ensemble_path*"topology_data.jld")["classes"]
# classes = topology_data[:"classes"]



FileIO.save(ensemble_path*"topology_data.jld", "classes", classes, "n_dense_sprouts", n_dense_sprouts, "where_dense_sprouts", where_dense_sprouts, "where_dense_roots", where_dense_roots, "dense_root_degrees", dense_root_degrees)
topology_data = FileIO.load(ensemble_path*"topology_data.jld")
# classes = FileIO.load(ensemble_path*"topology_data.jld")["classes"]
classes = topology_data[:"classes"]
n_dense_sprouts = topology_data[:"n_dense_sprouts"]
where_dense_sprouts = topology_data[:"where_dense_sprouts"]
where_dense_roots = topology_data[:"where_dense_roots"]
dense_root_degrees = topology_data[:"dense_root_degrees"]

println(n_dense_sprouts)
println(where_dense_sprouts)
println(where_dense_roots)
println(dense_root_degrees)

# gplots = Vector{Any}(undef, n_grids)
for grid_idx in 1:n_grids
    pg, RHS, op = find_power_grid_path(ensemble_path, grid_idx)
    gplot = my_graph_plot(pg, vcat(where_dense_roots[grid_idx], where_dense_sprouts[grid_idx]))
    display(gplot)
    # gplots[grid_idx] = my_graph_plot(pg, where_dense_sprouts[grid_idx])
end


grid_name = "grid_1"
filename = joinpath(ensemble_path, grid_name * ".json")

joinpath("path/", grid_name * ".json")
joinpath("path", grid_name * ".json")




#####

###
# function that sets color = node class 
function color_nodes(pg::PowerGrid)
    N = length(pg.nodes)
    pg_graph = graph_convert(pg.graph)
    classes =  full_node_classification(pg_graph, 100, 5) # last argument is "<=" threshold for sparse sprouts
    pa = palette(:tab10)
    node_color = fill(colorant"red", N)
    for i in 1:N
        class = classes[i]
        if class == "Bulk"
            node_color[i] = pa[8]
        elseif class == "Root"
            node_color[i] = pa[6]
        elseif class == "Inner Tree Node"
            node_color[i] = pa[3]
        elseif class == "Proper Leave"
            node_color[i] = pa[9]
        elseif class == "Sparse Sprout"
            node_color[i] = pa[10]
        elseif class == "Dense Sprout"
            node_color[i] = pa[1]
        end 
    end
    node_color
end

# testing
pg, RHS, op = find_power_grid(1)
color_nodes(pg)

### function that sets marker according to generator/consumer
function power_shape_nodes(pg::PowerGrid)
    N = length(pg.nodes)
    node_marker = fill(:utriangle, N)
    for i in 1:N
        if pg.nodes[i].P < 0 # consumer
            node_marker[i] = :rect
        elseif pg.nodes[i].P > 0 # generator
            node_marker[i] = :circle
        end
    end
    node_marker
end

# testing
power_shape_nodes(pg)