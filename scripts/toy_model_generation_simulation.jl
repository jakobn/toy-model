"""
Work in progress.
Generate toy model graphs in PowerDynamics.jl for normal form simulations.
"""
#
using Random
using PowerDynamics
using SyntheticNetworks
using LightGraphs
using LinearAlgebra
using DataFrames
using TreeNodeClassification
using FileIO
using JLD

import PowerDynamics: make_generator!, _make_generator_header
import PowerDynamics: make_bus_ac!, _make_bus_ac_header

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

"""
two_node_toy_model_Schiffer_full

Generates the PowerDynamics.PowerGrid object for the 2-node toy model with full equations.
n is implemented as coupling strength to the Slack node in order to allow for non-integer values.

"""

function two_node_toy_model_Schiffer_full(P, α, K, n, m1, m2, Γ, K_Q)
    K_P = 1/α
    τ_P1 = m1/α
    τ_P2 = m2/α
    τ_Q = 1/Γ
    V_r = 1
    P1 = P
    P2 = -P

    # lines = Array{Any}(undef, 2)...

    nodes = Array([
        PVAlgebraic(P = P1, V = 1.0),
        PVAlgebraic(P = P2, V = 1.0),
        SlackAlgebraic(U = complex(1.0))
    ])

    lines = Array([
        StaticLine(from = 1, to = 2, Y = -K*1im),
        StaticLine(from = 2, to = 3, Y = -n*K*1im)
    ])

    pg_cons = PowerGrid(nodes, lines)
    op_cons = find_operationpoint(pg_cons, sol_method=:rootfind)

    newnodes = Array([
        SchifferApprox(τ_P=τ_P1, τ_Q=τ_Q, K_P=K_P, K_Q=K_Q, V_r=1, P=P1, Q=op_cons[1, :q], Y_n=0),
        SchifferApprox(τ_P=τ_P2, τ_Q=τ_Q, K_P=K_P, K_Q=K_Q, V_r=1, P=P2, Q=op_cons[2, :q], Y_n=0),
        SlackAlgebraic(U = complex(1.0))
    ])

    pg = PowerGrid(newnodes, lines)
    op = find_operationpoint(pg, sol_method=:rootfind)

    #println(op[:, :u])
    #println(op[:, :p])
    #println(op[:, :q])

    return pg, op

end



P=1
α=0.1
K=6.0
n=5
m1=1
m2=1
Γ=1.0
K_Q=0.0

pg, op = two_node_toy_model_Schiffer_full(P, α, K, n, m1, m2, Γ, K_Q)
rpg = rhs(pg)

grid_idx = 0

# this is for fix n!!!

# write_powergrid(pg, joinpath( ensemble_path, "grid_"*string(grid_idx)*".json"), Json)