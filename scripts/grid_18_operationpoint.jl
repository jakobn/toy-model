"""extract operation point of grid 18, because in newer version of this package,
https://gitlab.pik-potsdam.de/jakobn/revelations,
find_operationpoint(pg, sol_method = :rootfind) produces errors (must have to do with newer package versios or so)
:nlsolve does not converge

But I know that there is a stable operationpoint. So I extract the one form here and use it as initial guess.
"""
#
using Random
using TreeNodeClassification
using FileIO
using JLD
using StatsBase
using Plots
using DataStructures
# using FixedPointNumbers imported in Images afaik
using Images

include(joinpath(@__DIR__,"../src/synthetic_networks.jl"))
include(joinpath(@__DIR__,"../src/helper_functions.jl"))

ensemble_path = joinpath(@__DIR__,"../data/ensemble/")
data_path = joinpath(@__DIR__,"../data/")
plot_path = joinpath(@__DIR__,"../plots/")

grid_idx = 18
pg, RHS, op = find_power_grid_path(ensemble_path, grid_idx)
op
op.vec
print(op.vec)

FileIO.save(joinpath(ensemble_path, "op_grid_18.jld"), "op", op)
