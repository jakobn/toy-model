#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=grid_2_all_dense_sprouts
#SBATCH --account=condynet
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err
#SBATCH --workdir=/home/jakobn/Kuramoto_ensemble_test/cluster_scripts
#SBATCH --time=00-00:15:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1

module load julia/1.7.0

julia Kuramoto_ensemble_simulation_one_grid.jl