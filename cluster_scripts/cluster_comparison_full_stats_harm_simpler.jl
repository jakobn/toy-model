using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings
using JLD
using ImplicitEquations
using FileIO
#

include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

data_path = joinpath(@__DIR__,"../data/")

α = 0.1
K = 6.0
ns = 0.:0.2:30.

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

n_initial_ϕ = 10
n_initial_ω = 100
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

savename_harm_simpler = "two_osc_harm_simpler_fullstats_ic_grid"
af_harm_simpler = asymptotic_statistics(two_osc! , ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
JLD.save(joinpath(data_path, savename_harm_simpler*".jld"), "data", af_harm_simpler)
af_harm_simpler = load(joinpath(data_path, savename_harm_simpler*".jld"))["data"]