#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=100_grids_all_dense_sprouts
#SBATCH --account=condynet
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err
#SBATCH --workdir=/home/jakobn/Kuramoto_ensemble/cluster_scripts
#SBATCH --time=00-24:00:00
#       --nodes=1
#       --ntasks-per-node=1

module load julia/1.7.0

julia Kuramoto_ensemble_simulation_100_grids_all_dense_sprouts.jl