# General Information

This repository contains code used for my master thesis "Resonant Velocity Tuning of Solitary States in Power Grids".
My name is Jakob Niehues.
A can be reached at jakobn[ät]pik-potsdam.de
The thesis was written to qualify for a Physics MSc degree at Humboldt University of Berlin and supervised by Dr Mehrnaz Anvari, Prof Dr Dr hc mult Jürgen Kurths and Dr Frank Hellmann from the Potsdam Institute for Climate Impact Research (PIK), and Dr Rico Berner and Dr Serhiy Yanchuk from Technische Universität Berlin and Humboldt University of Berlin.

The repository contains code for numerical simulation and plotting.
Some of it was used to gain intuition about the problems at hand, some was used to generate the figures.

# General Remarks

The file names indicate what was carried out during their execution.
Most files start with docstrings that state the exact purpose and contents.
If the docstrings start with "work in progress" the files is still in this state.

Files that end with "testground" were used for testing and building functions and understanding the syntax. The code was kept for the case I was searching for something I had tried previously.
It has no other relevance and should not be used.

Files ending with "tools" just provide useful functions used in other files.

# Contents by Folder

The following subsections are named after folders in the repository and list their contents.

- cluster_scripts:
This folder contains scripts for simulations of complex networks of coupled oscillators and the toy model on the PIK cluster.

- data: This folder contains simulation data and synthetically generated power grids.

- plots: This folder contains plots.

- scripts: Scripts for the ensemble generation, analysis and simulation.

- src: All other code.

# Contents by Topic

In the following I list the most important files.

## Toy Model

- Exploration of parameter ranges of the toy model: src/parameter_ranges.jl
- Simulation and Plotting of trajectories: src/simulation_trajectories_self_contained, src/simulation_3_osc
- Comparison of different dynamics (ode functions) and approximations: src/comparison_ode_functions
- Comparison of theoretical predictions and statistics of numerical simulations: src/comparison_statistics_numerics_analytics
- Fun animations: src/animations.jl
- Plots of self-consistency equation and comparison to simulation data: src/self-consistency.jl

## Complex Networks

For generation and simulation see folders scripts and cluster_scripts.

For self-consistency see src/self-consistency_network.jl.

## Other node models than Swing Equation

See https://gitlab.pik-potsdam.de/jakobn/normal-form .
