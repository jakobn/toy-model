"""
contains many different versions of the oscillators'
ode functions
"""

# might be needed
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings
using JLD
using ImplicitEquations
using FileIO
#

##
"""
function one_osc_harm_driv!(dy, y, p, t)
    α, K, n = p
    A = sqrt(K / n) / α
    v = sqrt(K * n)
    dy[1] = y[2]
    dy[2] = 1. - α * y[2] - K * sin(y[1] - A * cos(v*t))
    nothing
end
"""

##
function two_osc!(dy, y, p, t)
  α, K, n = p
  @views begin
    ϕ = y[1:2]
    ω = y[3:4]
    dϕ = dy[1:2]
    dω = dy[3:4]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
  dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2]) 
  nothing
end

##
# linearized wrt to ϕ_2
function two_osc_harm!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = -1. - α * ω[2] - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
    nothing
end

##
# linearized wrt to ϕ_2 and w/o P_2
function two_osc_harm_woP2!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = - α * ω[2] - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
    nothing
end

##
# linearized wrt to ϕ_2 and w/o P_1 and P_2
function two_osc_harm_woP12!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = - α * ω[2] - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
    nothing
end

##
# linearized wrt to ϕ_2 and w/o dampening of node 2 (α_2 = 0)
function two_osc_harm_wodamp2!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = -1. - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
    nothing
end

##
# linearized wrt to ϕ_2, w/o P2 and w/o dampening of node 2 (α_2 = 0)
function two_osc_harm_woP2_wodamp2!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
    nothing
end

##
# linearized wrt to ϕ_2, and n + cos(ϕ_1) = n
# this is the minimal toy model (you could set P_2 = 0 too but keeping it is no big effort)
function two_osc_harm_simpler!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = -1. - α * ω[2] - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
    nothing
end

# special ode functions for parameter tuning
function two_osc_harm_simpler_parameter_tuning!(dy, y, p, t)
  P, α, K, Kn,  = p
  @views begin
    ϕ = y[1:2]
    ω = y[3:4]
    dϕ = dy[1:2]
    dω = dy[3:4]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dω[1] = P - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
  dω[2] = -P - α * ω[2] - K * (-sin(ϕ[1])) - Kn * ϕ[2] 
  nothing
end

function two_osc_harm_simpler_parameter_tuning_only_K!(dy, y, p, t)
  P, α, K, n,  = p
  @views begin
    ϕ = y[1:2]
    ω = y[3:4]
    dϕ = dy[1:2]
    dω = dy[3:4]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dω[1] = P - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
  dω[2] = -P - α * ω[2] - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
  nothing
end


##
# linearized wrt to ϕ_2, osc2 is decoupled from osc 1 (no driving), P_2 = 0
function two_osc_harm_dec2_woP2!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = - α * ω[2] - K * n * ϕ[2] 
    nothing
end

##
# linearized wrt to ϕ_2, P_2 = 0, α_2 = 0, n + cos(ϕ_1) = n
function two_osc_harm_woP2_wodamp2_simpler!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
    nothing
end


##
# linearized wrt to ϕ_2, P_2 = 0, n + cos(ϕ_1) = n
function two_osc_harm_woP2_simpler!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
    dω[2] = - α * ω[2] - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
    nothing
end

# star-shaped three node system, full dynamics
function three_osc!(dy, y, p, t)
  α, K, n = p
  @views begin
    ϕ = y[1:3]
    ω = y[4:6]
    dϕ = dy[1:3]
    dω = dy[4:6]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dϕ[3] = ω[3]
  dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
  dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2] - ϕ[3])
  dω[3] = 0. - α * ω[3] - K * sin(ϕ[3] - ϕ[2])
  nothing
end

#  star-shaped three node system, linearized w.r.t. ϕ_2 and ϕ_3
function three_osc_lin!(dy, y, p, t)
α, K, n = p
@views begin
  ϕ = y[1:3]
  ω = y[4:6]
  dϕ = dy[1:3]
  dω = dy[4:6]
end
dϕ[1] = ω[1]
dϕ[2] = ω[2]
dϕ[3] = ω[3]
dω[1] = 1. - α * ω[1] - K * sin(ϕ[1]) + K * cos(ϕ[1]) * ϕ[2]
dω[2] = -1. - α * ω[2] + K * sin(ϕ[1]) - K * cos(ϕ[1]) * ϕ[2] - K * n * (ϕ[2] - ϕ[3])
dω[3] = 0. - α * ω[3] - K * (ϕ[3] - ϕ[2])
nothing
end

# star-shaped three node system, linearized w.r.t. ϕ_2 and ϕ_3,
# without nonlinear term in equation for \dot ω₂
function three_osc_lin_simpler!(dy, y, p, t)
α, K, n = p
@views begin
  ϕ = y[1:3]
  ω = y[4:6]
  dϕ = dy[1:3]
  dω = dy[4:6]
end
dϕ[1] = ω[1]
dϕ[2] = ω[2]
dϕ[3] = ω[3]
dω[1] = 1. - α * ω[1] - K * sin(ϕ[1]) + K * cos(ϕ[1]) * ϕ[2]
dω[2] = -1. - α * ω[2] + K * sin(ϕ[1]) - K * n * (ϕ[2] - ϕ[3])
dω[3] = 0. - α * ω[3] - K * (ϕ[3] - ϕ[2])
nothing
end