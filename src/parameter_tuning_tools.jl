"""
Tools for parameter_tuning.jl
Explore parameter ranges in the toy model in which the resonant solitary states appear by tuning parameters.
"""
#
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

# changed everything to match two_osc_harm_simpler_parameter_tuning!(dy, y, p, t) and similar fcts
# p = [P, α, K, Kn] for example
# markerstrokealpha = 0
# tuning of several parameters possible if safety checks are disabled manually

"""
tunepar_simulation tunes a parameter (for now only p = [P, α, K , Kn] or in general np=length(p)=3 implemented)
in steps of dp, e.g. [0, 0.0001, 0, 0]
(not tested for tuning several parameters at once (they should, however, need the same amount of steps) )
while at every step integrating ode_function for tspan, e.g. (0, 1000)
calculating statistics from tspan[2] - t_stat (e.g. 300) until tspan[2]
starting at p0, e.g. [1.0, 0.1, 6.0, 5.0] and y0, e.g. [0,0,5,0]
to p1, e.g. [1.0, 0.3, 6.0, 5.0]
the last y value is used as new initial condition

optional:
if show_steps = true: plots every integration solplot for plot_span = tspan
if save_steps_type != "": saves the plots (no imported self-written function)
with name = "2osc_adi_Kuramoto_"*string(run)*"_"*string(α)*"_"*string(K)*"_"*string(n)*save_steps_type
saving only works if plotting is enabled

# write new fct for this!
if show_stats = true: shows pl,
if save_stats= false,
if save_stats_name=""


output
p_mat: matrix of (tuned and constant) parameters for all steps of tuning
y_mat: matrix that containts y0 and all end points y of the integration, dimensions: ( (tuning_steps+1) * y),
stats: matrix that contains frequency statistics of 2 oscillator system or in general: mean(y_3), std(y3), mean(y4), std(y4)
that are obtained between tspan[2]-t_stat and tspan[2] in steps of t_stat/1000
"""
function tunepar_simulation(ode_function, y0, tspan, t_stat, p0, p1, dp; show_steps=false, save_steps_type="", solver_kwargs...) 
    @assert t_stat <= tspan[2] - tspan[1]
    np = length(p0)
    @assert (broadcast(abs, p1-(p0+dp)) .<= broadcast(abs, p1-p0)) == ones(np) "dp does not bring p0 closer to p1"
    where_dp = (dp .!= 0)
    runs_vec = broadcast( x -> ceil(Int,x), (p1[where_dp] - p0[where_dp]) ./ dp[where_dp])
    @assert runs_vec == ones(length(runs_vec))*runs_vec[1] "not all parameters require the same amount of steps of dp to reach p1"
    runs = runs_vec[1] + 1 # for the zeroth calculation
  
    p = p0
    
    y_mat = zeros(runs+1, length(y0))
    y_mat[1,:] = y0
    stats = zeros(runs, 4)
    p_mat = zeros(runs,np)
  
    t_end = tspan[2]
    t_start = t_end - t_stat
    t_step = t_stat/1000
  
    run = 1
    # delta = p1[where_dp]./dp[where_dp] * 10 * eps(Float64) # to stop p is below p1 by rounding error
    # while ( broadcast(abs, p1+2*dp-p-delta) .> broadcast(abs, dp) ) != zeros(np)
    # does not work: delta has wrong dimensions... (take min or max?)
      # +dp to be sure to overshoot target a little if (p1-p0)/dp is not a vector filled with the same int;
      # +dp to actually execute the loop after overshooting for the first time
    while run <= runs
      # print(runs)
      print("\r $run of $runs")
      # print(p)
      y = y_mat[run,:]
      prob = ODEProblem(ode_function, y, tspan, p)
      sol = solve(prob, Tsit5(); solver_kwargs...)
      y_mat[run+1,:] = sol.u[end]
   
      stats[run,:] = [mean(sol(t_start:t_step:t_end, idxs=3).u), std(sol(t_start:t_step:t_end, idxs=3).u), mean(sol(t_start:t_step:t_end, idxs=4).u), std(sol(t_start:t_step:t_end, idxs=4).u)]
      p_mat[run,:] = p
  
      if show_steps == true
        plt = solplot(sol, tspan)
        display(plt)
        if save_steps_type != ""
          P, α, K, Kn = p
          name = "2osc_adi_Kuramoto_"*string(run)*"_"*string(P)*"_"*string(α)*"_"*string(K)*"_"*string(Kn)*"."*save_steps_type
          save(name, plt)
        end
      end
       
      p += dp
      run += 1
    end
    
    p_mat, y_mat, stats
end

# in two directions
function tunepar_both_simulation(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp; show_steps=false, save_steps_type="")
  x_p_inc, y_mat_inc, stats_inc = tunepar_simulation(ode_function, y0, tspan, t_stat, p0, p1_high, dp; show_steps=show_steps, save_steps_type=save_steps_type)
  x_p_dec, y_mat_dec, stats_dec = tunepar_simulation(ode_function, y0, tspan, t_stat, p0, p1_low, -dp; show_steps=show_steps, save_steps_type=save_steps_type)
  
  x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec
end

# not adjusted to 4 params yet
# also returns statistics of the angles: std of ϕ_1 and mean and std of ϕ_2
function tunepar_angle(y0::Vector{Float64}, tspan::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1::Vector{Float64}, dp::Vector{Float64}; show_steps=false, save_steps=false, show_stats=true, save_stats= true, save_stats_name="") 
    @assert t_stat <= tspan[2] - tspan[1]
    np = length(p_0)
    @assert np == 3 "np!=3 is not implemented concerning plot axis"
    @assert (broadcast(abs, p_1-(p_0+dp)) .<= broadcast(abs, p_1-p_0)) == ones(np) "dp does not bring p_0 closer to p_1"
    where_dp = (dp .!= 0)
    runs_vec = broadcast( x -> ceil(Int,x), (p_1[where_dp] - p_0[where_dp]) ./ dp[where_dp])
    @assert runs_vec == ones(length(runs_vec))*runs_vec[1] "not all parameters require the same amount of steps of dp to reach p_1"
    runs = runs_vec[1] + 1 # for the zeroth calculation
  
    p = p_0
    
    y_mat = zeros(runs+1, length(y0))
    y_mat[1,:] = y0
    stats = zeros(runs, 6)
    p_mat = zeros(runs,np)
  
    t_end = tspan[2]
    t_start = t_end - t_stat
    t_step = t_stat/1000
  
    run = 1
    # delta = p_1[where_dp]./dp[where_dp] * 10 * eps(Float64) # to stop p is below p1 by rounding error
    # while ( broadcast(abs, p_1+2*dp-p-delta) .> broadcast(abs, dp) ) != zeros(np)
    # does not work: delta has wrong dimensions... (take min or max?)
      # +dp to be sure to overshoot target a little if (p_1-p_0)/dp is not a vector filled with the same int;
      # +dp to actually execute the loop after overshooting for the first time
    while run <= runs
      # print(runs)
      print("\r $run of $runs")
      # print(p)
      y = y_mat[run,:]
      prob = ODEProblem(two_osc_harm_simpler!, y, tspan, p)
      sol = solve(prob, Tsit5())
      y_mat[run+1,:] = sol.u[end]
   
      stats[run,:] = [mean(sol(t_start:t_step:t_end, idxs=2).u), std(sol(t_start:t_step:t_end, idxs=2).u), mean(sol(t_start:t_step:t_end, idxs=3).u), std(sol(t_start:t_step:t_end, idxs=3).u), mean(sol(t_start:t_step:t_end, idxs=4).u), std(sol(t_start:t_step:t_end, idxs=4).u)]
      p_mat[run,:] = p
  
      if show_steps == true
        plt1 = Plots.plot(sol,vars=(0,1))
        plt2 = Plots.plot(sol,vars=(0,2))
        plt3 = Plots.plot(sol,vars=(0,3))
        plt4 = Plots.plot(sol,vars=(0,4))
        plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
        if save_steps == true
          α, K, n = p
          name = "2osc_adi_harm_woP2_simpler_"*string(run)*"_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
          save(name, plt)
        end
      end
      # display(plt)
  
      p += dp
      run += 1
    end
  
    dir = ""
    ind_dp = findall(>(0), broadcast(abs,dp) )
    if np == 3 && length(ind_dp) == 1
      ind = ind_dp[1]
      symbol = ["α","K","n"][ind]
      dp_scalar = dp[ind]
      if dp_scalar > 0
        dir = "inc"
      elseif dp_scalar < 0
        dir = "dec"
      end
      
  
      x_p = p_mat[:,ind]
    else
      symbol = "sample"
      x_p = 1:runs
    end
    if show_stats == true
      stats_plot = Plots.scatter(x_p, stats, xlabel=symbol, markershape=:x, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend= :left)
  
      if save_stats_name != ""
        save_stats_name = "_"*save_stats_name
      end
  
      if save_stats == true
        save("2osc_adi_Kuramoto_stats_"*symbol*"_"*dir*"_"*string(dp_scalar)*save_stats_name*".png", stats_plot)
      end
      display(stats_plot)
    end
    # x_p, y_mat, stats
    p_mat, y_mat, stats # should be right output
  
end


# not adjusted to 4 params yet  
# also returns statistics of the angles: std of ϕ_1 and mean and std of ϕ_2
function tunepar_both_angle(y0::Vector{Float64}, tspan::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1_low::Vector{Float64}, p_1_high::Vector{Float64}, dp::Vector{Float64}; save_plot=false, legend_position=:best)
    p_mat_inc, y_mat_inc, stats_inc = tunepar_angle(y0,tspan,t_stat,p_0,p_1_high,dp, show_stats=false, show_steps=false)
    p_mat_dec, y_mat_dec, stats_dec = tunepar_angle(y0,tspan,t_stat,p_0,p_1_low,-dp, show_stats=false, show_steps=false)
    ind_dp = findall(>(0), broadcast(abs,dp) )
    np = length(dp)
    if np == 3 && length(ind_dp) == 1
      ind = ind_dp[1]
      symbol = ["α","K","n"][ind]
      dp_scalar = dp[ind]
    end
    start = p_0[ind]
    stats_plot = Plots.scatter(vcat(p_mat_inc[:,ind], p_mat_dec[:,ind]), vcat(stats_inc, stats_dec), xlabel=symbol, markershape=:x, markersize=1, labels=[L"$\langle\varphi_2\rangle$" L"$\Sigma_2$" L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=legend_position)
    vline!([start], label="start")
    display(stats_plot)
    if save_plot == true
      save("2osc_adi_harm_simpler_stats_"*symbol*"_"*string(dp_scalar)*".png", stats_plot)
    end
    #x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot
    p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot # should be right output
end



"""
tunepar_statsplot()
plots stats from tunepar_simulation()
e.g. legend_position=:none or :outerright is possible
"""
function tunepar_statsplot(dp, p_mat, stats, size_inches; legend_position=:outerright)
    np = length(dp)
    # @assert np == 3 "number of parameters np!=3 is not implemented concerning plot axis"
    p_0 = p_mat[1,:]   
    
    ind_dp = findall(>(0), broadcast(abs,dp) )
    if length(ind_dp) == 1 # && np == 3
        ind = ind_dp[1]
        symbol = ["P","α","K","Kn"][ind]
        x_p = p_mat[:,ind]
        start = p_0[ind]
        start_label = "start"
    else
        symbol = "sample"
        runs = size(p_mat)[1]
        x_p = 1:runs
        start = [NaN]
        start_label = false
    end

    fontsize = 12
    labels = [L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"]
    plt = Plots.scatter(x_p, stats,
      markershape=:x, markersize=1, # markerstrokewidth=0, markerstrokealpha=0,
      labels= labels,
      xlabel=symbol, labelfontsize=fontsize,
      xtickfontsize=fontsize, ytickfontsize = fontsize,
      legendfontsize= fontsize,
      size=100 .*size_inches, legend_position=legend_position)
    vline!([start], label=start_label)
    display(plt)
    plt 
end


function tunepar_both_statsplot(dp, p_mat_inc, p_mat_dec, stats_inc, stats_dec, size_inches; legend_position=:outerright)
  np = length(dp)
  # @assert np == 3 "number of parameters np!=3 is not implemented concerning plot axis"
  @assert p_mat_inc[1] == p_mat_dec[1] "first entry of p_mat_inc and p_mat_dec should be the same"
  p_0 = p_mat_inc[1,:]
  ind_dp = findall(>(0), broadcast(abs,dp) )
  if length(ind_dp) == 1 # && np == 3
      ind = ind_dp[1]
      symbol = ["P","α","K","Kn"][ind]
      x_p = vcat(p_mat_inc[:,ind], p_mat_dec[:,ind])
      start = p_0[ind]
      start_label = "start"
  else
      symbol = "sample"
      runs_inc = size(p_mat_inc)[1]
      runs_dec = size(p_mat_dec)[1]
      x_p = v_cat(1:runs_inc, 1:runs_dec)
      start = [NaN]
      start_label = false
  end

  fontsize = 12
  labels = [L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"]
  plt = Plots.scatter(x_p, vcat(stats_inc, stats_dec),
    markershape=:x, markersize=1, # markerstrokewidth=0, markerstrokealpha=0,
    labels= labels,
    xlabel=symbol, labelfontsize=fontsize,
    xtickfontsize=fontsize, ytickfontsize = fontsize,
    legendfontsize= fontsize,
    size=100 .*size_inches, legend_position=legend_position)
  vline!([start], label=start_label)
  display(plt)
  plt 
end

"""
statsplot_save()
saves plt from tunepar_statsplot as .save_type
with name "2osc_adi_Kuramoto_stats_"*dir*"_"*string(dp_scalar)*add_name*
"""
function statsplot_save(plt, dp, save_type; add_name="")
    np = length(dp)
    @assert np == 3 "number of parameters np!=3 is not implemented concerning plot axis"
dir = ""
ind_dp = findall(>(0), broadcast(abs,dp) )
    if length(ind_dp) == 1 # && np == 3
        ind = ind_dp[1]
        symbol = ["P","α","K","Kn"][ind]
        dp_scalar = dp[ind]
        if dp_scalar > 0
            dir = "inc"
        elseif dp_scalar < 0
            dir = "dec"
        end
    end
    save("2osc_adi_Kuramoto_stats_"*symbol*"_"*dir*"_"*string(dp_scalar)*add_name*"."*save_type, plt)
end

function statsplot_both_save(plt, dp, save_type; add_name="")
  np = length(dp)
  # @assert np == 3 "number of parameters np!=3 is not implemented concerning plot axis"
dir = ""
ind_dp = findall(>(0), broadcast(abs,dp) )
  if length(ind_dp) == 1 #  && np == 3
      ind = ind_dp[1]
      symbol = ["P","α","K","Kn"][ind]
      dp_scalar = dp[ind]
      if dp_scalar > 0
        dir = "inc"
      elseif dp_scalar < 0
        dir = "dec"
      end
  end
  save("2osc_adi_Kuramoto_stats_"*symbol*"_"*string(dp_scalar)*add_name*"."*save_type, plt)
end

"""
tunepar_statsplot_save() combines tunepar(), tunepar_statsplot() and statsplot_save()
"""
function tunepar_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1, dp, save_type, size_inches; show_steps=false, save_steps_type="", legend_position=:outerright, add_name="")
    p_mat, y_mat, stats = tunepar_simulation(ode_function, y0, tspan, t_stat, p0, p1, dp; show_steps=show_steps, save_steps_type=save_steps_type)
    plt = tunepar_statsplot(dp, p_mat, stats, size_inches, legend_position=legend_position)
    statsplot_save(plt, dp, save_type, add_name=add_name)
    p_mat, y_mat, stats, plt
end



function tunepar_both_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp, save_type, size_inches; show_steps=false, save_steps_type="", legend_position=:outerright, add_name="")
  p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec = tunepar_both_simulation(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp; show_steps=show_steps, save_steps_type=save_steps_type)
  plt = tunepar_both_statsplot(dp, p_mat_inc, p_mat_dec, stats_inc, stats_dec, size_inches, legend_position=legend_position)
  statsplot_both_save(plt, dp, save_type, add_name=add_name)
  p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, plt
end

