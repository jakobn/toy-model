"""
mostly a testground to make contour plots of implicit ImplicitEquations
scatter simulations data
"""
#
using Plots
using ImplicitEquations
using LaTeXStrings
using JLD
using CairoMakie
using FileIO



####### "old" Z vs. shifted Z in the case of new corotating frame and steady state

### start with star graph
## nomenclature
# approximate ⟨p_12⟩ in old coordinates: old
# approximate ⟨p_12⟩ in corotating coordinates: cor
# general with option to change Laplacian eienvalue by inserting gamma: Lgen
# Laplacian in corotating frame with ω_sync: Lcor ## n > |P|/K
# Laplacian in old frame: Lold
ω_sync(n,Ω) = - Ω/n # this is an approximation that lacks p_12 shift
γ(n, α, Ω, K) = sqrt(1 - (α*Ω/(K*(n+1)))^2)
ZstaroldLgen(n, α, Ω, K, v, m, γ) = α*Ω .- α*v .+ K^2/2 * (-α*v) .* (1 ./((1 .+ n) .* (α^2 *v.^2 .+ m^2 *v.^4)) .+ (n .+ 1)./((n) .* ((γ * K *(1 .+ n) .- m * v.^2).^2 .+ α^2 * v.^2))) 
ZstaroldLold(n, α, Ω, K, v, m) = ZstaroldLgen(n, α, Ω, K, v, m, 1)
ZstaroldLolde(n,v) = ZstaroldLold(n, 0.1, 10, 6, v, 1)
ZstarcorLold(n, α, Ω, K, v, m) = ZstaroldLold(n, α, Ω, K, v .- ω_sync.(n,Ω), m) .- α*ω_sync.(n,Ω)
ZstarcorLolde(n,v) = ZstarcorLold(n, 0.1, 10, 6, v, 1)
ZstaroldLcor(n, α, Ω, K, v, m) = ZstaroldLgen(n, α, Ω, K, v, m, γ(n, α, Ω, K))
ZstaroldLcore(n,v) = ZstaroldLcor(n, 0.1, 10, 6, v, 1)
ZstarcorLcor(n, α, Ω, K, v, m) = ZstaroldLcor(n, α, Ω, K, v .- ω_sync.(n,Ω), m) .- α*ω_sync.(n,Ω)
ZstarcorLcore(n,v) = ZstarcorLcor(n, 0.1, 10, 6, v, 1)

#### Plots with data
savename_full = "three_osc_star_full_fullstats_ic_grid"
af_full = load("data//"*savename_full*".jld")["data"]

savename_lin = "three_osc_lin_fullstats_ic_grid"
af_lin = load("data//"*savename_lin*".jld")["data"]

savename_lin_simpler = "three_osc_lin_simpler_fullstats_ic_grid"
af_lin_simpler = load("data//"*savename_lin_simpler*".jld")["data"]

n_initial_ϕ = 10
n_initial_ω = 100

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

alpha = 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)
color5 = (:blue, alpha)
marker1 = :circle
marker3 = :x
marker5 = :+

size1 = 6 *scaler # 9
size3 = 12 *scaler # 18
size5 = 12 *scaler

K = 6

ns = 0.:0.2:30.
d_min = ns[1] # - .5
d_max = ns[end] # + .5
vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
ns_finest = 0 : 0.03 : 30 # 0 : 0.003 : 30
ns_finest_nonzero = 1/K + 0.03 : 0.03 : 30 # 0 : 0.003 : 30


### appr old, Lold

Z_star_old_Lold_matrix = broadcast(ZstaroldLolde, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_star_old_Lold_matrix = log10.(abs.(Z_star_old_Lold_matrix))
# make heatmaps for pos and neg part separately
Z_star_old_Lold_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_old_Lold_matrix )
Z_star_old_Lold_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_old_Lold_matrix )
log_Z_star_old_Lold_matrix_pos = log10.(Z_star_old_Lold_matrix_pos)
log_Z_star_old_Lold_matrix_neg = log10.(-Z_star_old_Lold_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_old_Lold_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_old_Lold_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_correction_n+1_ylabel"*".png", f_ω, px_per_unit = 4)


### appr cor, Lold (does not make uch sense to mix, although laplacians are almost same here)


Z_star_cor_Lold_matrix = broadcast(ZstarcorLolde, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_star_cor_Lold_matrix = log10.(abs.(Z_star_cor_Lold_matrix))
# make heatmaps for pos and neg part separately
Z_star_cor_Lold_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_cor_Lold_matrix )
Z_star_cor_Lold_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_cor_Lold_matrix )
log_Z_star_cor_Lold_matrix_pos = log10.(Z_star_cor_Lold_matrix_pos)
log_Z_star_cor_Lold_matrix_neg = log10.(-Z_star_cor_Lold_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_cor_Lold_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1]);

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_cor_Lold_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_correction_n+1_ylabel"*".png", f_ω, px_per_unit = 4)



### appr old, Lcor (does not make much sense to mix)

Z_star_old_Lcor_matrix = broadcast(ZstaroldLcore, collect(ns_finest_nonzero), transpose(collect(vs_finest)))
log_Z_star_old_Lcor_matrix = log10.(abs.(Z_star_old_Lcor_matrix))
# make heatmaps for pos and neg part separately
Z_star_old_Lcor_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_old_Lcor_matrix )
Z_star_old_Lcor_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_old_Lcor_matrix )
log_Z_star_old_Lcor_matrix_pos = log10.(Z_star_old_Lcor_matrix_pos)
log_Z_star_old_Lcor_matrix_neg = log10.(-Z_star_old_Lcor_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest_nonzero, vs_finest, log_Z_star_old_Lcor_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1]);

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_old_Lcor_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_correction_n+1_ylabel"*".png", f_ω, px_per_unit = 4)



### appr cor, Lcor

Z_star_cor_Lcor_matrix = broadcast(ZstarcorLcore, collect(ns_finest_nonzero), transpose(collect(vs_finest)))
log_Z_star_cor_Lcor_matrix = log10.(abs.(Z_star_cor_Lcor_matrix))
# make heatmaps for pos and neg part separately
Z_star_cor_Lcor_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_cor_Lcor_matrix )
Z_star_cor_Lcor_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_cor_Lcor_matrix )
log_Z_star_cor_Lcor_matrix_pos = log10.(Z_star_cor_Lcor_matrix_pos)
log_Z_star_cor_Lcor_matrix_neg = log10.(-Z_star_cor_Lcor_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest_nonzero, vs_finest, log_Z_star_cor_Lcor_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1]);

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_cor_Lcor_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_correction_n+1_ylabel"*".png", f_ω, px_per_unit = 4)


###
# direct comparison of Z old Lold and cor Lcor

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
hm_neg_star_old = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_old_Lold_matrix_neg, colormap=(:gnuplot,0.8), cscale=log10);
hm_neg_star_cor = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_cor_Lcor_matrix_neg, colormap=(:viridis,0.6), cscale=log10);
Colorbar(f_ω[1,1], hm_neg_star_old, label=L"\ln (-Z_*^{-})", halign=0.85, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
Colorbar(f_ω[1,1], hm_neg_star_cor, label=L"\ln (-Z_{*,cor}^{-})", halign=0.65, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3, linestyle=:dash)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3, linestyle=:dash)
Legend(f_ω[1,1], [ω_plus, ω_minus], ["ω₊", "ω₋"], halign=0.45, valign=0.35, tellwidth=false, tellheight=false)
f_ω
save("comparison_self_consistency_star_static_vs_cor.png", f_ω, px_per_unit=4)





### all to all
ω_sync(n,Ω) = - Ω/n # this is an approximation that lacks p_12 shift
γ(n, α, Ω, K) = sqrt(1 - (α*Ω/(K*(n+1)))^2)
ZalloldLgen(n, α, Ω, K, v, m, γ) = α*Ω .- α*v .+ K^2/2 * (-α*v) .* (1 ./((1 .+ n) .* (α^2 *v.^2 .+ m^2 *v.^4)) .+ (n)./((2) .* ((γ * K *(1 .+ n) .- m * v.^2).^2 .+ α^2 * v.^2))) 
ZalloldLold(n, α, Ω, K, v, m) = ZalloldLgen(n, α, Ω, K, v, m, 1)
ZalloldLolde(n,v) = ZalloldLold(n, 0.1, 10, 6, v, 1)
ZallcorLold(n, α, Ω, K, v, m) = ZalloldLold(n, α, Ω, K, v .- ω_sync.(n,Ω), m) .- α*ω_sync.(n,Ω)
ZallcorLolde(n,v) = ZallcorLold(n, 0.1, 10, 6, v, 1)
ZalloldLcor(n, α, Ω, K, v, m) = ZalloldLgen(n, α, Ω, K, v, m, γ(n, α, Ω, K))
ZalloldLcore(n,v) = ZalloldLcor(n, 0.1, 10, 6, v, 1)
ZallcorLcor(n, α, Ω, K, v, m) = ZalloldLcor(n, α, Ω, K, v .- ω_sync.(n,Ω), m) .- α*ω_sync.(n,Ω)
ZallcorLcore(n,v) = ZallcorLcor(n, 0.1, 10, 6, v, 1)

#### Plots with data
savename_full = "three_osc_star_full_fullstats_ic_grid"
af_full = load("data//"*savename_full*".jld")["data"]

savename_lin = "three_osc_lin_fullstats_ic_grid"
af_lin = load("data//"*savename_lin*".jld")["data"]

savename_lin_simpler = "three_osc_lin_simpler_fullstats_ic_grid"
af_lin_simpler = load("data//"*savename_lin_simpler*".jld")["data"]

n_initial_ϕ = 10
n_initial_ω = 100

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

alpha = 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)
color5 = (:blue, alpha)
marker1 = :circle
marker3 = :x
marker5 = :+

size1 = 6 *scaler # 9
size3 = 12 *scaler # 18
size5 = 12 *scaler

K = 6

ns = 0.:0.2:30.
d_min = ns[1] # - .5
d_max = ns[end] # + .5
vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
ns_finest = 0 : 0.03 : 30 # 0 : 0.003 : 30
ns_finest_nonzero = 1/K + 0.03 : 0.03 : 30 # 0 : 0.003 : 30


### appr old, Lold

Z_all_old_Lold_matrix = broadcast(ZalloldLolde, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_all_old_Lold_matrix = log10.(abs.(Z_all_old_Lold_matrix))
# make heatmaps for pos and neg part separately
Z_all_old_Lold_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_old_Lold_matrix )
Z_all_old_Lold_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_old_Lold_matrix )
log_Z_all_old_Lold_matrix_pos = log10.(Z_all_old_Lold_matrix_pos)
log_Z_all_old_Lold_matrix_neg = log10.(-Z_all_old_Lold_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_old_Lold_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_all_old_Lold_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_ylabel"*".png", f_ω, px_per_unit = 4)


### appr. cor, Lcor


Z_all_cor_Lcor_matrix = broadcast(ZallcorLcore, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_all_cor_Lcor_matrix = log10.(abs.(Z_all_cor_Lcor_matrix))
# make heatmaps for pos and neg part separately
Z_all_cor_Lcor_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_cor_Lcor_matrix )
Z_all_cor_Lcor_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_cor_Lcor_matrix )
log_Z_all_cor_Lcor_matrix_pos = log10.(Z_all_cor_Lcor_matrix_pos)
log_Z_all_cor_Lcor_matrix_neg = log10.(-Z_all_cor_Lcor_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_cor_Lcor_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_all_cor_Lcor_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_ylabel"*".png", f_ω, px_per_unit = 4)








####### compare all toy models for old coordinates
# using MakieTeX does this do anything?

α = 0.1
K = 6.0
Ω = 10.0
v_range = 0.01:0.01:9.99

n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
hm_neg_star = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_old_Lold_matrix_neg, colormap=(:gnuplot,0.8), cscale=log10);
hm_neg_all = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_old_Lold_matrix_neg, colormap=(:viridis,0.6), cscale=log10);
Colorbar(f_ω[1,1], hm_neg_star, label=L"\ln (-Z_*^{-})", halign=0.85, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
Colorbar(f_ω[1,1], hm_neg_all, label=L"\ln (-Z_{all}^{-})", halign=0.65, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3, linestyle=:dash)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3, linestyle=:dash)
Legend(f_ω[1,1], [ω_plus, ω_minus], ["ω₊", "ω₋"], halign=0.45, valign=0.35, tellwidth=false, tellheight=false)
f_ω
save("comparison_self_consistency_different_models_static_v2.png", f_ω, px_per_unit=4)




### with data from three node model
# make alpha smaller
alpha = 0.01 # 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)
color5 = (:blue, alpha)


f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
hm_neg_star = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_old_Lold_matrix_neg, colormap=(:gnuplot,0.8), cscale=log10);
hm_neg_all = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_old_Lold_matrix_neg, colormap=(:viridis,0.6), cscale=log10);
Colorbar(f_ω[1,1], hm_neg_star, label=L"\ln (-Z_*^{-})", halign=0.85, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
Colorbar(f_ω[1,1], hm_neg_all, label=L"\ln (-Z_{all}^{-})", halign=0.65, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3, linestyle=:dash)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3, linestyle=:dash)

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler, ω_plus, ω_minus], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim", "ω₊", "ω₋"],
  bgcolor=false, framevisible=false,
  halign=0.47, valign=0.2, tellwidth=false, tellheight=false)
f_ω
save("comparison_self_consistency_different_models_with_data_v2.png", f_ω, px_per_unit=4)





# # old things I tried, some are wrong

# Z(n, α, Ω, K, v) = v - Ω + v * K^2 /2 /((K * n - v^2)^2 + α^2 * v^2)

# Ze(n, v) = Z(n, 0.1, 10, 6, v)

# # plt = Plots.plot(Eq(Ze,0), xlim=(-0.2,2), ylim=(-0.2,2), label=L"$Z=0$", linecolor=:red, fillcolor=:red, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")

# plt = Plots.plot(Eq(Ze,0), xlim=(-5,30), ylim=(-1,10), label=L"$Z=0$", linecolor=:red, fillcolor=:red, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")

# # e : easy (specified standard P=1, K=6, α=0.1)
# # a : α small
# # z: α = 0, P=1, K=6
# # n : 1/2 K^2 is neglected against n^2 K^2
# Zpol(n, α, P, K, v) = α*v^5 - P*v^4 + (-2*α*K*n+α^3)*v^3 + (2*K*n*P-α^2*P)*v^2 + (α*K^2*n^2+α*K^2/2)*v - P*K^2*n^2
# Zpoln(n, α, P, K,  v) =  α*v^5 - P*v^4 + (-2*α*K*n+α^3)*v^3 + (2*K*n*P-α^2*P)*v^2 + (α*K^2*n^2)*v - P*K^2*n^2
# Zpola(n, α, P, K, v) = α*v^5 - P*v^4 + (-2*α*K*n)*v^3 + (2*K*n*P)*v^2 + (α*K^2*n^2+α*K^2/2)*v - P*K^2*n^2
# Zpolan(n, α, P, K, v) = α*v^5 - P*v^4 + (-2*α*K*n)*v^3 + (2*K*n*P)*v^2 + (α*K^2*n^2)*v - P*K^2*n^2
# Zpole(n,v) = Zpol(n, 0.1, 1, 6, v)
# Zpolae(n, v) = Zpola(n, 0.1, 1, 6, v)
# Zpolz(n, v) = Zpol(n, 0, 1, 6, v)
# Zpolne(n, v) = Zpoln(n, 0.1, 1, 6, v)
# Zpolane(n, v) = Zpolan(n, 0.1, 1, 6, v)
# Zpolnz(n, v) = Zpoln(n, 0, 1, 6, v)

# #Ω = 10
# #α = 0.1
# #K = 6
# #Zpoleandir(n, v) = v^5 - Ω*v^4 + (-2*K*n)*v^3 + (2*K*n*Ω)*v^2 + (K^2*n^2)*v - Ω*K^2*n^2
# Zpoleanfac(n, v) = (v-Ω) * (v^2 - K*n)
# # mistake: square missing!
# # but yields v=Ω and v = +- sqrt(K*n)

# Zpoleanfac2(n, v) = (v-Ω) * (v^2 - K*n)^2
# # why is this not visible?!!!

# plt2 = Plots.plot(Eq(Zpoleanfac2,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. fac 2$", fillcolor=:orange, fillstyle=:x, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# Plots.plot!(Eq(Zpoleanfac,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. fac", fillcolor=:yellow, fillstyle=:x, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")

# plt = Plots.plot(Eq(Ze,0), xlim=(-5,30), ylim=(-1,10), label=L"$raw$", fillcolor=:red, fillstyle=:|, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$", size=(600,400))

# Plots.plot!(Eq(Zpole,0), xlim=(-5,30), ylim=(-1,10), label="polynomial", fillcolor=:blue, fillstyle=:-, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# Plots.plot(Eq(Zpolae,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. $\alpha \ll 1$", fillcolor=:green, fillstyle=:/, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# Plots.plot(Eq(Zpolz,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. $\alpha=0$", fillcolor=:green, fillstyle=:/, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# Plots.plot!(Eq(Zpolne,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. $n^2 \gg 1/2$", fillcolor=:black, fillstyle=:\, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# Plots.plot!(Eq(Zpolane,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. $\alpha \ll 1$ and $n^2 \gg 1/2$", fillcolor=:purple, fillstyle=:x, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# Plots.plot!(Eq(Zpolnz,0), xlim=(-5,30), ylim=(-1,10), label=L"pol. $\alpha = 0$ and $n^2 \gg 1/2$", fillcolor=:orange, fillstyle=:x, legend=true, xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")
# # only approximating α << 1 yields something indistinguishably similar to  not doing so
# # only approximating n^2 >> 1/2 yields no real roots
# # α = 0 should yield v = sqrt(K*n) if limit is taken correctly and no diverging Ω=P/α is left unchecked, but the plot is invisible - why?!
# save("self_consistency_versions.png", plt)

# # apparently you can set α=0 and don't see the change, but n^2 >> 1/2 makes problems for the contour plot... why?

# pyplot()

# a,b = -1,2
# f(x,y) = y^4 - x^4 + a*y^2 + b*x^2
# plot(f ⩵ 0)

# plot(Ze == 0)

# f(x,y) = y - Ω + y * K^2 /2 /((K * x - y^2)^2 + α^2 * y^2)
# plot(f == 0)

# α = 0.1
# Ω = 10.0
# K = 6.0
# f(x,y) = y^5 - Ω*y^4 + (-2*K*x+α^2)*y^3 + (2*K*x*Ω-α^2*Ω)*y^2 + (K^2*x^2+K^2/2)*y - Ω*K^2*x^2
# Plots.plot(f ⩵ 0, xlim=(0,30), ylim=(0,15), label="polynomial", linecolor=:red, fillcolor=:red, legend=true)
# Plots.plot!(Ze ⩵ 0, xlim=(0,30), ylim=(0,15), label="raw", linecolor=:black, fillcolor=:black, legend=true)

# # raw and polynomial produce the same graph!

# #what do you need this for?
# pyplot()
# ####


# α = 0.1
# Ω = 10.0
# K = 6.0
# ns = 0.:0.2:30.
# N_per_n = 20
# #loadname = "two_osc_harm_woP2_simpler"
# loadname = "two_osc_harm_simpler"
# # loadname = "two_osc_full_fullstats_200" then also change to ns[2:end] and af[3,2:end,:]
# af = load("data/"*loadname*".jld")["data"]

# approx(n) = sqrt(K * n)

# fontsize = 12
# size_inches = (2 *(3+3/8), 2/3 * 2* (3+3/8))
# legend_position = :bottomright
# plt = Plots.plot(approx, xlims=(0,30), ylims=(0,15),
#   label=L"$\sqrt{Kn}$", xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$",
#   legendfontsize= fontsize, labelfontsize=fontsize,
#   xtickfontsize=fontsize, ytickfontsize = fontsize,
#   size=100 .*size_inches, legend_position=legend_position);

# Plots.plot!(ns, Ω.* ones(length(ns)), label=L"$\Omega_1$", linecolor=:blue)

# alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02

# for i_N in 1:N_per_n
#   #Plots.scatter!(ns[2:end], af[3,2:end,i_N];
#   Plots.scatter!(ns, af[1,:,i_N];
#     label=false, seriesalpha = alpha, seriescolor=:black,
#     markershape=:circle, markersize=2);
# end
# Plots.scatter!(empty([]), empty([]), label="simulation",
#     seriesalpha = alpha, seriescolor=:black,
#     markershape=:circle, markersize=2);

# Z(n, α, Ω, K, v) = v - Ω + v * K^2 /2 /((K * n - v^2)^2 + α^2 * v^2)
# Ze(n, v) = Z(n, α, Ω, K, v)

# Plots.plot!(Eq(Ze, 0), xlims=(0,30), ylims=(0,15), label=L"self-consistency", linecolor=:red, fillcolor=:red, legend=legend_position)
# # ...legend=true
# # label=L"$Z=0$"

# #plt = Plots.plot(plt,  size=(700, 300))
# plt
# save("self_consistency_harm_simpler.pdf", plt)
# save("self_consistency_harm_simpler.png", plt)


# #save("self-consistency_approx_bifurcation.png", plt)

# ###

# # Re of roots if only n<<1/2 is applied
# K = 6;
# α = 0.1;
# Reroot(n) = real(sqrt(Complex( K*n-α^2/2 + sqrt(Complex( α^2*(α^2/4 - K*n)) ) ) ) )
# approx(n) = sqrt(K * n)
# plt = Plots.plot(Reroot, xlims=(0,30), ylims=(0,15), label=L"Re if $n \ll 1/2$", xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$");
# Plots.plot!(approx, xlims=(0,30), ylims=(0,15), linestyle=:dot, label=L"$\sqrt{Kn}$", xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")

# Diff(n) = Reroot(n) - approx(n)
# Plots.plot(Diff, xlims=(0,30), label=L"Diff", xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$")

# # difference between Re of complex roots when only n^2>>1/2 is applied is very small: O(10^-4) except for n<<1, where it seems to diverge towards -Inf




# ### Star network
# Zstar(n, α, Ω, K, v, m) = α*Ω .- α*v .+ K^2/2 * (-α*v) .* (1 ./((1 .+ n) .* (α^2 *v.^2 .+ m^2 *v.^4)) .+ (n .+ 1)./((n) .* ((K *(1 .+ n) .- m * v.^2).^2 .+ α^2 * v.^2))) 
# Zstare(n,v) = Zstar(n, 0.1, 10, 6, v, 1)
# # remove 0 mode
# Zstarwo0(n, α, Ω, K, v, m) = α*Ω - α*v + K^2/2 * (-α*v) * n/((1+n) * ((K *(1+n) * sqrt(1 - (α*Ω / (K*n))^2) - m * v^2)^2 + α^2 * v^2)) 
# Zstarwo0e(n,v) = Zstarwo0(n, 0.1, 10, 6, v, 1)

# data_path = "C:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/3_osc_af"
# af = JLD.load(joinpath(data_path, "3_osc_af.jld"))["af"]
# ns = 0. : 0.2 : 30.
# N_per_n = 100

# alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
# color = (:black, alpha)
# scaler = 2
# markersize = scaler*2
# fontsize = scaler *12
# size_inches = scaler.*(2* (3+3/8), 2/3 * 2 * (3+3/8))
# # half the size does not work, looks stupid
# size_pt = 72 .* size_inches
# legend_position = :right
# xlims = (0,30)
# ylims = (0,11)

# plt = Plots.scatter(1,
# xaxis=L"$n$", yaxis=L"$\langle \omega_1 \rangle$",
# xlims=xlims, ylims=ylims,
# label="simulation",
# seriesalpha = 0.2, seriescolor=:black,
# markershape=:circle, markersize=markersize,
# legendfontsize= fontsize, labelfontsize=fontsize,
# xtickfontsize=fontsize, ytickfontsize = fontsize,
# size=100 .*size_inches, legend_position=legend_position, margin=5Plots.mm); #100 .* size_inches not enough, bad resolution

# Plots.plot!(Eq(Zstare, 0), xlims=xlims, ylims=ylims, label="self-consistency", linecolor=:red, fillcolor=:red, legend=legend_position);
# # w/o 0 eigenmode the lowest branch disappears
# # Plots.plot!(Eq(Zstarwo0e, 0), xlims=xlims, ylims=ylims, label="self-consistency", linecolor=:blue, fillcolor=:blue, legend=legend_position);

# for i_N in 1:N_per_n
#   #Plots.scatter!(ns[2:end], af[3,2:end,i_N];
#   Plots.scatter!(ns, af[1,:,i_N];
#     label=false, seriesalpha = alpha, seriescolor=:black,
#     markershape=:circle, markersize=markersize);
# end
# plt
# FileIO.save("three_osc_comparison_self_consistency.png", plt)
# # for some reason, px_per_unit or pt_per_unit only work with Makie Figures.
# # large image size makes axis labels disappear beyond the edges.
# # solution: margin=5Plots.mm as kwargs in Plots.plot
# # save("three_osc_comparison_self_consistency.pdf", plt) too large
# # or Plots.png()

# Plots.plot!(Eq(Zstarwo0e, 0), xlims=xlims, ylims=ylims, label="w/o 0 mode", linecolor=:blue, fillcolor=:blue);
# Plots.plot!(Eq(Ze, 0), xlims=xlims, ylims=ylims, label="two-node toy model", linecolor=:green, fillcolor=:green, legend=:right);
# plt
# # 2 node model closer to nonlinear data!
# # only notable difference with and w/o 0 mode is presence of  0 branch


# ### 2+n network with all-to-all coupling
# Zall(n, α, Ω, K, v, m) = α*Ω - α*v + K^2/2 * (-α*v) * (1/((1+n) * (α^2 *v^2 + m^2 *v^4)) + n/((2) * ((K *(1+n) - m * v^2)^2 + α^2 * v^2))) 
# Zalle(n,v) = Zall(n, 0.1, 10, 6, v, 1)
# # remove 0 mode
# Zallwo0(n, α, Ω, K, v, m) = α*Ω - α*v + K^2/2 * (-α*v) * n/((1+n) * ((K *(1+n) * sqrt(1 - (α*Ω / (K*n))^2) - m * v^2)^2 + α^2 * v^2)) 
# Zallwo0e(n,v) = Zallwo0(n, 0.1, 10, 6, v, 1)




# ####### make plots as in comparison_ode_fcts that include different datasets and Z
# savename_full = "three_osc_star_full_fullstats_ic_grid"
# af_full = load("data//"*savename_full*".jld")["data"]

# savename_lin = "three_osc_lin_fullstats_ic_grid"
# af_lin = load("data//"*savename_lin*".jld")["data"]

# savename_lin_simpler = "three_osc_lin_simpler_fullstats_ic_grid"
# af_lin_simpler = load("data//"*savename_lin_simpler*".jld")["data"]

# size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
# scaler = 1
# size_pt = scaler .* 72 .* size_inches

# alpha = 0.02

# color1 = (:red, alpha)
# color3 = (:black, alpha)
# color5 = (:blue, alpha)
# marker1 = :circle
# marker3 = :x
# marker5 = :+

# size1 = 6 *scaler # 9
# size3 = 12 *scaler # 18
# size5 = 12 *scaler

# ns = 0.:0.2:30.
# d_min = ns[1] # - .5
# d_max = ns[end] # + .5

# vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
# ns_finest = 0 : 0.03 : 30 # 0 : 0.003 : 30
# Z_star_matrix = broadcast(Zstare, collect(ns_finest), transpose(collect(vs_finest)))
# log_Z_star_matrix = log10.(abs.(Z_star_matrix))
# # make heatmaps for pos and neg part separately
# Z_star_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_matrix )
# Z_star_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_matrix )
# log_Z_star_matrix_pos = log10.(Z_star_matrix_pos)
# log_Z_star_matrix_neg = log10.(-Z_star_matrix_neg)

# f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
# ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$")

# hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_matrix_neg, cscale=log10);
# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
# end
# mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
# end
# mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])

# Makie.xlims!(ax1, [d_min, d_max])
# Makie.ylims!(ax1, [0, 10.5])
# Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
# Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

# f_ω
# save("three_osc_star_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 8)



# ##### all to all coupling (same data)


# vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
# ns_finest = 0 : 0.03 : 30 # 0 : 0.003 : 30
# Z_all_matrix = broadcast(Zalle, collect(ns_finest), transpose(collect(vs_finest)))
# log_Z_all_matrix = log10.(abs.(Z_all_matrix))
# # make heatmaps for pos and neg part separately
# Z_all_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_matrix )
# Z_all_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_matrix )
# log_Z_all_matrix_pos = log10.(Z_all_matrix_pos)
# log_Z_all_matrix_neg = log10.(-Z_all_matrix_neg)

# f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
# ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$")

# hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_matrix_neg, cscale=log10);
# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
# end
# mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
# end
# mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])

# Makie.xlims!(ax1, [d_min, d_max])
# Makie.ylims!(ax1, [0, 10.5])
# Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
# Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

# f_ω
# save("three_osc_all_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 8)






####


# """
# ####### shifted Z in the case of new corotating frame and steady state
# # this is wrong in the Power constant!

# ### start with star graph

# ω_sync(n,Ω) = - Ω/n 
# Zstarcor(n, α, Ω, K, v, m) = α*Ω .- α .* ω_sync.(n,Ω) .- α*v .+ K^2/2 * (-α*v) .* (1 ./((1 .+ n) .* (α^2 *v.^2 .+ m^2 *v.^4)) .+ (n .+ 1)./((n) .* ((sqrt(1 - (α*Ω/(K*n))^2) * K *(1 .+ n) .- m * v.^2).^2 .+ α^2 * v.^2))) 
# Zstarorig(n, α, Ω, K, v, m) = Zstarcor(n, α, Ω, K, v .- ω_sync.(n,Ω), m) 
# Zstarorige(n,v) = Zstarorig(n, 0.1, 10, 6, v, 1)


# vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
# ns_finest_nonzero = 1/K + 0.03 : 0.03 : 30 # 0 : 0.003 : 30
# Z_star_orig_matrix = broadcast(Zstarorige, collect(ns_finest_nonzero), transpose(collect(vs_finest)))
# log_Z_star_orig_matrix = log10.(abs.(Z_star_orig_matrix))
# # make heatmaps for pos and neg part separately
# Z_star_orig_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_orig_matrix )
# Z_star_orig_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_orig_matrix )
# log_Z_star_orig_matrix_pos = log10.(Z_star_orig_matrix_pos)
# log_Z_star_orig_matrix_neg = log10.(-Z_star_orig_matrix_neg)

# f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
# ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$")

# hm_neg = Makie.heatmap!(ns_finest_nonzero, vs_finest, log_Z_star_orig_matrix_neg, cscale=log10);
# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
# end
# mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
# end
# mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])

# Makie.xlims!(ax1, [d_min, d_max])
# Makie.ylims!(ax1, [0, 10.5])
# Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
# Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

# f_ω
# save("three_osc_star_cor_orig_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 8)


# ### all to all

# Zallcor(n, α, Ω, K, v, m) = α*Ω .- α .* ω_sync.(n,Ω) .- α*v .+ K^2/2 * (-α*v) .* (1 ./((1 .+ n) .* (α^2 *v.^2 .+ m^2 *v.^4)) .+ n./((2) .* ((sqrt(1 - (α*Ω/(K*n))^2) * K *(1 .+ n) .- m * v.^2).^2 .+ α^2 * v.^2))) 
# Zallorig(n, α, Ω, K, v, m) = Zallcor(n, α, Ω, K, v .- ω_sync.(n,Ω), m) 
# Zallorige(n,v) = Zallorig(n, 0.1, 10, 6, v, 1)


# vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
# ns_finest_nonzero = 1/K + 0.03 : 0.03 : 30 # 0 : 0.003 : 30
# Z_all_orig_matrix = broadcast(Zallorige, collect(ns_finest_nonzero), transpose(collect(vs_finest)))
# log_Z_all_orig_matrix = log10.(abs.(Z_all_orig_matrix))
# # make heatmaps for pos and neg part separately
# Z_all_orig_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_orig_matrix )
# Z_all_orig_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_orig_matrix )
# log_Z_all_orig_matrix_pos = log10.(Z_all_orig_matrix_pos)
# log_Z_all_orig_matrix_neg = log10.(-Z_all_orig_matrix_neg)

# f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
# ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$")

# hm_neg = Makie.heatmap!(ns_finest_nonzero, vs_finest, log_Z_all_orig_matrix_neg, cscale=log10);
# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
# end
# mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
# end
# mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])

# Makie.xlims!(ax1, [d_min, d_max])
# Makie.ylims!(ax1, [0, 10.5])
# Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
# Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

# f_ω
# save("three_osc_all_cor_orig_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 8)


# # maybe include 2 node toy model too?

# α = 0.1
# K = 6.0
# Ω = 10.0
# v_range = 0.01:0.01:9.99

# n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)
# """






# # old
# ### all to all

# Zallcor(n, α, Ω, K, v, m) = α*Ω .- α .* ω_sync.(n,Ω) .- α*v .+ K^2/2 * (-α*v) .* (1 ./((1 .+ n) .* (α^2 *v.^2 .+ m^2 *v.^4)) .+ n./((2) .* ((sqrt(1 - (α*Ω/(K*n))^2) * K *(1 .+ n) .- m * v.^2).^2 .+ α^2 * v.^2))) 
# Zallorig(n, α, Ω, K, v, m) = Zallcor(n, α, Ω, K, v .- ω_sync.(n,Ω), m) 
# Zallorige(n,v) = Zallorig(n, 0.1, 10, 6, v, 1)


# vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
# ns_finest_nonzero = 1/K + 0.03 : 0.03 : 30 # 0 : 0.003 : 30
# Z_all_orig_matrix = broadcast(Zallorige, collect(ns_finest_nonzero), transpose(collect(vs_finest)))
# log_Z_all_orig_matrix = log10.(abs.(Z_all_orig_matrix))
# # make heatmaps for pos and neg part separately
# Z_all_orig_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_orig_matrix )
# Z_all_orig_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_orig_matrix )
# log_Z_all_orig_matrix_pos = log10.(Z_all_orig_matrix_pos)
# log_Z_all_orig_matrix_neg = log10.(-Z_all_orig_matrix_neg)

# f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
# ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$")

# hm_neg = Makie.heatmap!(ns_finest_nonzero, vs_finest, log_Z_all_orig_matrix_neg, cscale=log10);
# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
# end
# mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

# for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
#   Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
# end
# mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])

# Makie.xlims!(ax1, [d_min, d_max])
# Makie.ylims!(ax1, [0, 10.5])
# Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
# Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

# f_ω
# save("three_osc_all_cor_orig_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 8)

