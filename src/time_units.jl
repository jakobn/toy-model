"""
contains a simulation to check what time units the solver uses
"""
#
include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

##### for two node toy model with full dynamics
ode_function = two_osc!
y0 = vcat( 0,0,5,0)
tspan = (0,200)

sol = simulation(ode_function, y0, tspan, p)

Plots.scatter(sol.t)
Plots.scatter(diff(sol.t))
sol.t
sol(sol.t,idxs=2)
Plots.scatter(sol.t, sol(sol.t,idxs=2))
Plots.plot(sol.t, sol(sol.t,idxs=2))
Plots.plot(sol.t, sol(sol.t,idxs=2), xlims=(50,51))
# about 0.1 to 0.15 time units each step




## now  with tol = 1e-3
sol = simulation(ode_function, y0, tspan, p, abstol=1e-3, reltol=1e-3)

Plots.scatter(sol.t)
Plots.scatter(diff(sol.t))
# same


## now with tol = 1e-6
sol = simulation(ode_function, y0, tspan, p, abstol=1e-6, reltol=1e-6)

Plots.scatter(sol.t)
Plots.scatter(diff(sol.t))
# 0.04



##### for networks with full dynamics