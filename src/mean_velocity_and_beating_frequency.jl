"""
compute values of solitary mean phase velocity and beating frequency from the theoretical predictions.
"""
#
using Roots

# parameters
P1 = 1.
α = 0.1
K = 6.0
n = 10 # in this case

## self consistency equation
Z(P1, α, K, n, ω) = P1 .- α .*ω .- K.^2 ./2 .* α .* ω ./ ((K.*n .- ω.^2).^2 .+ α.^2 .* ω.^2)
params = [P1, α, K, n]
ωs= find_zeros(ω -> Z(params...,ω), (1,15))

# we need the smallest of the three values
ω = find_zero(ω -> Z(params...,ω), (3,8))

# convert to a real frequency (necessary for comparison with FourierAnalysis.jl) 
f = ω/(2* pi)

## beating frequency
ω_beat_function(K, n, ω) = (K .* n .- ω.^2)./(2 .* ω)
ω_beat = ω_beat_function(K, n, ω)

# convert to a real frequency
f_beat = ω_beat./(2 * pi)