"""
Make power spectra and fits of trajectories to identify beating frequency to compare with prediction.
Also contains the call for the example trajectory plot from the thesis
"""
# using CairoMakie # probably not needed?
using Plots
using FourierAnalysis
using Roots
using LaTeXStrings
using DelimitedFiles
using LsqFit

include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

### compute timeseries
ode_function = two_osc_harm_simpler!
y0 = [0.,0.,5.,0] # this usually ends up as SESS
tspan = (0, 500)
α = 0.1
K = 6.0
n = 10.0 # beating better visible than n=6
p = [α, K , n]
plot_span = tspan

sol = simulation(ode_function, y0, tspan, p)
plt = solplot(sol, (0,50))
save("2osc_harm_simpler_n10_v2.pdf", plt) # Figure for thesis
solplot(sol, plot_span)
# solplot(sol, (0,50))
solplot(sol, (10,100))
# solplot(sol, (450,500))
solplot(sol, (100,500))
sol.t

## export
# adaptive solver
t = sol.t
writedlm("adaptive_time.txt", t)
writedlm("adaptive_phi_1.txt",sol(t, idxs=1).u)
writedlm("adaptive_phi_2.txt",sol(t, idxs=2).u)
writedlm("adaptive_omega_1.txt",sol(t, idxs=3).u)
writedlm("adaptive_omega_2.txt",sol(t, idxs=4).u)
# interpolated
t_step = 0.001
t = tspan[1] : t_step : tspan[2] 
writedlm("adaptive_interpolated_time.txt", t)
writedlm("adaptive_interpolated_phi_1.txt",sol(t, idxs=1).u)
writedlm("adaptive_interpolated_phi_2.txt",sol(t, idxs=2).u)
writedlm("adaptive_interpolated_omega_1.txt",sol(t, idxs=3).u)
writedlm("adaptive_interpolated_omega_2.txt",sol(t, idxs=4).u)

## make equidistant time steps in the solver
# sol = simulation(ode_function, y0, tspan, p)
# abstol = 1e-6
# reltol = 1e-6

prob = ODEProblem(ode_function, y0, tspan, p)
sol = solve(prob, Tsit5(), tstops = t, dt = t_step)
t = sol.t
writedlm("equidistant_time.txt", t)
writedlm("equidistant_phi_1.txt",sol(t, idxs=1).u)
writedlm("equidistant_phi_2.txt",sol(t, idxs=2).u)
writedlm("equidistant_omega_1.txt",sol(t, idxs=3).u)
writedlm("equidistant_omega_2.txt",sol(t, idxs=4).u)

## try the same with modified simlate fct with kwargs...
sol = simulation(ode_function, y0, tspan, p, tstops = t, dt = t_step)
#works!

plot_span_transient = (0, 100)
_, plt = simplotsave(ode_function, y0, tspan, [α, K, 10], plot_span_transient)
plt
save("2osc_harm_simpler_n10_t0-100.pdf", plt)

## what is the expected frequency? f = ω/2π
sqrt(K *n)
sqrt(K*n)/(2* pi)

# self-consistency equation
Z(P1, α, K, n, ω) = P1 .- α .*ω .- K.^2 ./2 .* α .* ω ./ ((K.*n .- ω.^2).^2 .+ α.^2 .* ω.^2)
P1 = 1.
params = [P1, α, K, n]

ω_range = -20. : 0.01 : 20.
Z(params..., ω_range)
Plots.plot(ω_range, ω -> Z(params..., ω), ylims=(-10,10)) #same

ωs= find_zeros(ω -> Z(params...,ω), (1,15))
ω = find_zero(ω -> Z(params...,ω), (3,8))

f = ω/(2* pi)

## What is the expected beating frequency?
ω_beat_function(K, n, ω) = (K .* n .- ω.^2)./(2 .* ω)
f_beat(K, n, ω) = (K .* n .- ω.^2)./(2 .* ω) ./(2 * pi)
f_beat_Z(P1, α, K, ω) = sqrt((K.^2 .* α) ./ (8 .* ω .* (P1 .- ω .* α)) .- α.^2 ./ 4) ./(2 * pi)

ω_beat_prediction = ω_beat_function(K, n , ω)

f_beat(K, n, ω)
f_beat_Z(P1, α, K, ω)


### calculate power spectra and plot
size_inches = (1.6* (3+3/8), 1.6* 2/3 * (3+3/8)) # 1.5 because 1.4 was too small
size_pt = 72 .* size_inches

## whole timeseries
t_stat_step = 0.001
sr = round(Int, 1/t_stat_step)
stat_span = tspan[1] : t_stat_step : tspan[2]
ϕ_2_timeseries = sol(stat_span, idxs=2).u
wl = length(ϕ_2_timeseries) 
S = spectra(ϕ_2_timeseries, sr, wl; tapering=rectangular, DC=true)
S_plt = Plots.plot(S.flabels, S.y, xlims = (0,2.5), ylims=(10^-8, 2*10^-1), yaxis=:log, yticks=[10^-8,10^-7,10^-6,10^-5,10^-4,10^-3,10^-2], xlabel="frequency f", ylabel="power spectrum S(f)", label=false, size=size_pt);
Plots.vline!([f], label="⟨f₁⟩");
Plots.vline!([2*f], label="2⟨f₁⟩");
Plots.vline!([f+f_beat(K,n,ω)], label=L"$\langle f_1\rangle + f_b$", line=(:dash,1));
Plots.vline!([f-f_beat(K,n,ω)], label=L"$\langle f_1\rangle - f_b$", line=(:dash,1))
save("S_whole_n10.pdf", S_plt)

## from 400 to 500
t_transient = 100
t_stat_step = 0.001
stat_span = t_transient : t_stat_step : tspan[2]
ϕ_2_timeseries = sol(stat_span, idxs=2).u
# sampling rate and FFT window length
sr = round(Int, 1/t_stat_step)
wl = length(ϕ_2_timeseries) 
resolution = sr/wl


S = spectra(ϕ_2_timeseries, sr, wl; tapering=rectangular, DC=true)
S_plt = Plots.plot(S.flabels, S.y, xlims = (0,2.5), ylims=(10^-6, 10^0), yaxis=:log, xlabel="frequency f", ylabel="limit power spectrum Sₗ(f)", label=false, size=size_pt);
Plots.vline!([f], label="⟨f₁⟩");
Plots.vline!([2*f], label="2⟨f₁⟩")
save("S_limit_n10.pdf", S_plt)

peak_height = maximum(S.y)
peak_position = findall(i -> isequal(peak_height, i), S.y)
peak_freq = S.flabels[peak_position][1]
# close to prediction

## time window 10 : 100 to be cleaner
t_start = 10
t_transient_shorter = 100
t_stat_step_transient = 0.0001

stat_span_transient = t_start : t_stat_step_transient : t_transient_shorter

ϕ_2_transient_timeseries = sol(stat_span_transient, idxs=2).u

sr_transient = round(Int, 1/t_stat_step_transient)
wl_transient = length(ϕ_2_transient_timeseries)
resolution_transient = sr_transient/wl_transient

# layout still a bit too small for y axis label
S_transient = spectra(ϕ_2_transient_timeseries, sr_transient, wl_transient; tapering=rectangular, DC=true)
S_transient_plt = Plots.plot(S_transient.flabels, S_transient.y, xlims = (0,2.5), ylims=(10^-7, 10^0), yaxis=:log, xlabel=L"frequency $f$", ylabel=L"transient pow. spec. $S_t(f)$", label=false, size=size_pt);
Plots.vline!([f+3.5*f_beat(K,n,ω)], label=L"$\langle f_2\rangle + 3.5 f_b$");
Plots.vline!([f-3.5*f_beat(K,n,ω)], label=L"$\langle f_2\rangle - 3.5 f_b$")
save("S_transient_n10.pdf", S_transient_plt)
# seems to be around 3.5 (not 1 or pi), but whyyy

## make plot with both
#careful what S is!!!
S_all_plt = Plots.plot(S_transient.flabels, S_transient.y, yaxis=:log, line=2, xlabel=L"frequency $f$", ylabel=L"power spectrum $S(f)$", label=L"$t \in [10,100]$",legend=:topright, size=size_pt);
Plots.plot!(S.flabels, S.y, yaxis=:log, line=2, label=L"$t \in [100,500]$");
Plots.vline!([f], label=L"\langle f_1 \rangle", line=(:dot,2));
Plots.vline!([2*f], label=L"2\langle f_1 \rangle", line=(:dot,2));
Plots.vline!([f+3.5*f_beat(K,n,ω)], label=L"$\langle f_1\rangle + 3.5 f_b$", line=(:dash,2));
Plots.vline!([f-3.5*f_beat(K,n,ω)], label=L"$\langle f_1\rangle - 3.5 f_b$", line=(:dash,2));
Plots.xlims!(0,2.5);
Plots.ylims!(10^-7, 10^0);
S_all_plt
save("S_all_n10.pdf", S_all_plt)


##### try to make fit or adjust visually

# problem: ω_beat prediction is larger than the visible beating frequency
# weird because what I saw in the power spectrum was also larger than the expected beating frequency ?!

plot_range = 0 : 0.01 : 50
t = sol(plot_range).t

Plots.plot(t, sol(plot_range, idxs=2), ylabel=L"$\varphi_2$", legend=false)

decay_rate_prediction = α/2
# in real time, rescaled time would be ϵ=α/(2*sqrt(K * n))

δ_prediction = atan(-α * ω/(K*n - ω^2))

a_prediction = K/sqrt((K*n - ω^2)^2+ α^2 * ω^2)


ω_prediction = ω
# ϕ_1_ψ_1(u0,v0,P,α,K,n,ω,t) = ω * t + 1




ϕ_2(t,θ,θ_b,a0,b0,a,d,P,K,n,ω,ω_b) = -P/(K*n) .+ (a .+ a0 * exp.(-d * t) .* (1 .+ b0 * sin.(ω_b * t .+ θ_b) ) )  .* sin.(ω*t .+ θ)
# only the initial condition parameters we want to fit, e = easy
ϕ_2_e(t,p) = ϕ_2(t,p[1],p[2],p[3],p[4],a_prediction,decay_rate_prediction,1,6,10,ω_prediction,ω_beat_prediction)
# p = [θ, θ_b, a0, b0]

p0 = [0,0,a_prediction,0.1]

# this is bad notation, change it to the syntax below
# t_data should be a sol(range).t object
t_data = t
ϕ_2_data = sol(t_data, idxs=2).u

fit = curve_fit(ϕ_2_e, t_data, ϕ_2_data, p0)

fit.param

Plots.plot(t_data, ϕ_2_data, ylabel=L"$\varphi_2$",label="simulation");
Plots.plot!(t_data, ϕ_2_e(t, fit.param), label="fit")

### try shorter sample 10:50
plot_range = 10 : 0.01 : 50
t_data = sol(plot_range).t
ϕ_2_data = sol(plot_range, idxs=2).u
# Plots.plot(t_data, ϕ_2_data, ylabel=L"$\varphi_2$",label="simulation")

# initial guess a0
a0 = 0.5 * exp.(10 * decay_rate_prediction)

p0 = [2.91,0.7,0.82,0.1]

fit = curve_fit(ϕ_2_e, t_data, ϕ_2_data, p0)

fit.param

# amplitude at t=10 with these fit parameters
a_prediction - exp(-10 * decay_rate_prediction) * fit.param[3]

Plots.plot(t_data, ϕ_2_data, ylabel=L"$\varphi_2$",label="simulation");
Plots.plot!(t_data, ϕ_2_e(t_data, fit.param), label="fit")

# problem: frequency changes, too

## try 20:50

plot_range = 20 : 0.01 : 50
t_data = sol(plot_range).t
ϕ_2_data = sol(plot_range, idxs=2).u
# Plots.plot(t_data, ϕ_2_data, ylabel=L"$\varphi_2$",label="simulation")

# initial guess a0
a0 = 0.5 * exp.(10 * decay_rate_prediction)

p0 = [2.91,0.7,0.82,0.1]

fit = curve_fit(ϕ_2_e, t_data, ϕ_2_data, p0)

fit.param

# amplitude at t=10 with these fit parameters
a_prediction - exp(-10 * decay_rate_prediction) * fit.param[3]

Plots.plot(t_data, ϕ_2_data, ylabel=L"$\varphi_2$",label="simulation");
Plots.plot!(t_data, ϕ_2_e(t_data, fit.param), label="fit")



### make frequencies fit parameters too
ϕ_2_ω_ω_b(t,θ,θ_b,a0,b0,ω,ω_b,a,d,P,K,n) = -P/(K*n) .+ (a .+ a0 * exp.(-d * t) .* (1 .+ b0 * sin.(ω_b * t .+ θ_b) ) )  .* sin.(ω*t .+ θ)
# only the initial condition parameters we want to fit, e = easy
ϕ_2_ω_ω_b_e(t,p) = ϕ_2_ω_ω_b(t,p[1],p[2],p[3],p[4],p[5],p[6],a_prediction,decay_rate_prediction,1,6,10)
# p = [θ, θ_b, a0, b0, ω, ω_b]

plot_range = 20 : 0.01 : 50
t_data = sol(plot_range).t
ϕ_2_data = sol(plot_range, idxs=2).u

p0 = [2.91,0.7,-0.9,0.5,ω_prediction, ω_beat_prediction*pi]

fit = curve_fit(ϕ_2_ω_ω_b_e, t_data, ϕ_2_data, p0)

fit.param

Plots.plot(t_data, ϕ_2_data, ylabel=L"$\varphi_2$",label="simulation");
Plots.plot!(t_data, ϕ_2_ω_ω_b_e(t_data, fit.param), label="fit")


plot_range_full = 0 : 0.01 : 50
t_data_full = sol(plot_range_full).t
ϕ_2_data_full = sol(plot_range_full, idxs=2).u

plt = Plots.plot(t_data_full, ϕ_2_data_full, xlabel=L"$t$", ylabel=L"$\varphi_2$",label="simulation", legend=:right);
Plots.plot!(t_data, ϕ_2_ω_ω_b_e(t_data, fit.param), label="fit", linestyle=:dash)

# beating frequency is 1.59 instead of 1.49 (pi*ω_beat_prediction) or ω_beat_prediction

save("fit.pdf",plt)

fit.param[6]/ω_beat_prediction

"""
#old stuff:

## start tail later
# we see: peaks become more pronounced, but the peak at 2f stays.
t_tail = 400
t_stat_step = 0.0011
stat_span = t_tail : t_stat_step : tspan[2]
ϕ_2_tail_timeseries = sol(stat_span, idxs=2).u
sr = round(Int, 1/t_stat_step)
wl = length(ϕ_2_tail_timeseries)
resolution = sr/wl

S_tail = spectra(ϕ_2_tail_timeseries, sr, wl; tapering=rectangular, DC=true)
S_tail_plt = Plots.plot(S_tail.flabels, S_tail.y, xlims = (1.0,2.5), ylims=(10^-10, 10^0), yaxis=:log, xlabel="frequency f", ylabel="limit power spectrum Sₗ(f)", label=false, size=size_pt);
Plots.vline!([f], label="⟨f₁⟩");
Plots.vline!([2*f], label="2⟨f₁⟩")
save("S_tail_n10.pdf", S_plt)


## some experimentation with spectra
# phase shift does nothing, exp decay makes tails left and right decay faster but no shift in frequency
test_timeseries = cos.(2*pi* f .* stat_span_transient .+ pi/2) .* cos.(2*pi*f_beat(K,n,ω) .* stat_span_transient)
S_test = spectra(test_timeseries, sr_transient, wl_transient; tapering=rectangular, DC=true)
S_test_plt = Plots.plot(S_test.flabels, S_test.y, xlims = (0,2.5), ylims=(10^-6, 10^2), yaxis=:log, xlabel="frequency f", ylabel="transient power spectrum Sₜ(f)", label=false);
Plots.vline!([f+f_beat(K,n,ω)], label="⟨f₁⟩ + f beat");
Plots.vline!([f-f_beat(K,n,ω)], label="⟨f₁⟩ - f beat")

test_timeseries_exp = exp.(-10* α.^2 ./2 .* stat_span_transient) .* cos.(2*pi* f .* stat_span_transient) .* cos.(2*pi*f_beat(K,n,ω) .* stat_span_transient)
S_test_exp = spectra(test_timeseries_exp, sr_transient, wl_transient; tapering=rectangular, DC=true)
S_test_plt_exp = Plots.plot(S_test_exp.flabels, S_test_exp.y, xlims = (0,2.5), ylims=(10^-6, 10^2), yaxis=:log, xlabel="frequency f", ylabel="transient power spectrum Sₜ(f)", label=false);
Plots.vline!([f+f_beat(K,n,ω)], label="⟨f₁⟩ + f beat");
Plots.vline!([f-f_beat(K,n,ω)], label="⟨f₁⟩ - f beat")
# n = 20
# sol, plt = simplotsave(ode_function, y0, tspan, [α, K, 20], plot_span)
# plt
# S_transient = spectra(sol(stat_span_transient, idxs=3).u, sr_transient, wl_transient; tapering=rectangular, DC=true)
# Plots.plot(S_transient.flabels, S_transient.y, xlims = (0,10), yaxis=:log)

# n = 100
# sol, plt = simplotsave(ode_function, y0, tspan, [α, K, 100], plot_span)
# plt
# S_transient = spectra(sol(stat_span_transient, idxs=3).u, sr_transient, wl_transient; tapering=rectangular, DC=true)
# Plots.plot(S_transient.flabels, S_transient.y, xlims = (0,10), yaxis=:log)

# ## try to find beating frequency in t_transient_shorter
# t_transient_shorter = 50
# t_stat_step_transient = 0.00005

# stat_span_transient = 0 : t_stat_step_transient : t_transient_shorter

# ϕ_2_transient_timeseries = sol(stat_span_transient, idxs=2).u

# sr_transient = round(Int, 1/t_stat_step_transient)
# wl_transient = 1000000
# resolution_transient = sr_transient/wl_transient

# S_transient = spectra(ϕ_2_transient_timeseries, sr_transient, wl_transient; tapering=rectangular, DC=true)
# S_transient_plt = Plots.plot(S_transient.flabels, S_transient.y, xlims = (0,2.5), ylims=(10^-6, 10^2), yaxis=:log, xlabel="frequency f", ylabel="transient power spectrum Sₜ(f)", label=false, size=size_pt);
# Plots.vline!([f], label="⟨f₁⟩");
# Plots.vline!([f_beat(K,n,ω)], label="f beat")
# save("S_transient_n10.pdf", S_transient_plt)
"""
