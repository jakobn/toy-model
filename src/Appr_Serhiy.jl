"""
Generate Plots to compare with the multiscaling approach that Serhiy Yanchuk suggested for the toy model.
"""
#
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings

function two_osc!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
    dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2])
    nothing
end

##

y0 = [0.,0.,5.,0.]
tspan = (0., 500.)
p = [0.1, 6., 30]
tplot = 497.:0.001:500.

function asymptotics_multiscaling(y0, tspan, tplot, p; savename="")

    prob = ODEProblem(two_osc!, y0, tspan, p)
    sol = solve(prob, Tsit5())

    α, K, n = p
   
    ϕ_1 = sol(tplot, idxs=1).u
    ϕ_2 = sol(tplot, idxs=2).u
    f_1 = ϕ_2
    f_2 = (-1. .+ K .* broadcast(x->sin(x), ϕ_1)) ./ (K * n)

    plt1 = Plots.plot(tplot, [f_1, f_2], xaxis="t", labels=[L"$\phi_2$" L"$f_2$"], title=("n="*string(n)) );
    plt2 = Plots.plot(f_1, f_2, xaxis=L"$f_1$", yaxis=L"$f_2$", labels="");
    plt = Plots.plot(plt1, plt2; layout=(2,1), size=(700,500))

    if savename != ""
        save(savename*".png", plt)
    end

    plt
end

##
function simplot(y0, tspan, p; saveit=true)
    prob = ODEProblem(two_osc!, y0, tspan, p)
    sol = solve(prob, Tsit5())
    plt1 = Plots.plot(sol,vars=(0,1))
    plt2 = Plots.plot(sol,vars=(0,2))
    plt3 = Plots.plot(sol,vars=(0,3))
    plt4 = Plots.plot(sol,vars=(0,4))
    plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
    if saveit == true
      α, K, n = p
      name = "1osc_Kuramoto_"*string(α)*"_"*string(K)*".png"
      save(name, plt)
    end
    plt
end