"""plots trajectories of phase angles
shows slow drift of synchronous component due to power imbalance
"""
#
include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

t_sim = (0,1000)

α=0.1
K=6.0
n = 10
p = [α, K, n]

y0 =[0,0,5,0]
sol = simulation(two_osc!, y0, t_sim, p)
solplot(sol, t_sim)

# no slow rotation visible, reason is Slack node?

### three oscillators
y0 =[0,0,0,5,0,0]
sol = simulation(three_osc!, y0, t_sim, p)
solplot_omegas(sol, t_sim)

function solplot_phis(sol, plot_span)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  n_phis = Int(length(sol.u[1])/2)
  label = raw"$\varphi_1$"
  plt = Plots.plot(t, sol(plot_range, idxs=1),
    label=label ,xaxis="t", size = (700,500))
  for i in (2) : (n_phis)
    label = raw"$\varphi_{"* string(i) *  raw"}$"
    Plots.plot!(t, sol(plot_range, idxs=i), label=label)
  end
  plt
end

# check that the synchronous component slowly rotates, all phases with \omega_{sync} t
phi_plot = solplot_phis(sol, (0,100))

save("phi_plot.pdf", phi_plot)

# LaTeXStrings are broken?
Plots.plot(sol(0:0.01:1000, idxs=2))
Plots.plot(sol(0:0.01:1000, idxs=[2,3]), labels = [L"$\varphi_2$", L"$\varphi_3$"], linestyle=[:solid,:dash], size=(1050,750), ticklabelfontsize=12, legendfontsize=12)


P1 = 1
N = n+2
#overestimated ω_sync (would be true for natural limit cycle)
ω_sync = -P1/(α *(N-1))
ω_sync * (t_sim[2] - t_sim[1])



## check what the zero eigenmode in the reduced system does

function solplot_sum_phis_star_original(sol, plot_span, n)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  label = raw"$\xi_1$"
  plt = Plots.plot(t, sol(plot_range, idxs=1).u .+ sol(plot_range, idxs=2).u + sol(plot_range, idxs=3).u .* n,
    label=label, xaxis="t", size = (700,500))
end

function solplot_sum_phis_star_reduced(sol, plot_span, n)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  label = raw"$\xi_1$"
  plt = Plots.plot(t, sol(plot_range, idxs=2).u .+ sol(plot_range, idxs=3).u .* n,
    label=label, xaxis="t", size = (700,500))
end

function solplot_sum_phis_star_reduced(sol, plot_span, n)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  label = raw"$\sqrt{n+1} \xi_1$"
  plt = Plots.plot(t, sol(plot_range, idxs=2).u .+ sol(plot_range, idxs=3).u .* n,
    label=label, xaxis="t", size = (700,500))
end

function solplot_zero_mode_star_reduced(sol, plot_span, n)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  label = raw"$\xi_1$"
  plt = Plots.plot(t, (sol(plot_range, idxs=2).u .+ sol(plot_range, idxs=3).u .* n) ./ sqrt(n+1),
    label=label, xaxis="t", size = (700,500))
end
# check that sum of all phases converges to a constant
solplot_sum_phis_star_original(sol, (0,1000), n)

# compute solitary mean phase velocity numerically
ω_s = mean(sol(100:0.1:1000, idxs=4).u)

# check that sum of phases in reduced network goes with -\omega_s t
solplot_sum_phis_star_reduced(sol, (0,1000), n)
Plots.plot!(t -> -ω_s * identity(t), label=L"$\omega_{s} t$")

# rescale to get zero mode
solplot_zero_mode_star_reduced(sol, (0,1000), n)
Plots.plot!(t -> -ω_s * identity(t), label=L"$\omega_{s} t$")

# plot the modulation of the zero mode
function solplot_zero_mode_modulation_star_reduced(sol, plot_span, n)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  t_span_mean = sol.t[end]-500 : 0.01 : sol.t[end] 
  ω_s = mean(sol(t_span_mean, idxs=4))
  label = raw"$\xi_1 - \omega_s t / \sqrt{n+1}$"
  plt = Plots.plot(t, (sol(plot_range, idxs=2).u .+ sol(plot_range, idxs=3).u .* n) ./ (sqrt(n+1)) .+  ω_s .* identity(t) ./ sqrt(n+1),
    label=label ,xaxis="t", size = (700,500))
end

solplot_zero_mode_modulation_star_reduced(sol, (990,1000), n)
# very slow drift probably comes from numerical error when computing \omega_s
# ar least it gets smaller when increasing the accuracy of the estimate

# rough estimate of period (with the eye)
10/8.5 * 2*pi

# conclusion: zero mode oscillates with roughly \omega_s.