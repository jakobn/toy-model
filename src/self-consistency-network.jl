"""
self-consistency equations for complex networks
contour plots
"""
#
using Plots
using ImplicitEquations
using LaTeXStrings
using FileIO
using JLD
using CairoMakie
using LinearAlgebra
using Graphs
using Symbolics

N = 2 #N = size(L)[1]
α_vec = 0.1 .* ones(N)
β_vec = 1 .* ones(N)
K = 6.
K_mat = [0 K; K 0]# adjacency matrix
@variables n, ω_s
P = 1.
P_vec = [-P, P/n]
γ = sqrt(1 - (P/(K*n))^2 )
L = K * γ * [n -n;-1 1]# Laplacian

size(L)

eigenvalues, eigenvectors = eigen(L)

eigenvalues
eigenvectors
eigenvectors[:,1]

β_vec[:]

test(l, α_vec) = α_vec[l]

R_kil(k, i, l, α_vec, β_vec, ω_s, eigenvalues, eigenvectors) = eigenvectors[i,l] * eigenvectors[k,l] /(-β_vec[l] * ω_s^2 + 1im * α_vec[l] * ω_s + eigenvalues[l])
R_il(k, i, α_vec, β_vec, ω_s ,eigenvalues, eigenvectors) = sum(eigenvectors[i,:] .* eigenvectors[k,:] ./(-β_vec[:] .* ω_s^2 .+ 1im .* α_vec[:] .* ω_s .+ eigenvalues[:]) )

# R_mat_vec = 
# R_mat = # sum over l # outer_product of v_i v_k # R_ik
# -β_vec[:] .* ω_s^2 .+ 1im .* α_vec[:] .* ω_s .+ eigenvalues[:]

