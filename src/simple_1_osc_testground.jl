"""
Testground for single oscillator system.
Simulation and Plotting.
"""
##

using OrdinaryDiffEq
using Random
using Statistics

##
"""
does not work --- why?

function one_osc!(dy, y, k, t)
  @views begin
    ϕ = y[1:1]
    ω = y[2:2]
    dϕ = dy[1:1]
    dω = dy[2:2]
  end
  dϕ = ω[1]
  dω = 1. - k[1] * ω[1] - k[2] * sin(ϕ[1])
  nothing
end

function one_osc!(dy, y, k, t)
  @views begin
    ϕ = y[1]
    ω = y[2]
    dϕ = dy[1]
    dω = dy[2]
  end
  dϕ = ω
  dω = 1. - k[1] * ω - k[2] * sin(ϕ)
  nothing
end
"""
##

function one_osc!(dy, y, p, t)
  α, K = p
  dy[1] = y[2]
  dy[2] = 1. - α * y[2] - K * sin(y[1])
  nothing
end

##

prob = ODEProblem(one_osc!,5. * randn(2),(0., 100.),[0.1, 6.0])
sol = solve(prob, Tsit5())

##

function sample_asymptotic_frequencies(p)
  prob = ODEProblem(one_osc!,5. * randn(2),(0., 1000.),p)
  sol = solve(prob, Tsit5())
  mean(sol(700.:0.1:1000., idxs=2).u)
end

##

function simplot(y0, tspan, p; saveit=true)
  prob = ODEProblem(one_osc!, y0, tspan, p)
  sol = solve(prob, Tsit5())
  plt1 = Plots.plot(sol,vars=(0,1))
  plt2 = Plots.plot(sol,vars=(0,2))
  plt = Plots.plot(plt1, plt2; layout=(1,2), size=(700,500), xaxis="t")
  if saveit == true
    α, K = p
    name = "1osc_Kuramoto_"*string(α)*"_"*string(K)*".png"
    save(name, plt)
  end
  plt
end

##

function asymptotic_freqs(ks, N_per_k)
  af = zeros(length(ks[1]), length(ks[2]), N_per_k)
  print("\n Simulating")
  for (i_k_1, k_1) in enumerate(ks[1])
    print("\r Simulating $k_1 ")
    for (i_k_2, k_2) in enumerate(ks[2])
      print("\r Simulating $k_2 ")
      for i_N in 1:N_per_k
        af[i_k_1, i_k_2, i_N] = sample_asymptotic_frequencies([k_1, k_2]) 
      end
    end
  end
  af
end

##
# α, K
ks = [0.01:0.01:0.3,0.:1:10.]
# ks = [0.01:0.01:0.3,6.]
# ks = [0.1, 0.:0.2:10.]

ks[1][10] # this is where α = 0.1
ks[2][7] # this is where K = 6.0

N_per_k = 30
af = asymptotic_freqs(ks, N_per_k)

##

using CairoMakie
using LaTeXStrings

##

# Can't make alpha to small or it will round to zero when cast to RGBA.
alpha = 2. / N_per_k > 0.02 ? 2. / N_per_k : 0.02
color = (:black, alpha)

marker_size = 4

size_inches = (1.4* (3+3/8), 1.4* 2/3 * (3+3/8)) # half the size (1.4 times side length instead of 2) of other bifurcation diagrams like 2-node toy model
size_pt = 72 .* size_inches

f = Figure(resolution=size_pt, fontsize=12);
# f = Figure(resolution=(1200, 800));
Axis(f[1, 1], ylabel="⟨ω⟩", xlabel="α")
 
N_α, N_K, N_per_k = size(af)

for i_N in 1:N_per_k
  for i_K in 7 #1:N_K
    Makie.scatter!(ks[1], af[:,i_K,i_N]; color = color, markersize=marker_size)
  end
end

save("bif_1osc_K_6.0_v2.png", f, px_per_unit = 5)
# save("bif_1osc_K_6.0_v2.pdf", f, px_per_unit = 5)

f

##

f = Figure(resolution=size_pt, fontsize=12);
Axis(f[1, 1], ylabel="⟨ω⟩", xlabel="K")

for i_N in 1:N_per_k
  for i_α in 10 #1:N_α
    Makie.scatter!(ks[2], af[i_α,:,i_N]; color = color, markersize=marker_size)
  end
end

save("bif_1osc_a_0.1_v2.png", f, px_per_unit = 5)

f

##
 #twice the size (1.4 times side length): same size as 2-node toy model bifurcation diagrams (full page)
f_3d = Figure(resolution = size_pt .* 1.4, fontsize=12);
ax = Axis3(f_3d[1, 1], xlabel="α", ylabel = "⟨ω⟩", zlabel = "K")

for (i_k_1, k_1) in enumerate(ks[1])
  for i_N in 1:N_per_k
    Makie.scatter!(ones(length(ks[2]))*k_1, af[i_k_1,:,i_N], ks[2]; color = color, markersize= marker_size)
  end
end

save("bif_1osc_3d_new_v2.png", f_3d, px_per_unit = 5)

f_3d

##
