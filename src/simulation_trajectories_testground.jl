"""
Testground.
Simulation and saving of trajectories.
"""
#
using OrdinaryDiffEq
using Random
# using Statistics
using Plots
using CairoMakie
using LaTeXStrings
# using JLD
# using ImplicitEquations
# using FileIO


## ode functions

function two_osc!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      ω = y[3:4]
      dϕ = dy[1:2]
      dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
    dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2]) 
    nothing
  end
  
  ##
  # linearized wrt to ϕ_2
  function two_osc_harm!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = -1. - α * ω[2] - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2 and w/o P_2
  function two_osc_harm_woP2!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = - α * ω[2] - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2 and w/o P_1 and P_2
  function two_osc_harm_woP12!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = - α * ω[2] - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2 and w/o dampening of node 2 (α_2 = 0)
  function two_osc_harm_wodamp2!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = -1. - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2, w/o P2 and w/o dampening of node 2 (α_2 = 0)
  function two_osc_harm_woP2_wodamp2!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = - K * (-sin(ϕ[1]) + cos(ϕ[1]) * ϕ[2]) - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2, and n + cos(ϕ_1) = n
  # this is the minimal toy model (you could set P_2 = 0 too but keeping it is no big effort)
  function two_osc_harm_simpler!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = -1. - α * ω[2] - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2, osc2 is decoupled from osc 1 (no driving), P_2 = 0
  function two_osc_harm_dec2_woP2!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = - α * ω[2] - K * n * ϕ[2] 
      nothing
  end
  
  ##
  # linearized wrt to ϕ_2, P_2 = 0, α_2 = 0, n + cos(ϕ_1) = n
  function two_osc_harm_woP2_wodamp2_simpler!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
      nothing
  end
  
  
  ##
  # linearized wrt to ϕ_2, P_2 = 0, n + cos(ϕ_1) = n
  function two_osc_harm_woP2_simpler!(dy, y, p, t)
      α, K, n = p
      @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
      end
      dϕ[1] = ω[1]
      dϕ[2] = ω[2]
      dω[1] = 1. - α * ω[1] - K * (sin(ϕ[1]) - cos(ϕ[1]) * ϕ[2])
      dω[2] = - α * ω[2] - K * (-sin(ϕ[1])) - K * n * ϕ[2] 
      nothing
  end

## simulation and plot functions

"""
simulation(ode_function, y0, tspan, p; saveit=true)
simulates ode_function
with parameters p
and initial value y0
for time interval t_span

returns solution sol
"""

function simulation(ode_function, y0, tspan, p)
    prob = ODEProblem(ode_function, y0, tspan, p)
    sol = solve(prob, Tsit5())
end

"""
solplot(sol, plot_span)
plots trajectories of sol, assuming 4 variables (ϕ_1, ϕ_2, ω_1, ω_2)
for plot_span as plt
returns plt
"""
function solplot(sol, plot_span)
    plt1 = Plots.plot(sol,vars=(0,1), labels=L"$\varphi_1$")
    plt2 = Plots.plot(sol,vars=(0,2), labels=L"$\varphi_2$")
    plt3 = Plots.plot(sol,vars=(0,3), labels=L"$\omega_1$")
    plt4 = Plots.plot(sol,vars=(0,4), labels=L"$\omega_2$")
    plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
    plt
end
"""
saveplot saves the plot plt as name.pdf
the name is automatically generated using the name of ode_function and parameters assuming the form p =  [α, K , n]
optional argument add_name can be added to the end
file type is save_type, e.g. "pdf", "png"

for custom_name use save(custom_name, plt)
custom_name has to end with file type, e.g. ".pdf", ".png"
"""
function saveplot(plt, ode_function, p, save_type; add_name="")
    if add_name != ""
        add_name = "_"*add_name
    end
    function_name = strip(string(ode_function),'!') # has to be single quotation marks for strip()
    α, K, n = p
    plot_name = function_name*"_"*string(α)*"_"*string(K)*"_"*string(n)*add_name*"."*save_type
    save(plot_name, plt)
end

"""
simplotsave(ode_function, y0, tspan, p; save_it=false, custom_name="", add_name="")
simulates ode_function
with parameters p
and initial value y0
for time interval t_span

plots trajectories as in solplot() as plt

if save_type == "" (default): no saving
else: saves plt as type save_type
with custom_name
if custom_name = "", saves with automated name as in saveplot() with optional argument add_name

returns solution sol and plot plt
"""
function simplotsave(ode_function, y0, tspan, p, plot_span; save_type="", custom_name="", add_name="")
    sol = simulation(ode_function, y0, tspan, p)
    plt = solplot(sol, plot_span)
    if save_type != ""
        if custom_name != ""
            save(custom_name*"."*save_type, plt)
        else
            saveplot(plt, ode_function, p, save_type; add_name=add_name)
        end
    end
    return sol, plt
end

## example

ode_function = two_osc_harm_simpler!
y0 = [0.,0.,5.,0] # this usually ends up as SESS
tspan = (0, 50)
α = 0.1
K = 6.0
n = 5.0
p = [α, K , n]
plot_span = tspan
save_type = "pdf"
custom_name_simplotsave = "test_simplotsave"
custom_name_manually = "test_manually"

# compact function
sol, plt = simplotsave(ode_function, y0, tspan, p, plot_span; save_type="pdf", custom_name=custom_name_simplotsave)

# does the same as
sol = simulation(ode_function, y0, tspan, p)
plt = solplot(sol, plot_span);
save(custom_name_manually*"."*save_type, plt)

## make plots for different n, save as png for easy sliding while watching
n_vec = [2, 3, 4, 5, 6, 7, 8, 9, 10, 100]
for n in n_vec
    p = [α, K, n]
    sol, plt = simplotsave(ode_function, y0, tspan, p, plot_span; save_type="png", add_name="n_scan")
    display(plt)
end



