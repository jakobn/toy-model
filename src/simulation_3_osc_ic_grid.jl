"""
Generate statistics for three-oscillator system from tail of simulated trajectories with a grid of initial conditions.
"""
#
include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

α = 0.1
K = 6.0
ns = 0.:0.2:30.

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

n_initial_ϕ = 10
n_initial_ω = 100
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

# testing plotting abilities
# y0 =[0,0,0,5,0,0]
# n = 10
# p = [α, K, n]
# sol = simulation(three_osc!, y0, t_sim, p)
# solplot_omegas(sol, t_sim)

###
savename_full = "three_osc_star_full_fullstats_ic_grid"
@time af_full = asymptotic_statistics_3(three_osc! , ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
JLD.save("data//"*savename_full*".jld", "data", af_full)
af_full = load("data//"*savename_full*".jld")["data"]

savename_lin = "three_osc_lin_fullstats_ic_grid"
af_lin = asymptotic_statistics_3(three_osc_lin! , ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
JLD.save("data//"*savename_lin*".jld", "data", af_lin)
af_lin = load("data//"*savename_lin*".jld")["data"]

savename_lin_simpler = "three_osc_lin_simpler_fullstats_ic_grid"
af_lin_simpler = asymptotic_statistics_3(three_osc_lin_simpler! , ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
JLD.save("data//"*savename_lin_simpler*".jld", "data", af_lin_simpler)
af_lin_simpler = load("data//"*savename_lin_simpler*".jld")["data"]