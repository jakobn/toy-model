"""
Simulations and plots for first order Kuramoto model.
"""
#
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings

##

function two_osc!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:2]
      dϕ = dy[1:2]
    end
    dϕ[1] = 1. /α - K/α * sin(ϕ[1] - ϕ[2])
    dϕ[2] = -1. /α - K/α * sin(ϕ[2] - ϕ[1]) - K/α * n * sin(ϕ[2])
    nothing
end

##

function two_osc_red!(dy, y, p, t)
    K, n = p
    @views begin
      ϕ = y[1:2]
      dϕ = dy[1:2]
    end
    dϕ[1] = 1. - K * sin(ϕ[1] - ϕ[2])
    dϕ[2] = -1. - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2])
    nothing
  end

##

  function simplot(y0, tspan, p; saveit=true)
    prob = ODEProblem(two_osc!, y0, tspan, p)
    sol = solve(prob, Tsit5())
    plt1 = Plots.plot(sol,vars=(0,1))
    plt2 = Plots.plot(sol,vars=(0,2))
    plt = Plots.plot(plt1, plt2; layout=(1,2), size=(700,500), xaxis="t")
    if saveit == true
      α, K, n = p
      name = "2osc_Kuramoto_1st_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
      save(name, plt)
    end
    plt
  end

##

#######

function sample_asymptotic_frequencies(p)
    prob = ODEProblem(two_osc_red!,pi * randn(2),(0., 1000.),p)
    sol = solve(prob, Tsit5())
    δ = 0.1
    mean(diff(sol(500.:δ:1000., idxs=1).u) ./ δ), mean(diff(sol(500.:δ:1000., idxs=2).u) ./ δ)
  end
  
  ##
  
  function asymptotic_freqs(K, ns, N_per_n)
    af = zeros(2, length(ns), N_per_n)
    print("\n Simulating")
    for (i_n, n) in enumerate(ns)
      print("\r Simulating $n ")
      for i_N in 1:N_per_n
        af[1, i_n, i_N], af[2, i_n, i_N] = sample_asymptotic_frequencies([K,n])  
      end
    end
    af
  end
  
  ##
  
  function asymptotic_freqs_Ks(Ks, n, N_per_K)
    af = zeros(2, length(Ks), N_per_K)
    print("\n Simulating")
    for (i_K, K) in enumerate(Ks)
      print("\r Simulating $K ")
      for i_N in 1:N_per_K
        af[1, i_K, i_N], af[2, i_K, i_N] = sample_asymptotic_frequencies([K,n])  
      end
    end
    af
  end  

  ##
  ns = 0.:0.2:40.
  N_per_n = 100
  K = 1.01
  af = asymptotic_freqs(K, ns, N_per_n)

  N_per_K = 100
  n = 30
  Ks = 0.:0.01:1.1
  af = asymptotic_freqs_Ks(Ks, n, N_per_K)
  ##
  
  using CairoMakie
  using LaTeXStrings
  
  ##
  
  # Can't make alpha to small or it will round to zero when cast to RGBA.
  alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
  alpha = 2. / N_per_K > 0.02 ? 2. / N_per_K : 0.02
  color = (:black, alpha)
  
  
  f = Figure(resolution=(1200, 800));
  Axis(f[1, 1], ylabel=L"$\langle\omega_1\rangle$", xlabel=L"$n$")
  
  for i_N in 1:N_per_n
    Makie.scatter!(ns, af[1,:,i_N]; color = color, legend = false)
  end
  
  Axis(f[2, 1], ylabel=L"$\langle\omega_2\rangle$", xlabel=L"$n$")
  
  for i_N in 1:N_per_n
    Makie.scatter!(ns, af[2,:,i_N]; color = color, legend = false)
  end
  
  save("bif_2osc_1st_K_"*string(K)*".png", f)
  
  f
  
  ##

  f = Figure(resolution=(1200, 800));
  Axis(f[1, 1], ylabel=L"$\langle\omega_1\rangle$", xlabel=L"$K$")
  
  for i_N in 1:N_per_K
    Makie.scatter!(Ks, af[1,:,i_N]; color = color, legend = false)
  end
  
  Axis(f[2, 1], ylabel=L"$\langle\omega_2\rangle$", xlabel=L"$K$")
  
  for i_N in 1:N_per_K
    Makie.scatter!(Ks, af[2,:,i_N]; color = color, legend = false)
  end
  
  save("bif_2osc_1st_n_"*string(n)*".png", f)
  
  f

  ##
  
  f_3d = Figure(resolution=(1200, 800), title="Osillators 3d")
  ax = Axis3(f_3d[1, 1], title = "Oscillators 3d", xlabel="n", ylabel = "Mean ω Oscillator 1", zlabel = "Mean ω Oscillator 2")
  
  for i_N in 1:N_per_n
    Makie.scatter!(ns, af[1,:,i_N], af[2,:,i_N]; color = color)
  end
  
  save("bif_2osc_1st_K_"*string(K)*"_3d.png", f_3d)
  
  f_3d
  
  ##