"""
find and plot new in-between states that appear in the harmonic system
"""
#
where_between = findall(i->(i<5.5 && i > 4),af)
Random.seed!(21)
# turns out ic != ic_dump. Maybe randn is used by the solvers or so
ic = 5 .* randn(4, 151, 20)

pos = 3
ind = where_between[pos]

simplot(ic_dump[:,ind[2],ind[3]], (0.,100.),[0.1, 6.0, ns[ind[2]]])
simplot([0.,0.,7., 0.], (0.,50.),[0.1, 6.0, ns[ind[2]]])

x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot = tunepar_both[ic_dump[:,ind[2],ind[3]], (0., 200.), 50., [.1, 6., ns[ind[2]]], [.1, 6., 30.], [.1, 6., 1.], [0., 0., 0.1], save_plot=true ]

y_0 = ic_dump[:,ind[2],ind[3]]
t_span = (0., 200.)
t_stat = 50.
p_0 = [.1, 6., ns[ind[2]]]
p_1_high = [.1, 6., 30.]
p_1_low = [.1, 6., 1.]
dp = [0., 0., 0.1]

tunepar_both(y_0, t_span, t_stat, p_0, p_1_low, p_1_high, dp, save_plot=true)

Plot(K / Sqrt((K n - v^2)), )