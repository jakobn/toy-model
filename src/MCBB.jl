"""
Work in progess.
Use MCBB to analyze oscillator system.
"""
#
using MCBB
using LightGraphs
using Clustering
using DifferentialEquations
using Distributions
using StatsBase
using Plots
using Random
import PyPlot
Random.Random.seed!(1997);


struct Kuramoto_pars <: DEParameters
    P::Number
    α::Number
    K::Number
    n::Number
end

function Kuramoto!(dy,y,p::Kuramoto_pars,t)
    @views begin
        ϕ = y[1:2]
        ω = y[3:4]
        dϕ = dy[1:2]
        dω = dy[3:4]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dω[1] = p.P - p.α * ω[1] - p.K * sin(ϕ[1] - ϕ[2])
    dω[2] = -p.P - p.α * ω[2] - p.K * sin(ϕ[2] - ϕ[1]) - p.K * n * sin(ϕ[2])
    nothing
end

Δω = 10

# initial conditions
ic = zeros(4)
ϕ_dist = Uniform(-pi, pi)
ω_dist = Uniform(-Δω,Δω)
ic_ranges = ()->[rand(ϕ_dist),rand(ϕ_dist),rand(ω_dist),rand(ω_dist)]
N_ics = 1000

# parameters
P_dist = Dirac(1)
α_dist = Uniform(0.05, 0.3)
K_dist = I


ndist = Uniform(0,30)









