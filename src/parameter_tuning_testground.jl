"""
Testground.
Explore parameter ranges in the toy model in which the resonant solitary states appear by tuning parameters.
"""
#
using CairoMakie

include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))
# include(joinpath(@__DIR__,"parameter_tuning_tools.jl"))

ode_function = two_osc!
y0 = [0,0,5,0]
tspan = (0, 1000)
t_stat = 300
# standard values
α = 0.1
K = 6.0
n = 5.0
P = 1.0

p0 = [α, K, n]



p1_low = [0.05, K, n]
p1_high = [0.3, K, n]
dp = [0.01, 0, 0]

x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot =
tunepar_both_simulation(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp; save_plot=false)

size_inches = ((3+3/8), 2/3 * (3+3/8))
size_pt = 72 .* size_inches
f = Figure(resolution=size_pt, fontsize=12);
#
symbol = "α"
start = 0.1
#
ax = Axis(f[1,1], xlabel=symbol)
Plots.scatter!(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec),
markershape=:x, markersize=1,
labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"])
vline!([start], label="start")
# seems like Plots.scatter does not plot on ax automatically
# I have a convenient way of handling figure sizes in Makie now, so I use Makie
Legend(f, ax)
display(f)

# does not work woth Makie in this format, only with Plots
# x = [1,2,3]
# repeat(x,1,size(y)[2])
# y = [[3,3,4] [6,6,6]]
# size(y)
# Makie.scatter(repeat(x,1,size(y)[2]),y)

# dpi = 100 # standard

ind = 1
x_p_inc = p_mat_inc[:,ind]
x_p_dec = p_mat_dec[:,ind]

fontsize = 12
legend_position = :bottomright
savename = "test_save"
labels = [L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"]
# markerstyle=:x yields black legend entries
# markerstrokealpha=0 or markerstrokewidth=0 (either makes black rim disappear))
# but then: emptly legend but plot markers are there?!
# settle for round markers?

plt = Plots.scatter(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec),
      markershape=:o, markersize=2, markerstrokewidth=0,
      markerstrokecolor=:match, labels= labels,
      xlabel=symbol, labelfontsize=fontsize,
      
      xtickfontsize=fontsize, ytickfontsize = fontsize,
      legendfontsize= fontsize,
      size=100 .*size_inches, legend=:bottomright)
vline!([start], label="start")
# markersize 1: not visible in pdf
# saving with higher resolution/size as png is not possible?
# increasing size of everything makes text go over figure edges, very thin axes...
# FileIO.save(), Plots.savefig(), Plots.png() all don't seem to have a resolution or px_per_unit functionality for Plots.jl Plot objects,
# the latter works with Makie Figures, which don't seem to work with Plots.jl functions...
# Plots.savefig(plt, "savefig.png", width=500, height=..., scale=...) # does not work
Plots.pdf(plt, savename)

Plots.savefig(f, "savefig.png")

f = Figure(resolution=size_pt, fontsize=12);
ax = Axis(f[1,1], xlabel=symbol)
save("test_5.png", f, px_per_unit=5)
# for some reason px_per_unit kw does not work (only with figures (Makie))
#####


ode_function = two_osc!
y0 = [0,0,5,0]
tspan = (0, 100)
t_stat = 50
p0 = [0.1, 6.0, 5.0]
p1 = [0.3, 6.0, 5.0]
dp = [0.05,0.,0.]
show_steps = true
save_steps_type = "png"
size_inches = size_inches = ((3+3/8), 2/3 * (3+3/8))

p_mat, y_mat, stats = tunepar_simulation(ode_function, y0, tspan, t_stat, p0, p1, dp, show_steps=show_steps, save_steps_type=save_steps_type)


# save_type = "pdf"
# statsplot_save(plt, dp, save_type)

##
# p_mat, y_mat, stats = tunepar_simulation(ode_function, y0, tspan, t_stat, p0, p1, dp; show_steps=show_steps, save_steps_type=save_steps_type)
# plt = tunepar_statsplot(dp, p_mat, stats, size_inches, legend_position=legend_position);
##

# save_steps_type="png"
# legend_position = :outerright
# add_name = ""

# p_mat, y_mat, stats, plt = tunepar_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1, dp, save_type, size_inches;
#     show_steps=show_steps, save_steps_type=save_steps_type, legend_position=legend_position, add_name=add_name)



p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, plt =
tunepar_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp, save_type, size_inches; show_steps=show_steps, save_steps_type=save_steps_type, legend_position=legend_position, add_name=add_name)

## agenda:
# turn off legend
# fix naming
# make the plots


"""
old
  # x_p_K_inc, y_mat_K_inc, stats_K_inc = tunepar([0.0,0.0,5.0,0.0],(0.0,200.0),50.0,[0.1,6.0,5.0],[0.1,30.0,5.0],[0.0,0.05,0.0],save_stats=false)
  # x_p_K_dec, y_mat_K_dec, stats_K_dec = tunepar([0.0,0.0,5.0,0.0],(0.0,200.0),50.0,[0.1,6.0,5.0],[0.1,0.0,5.0],[0.0,-0.05,0.0],save_stats=false)
  # stats_plot_K = Plots.scatter(vcat(x_p_K_inc, x_p_K_dec), vcat(stats_K_inc, stats_K_dec), xlabel=L"$K$", markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:bottomright)
  # vline!([6], label="start")
  # save("2osc_adi_Kuramoto_stats_K_0.05.png", stats_plot_K)

  ##
  function tunepar_both(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp; save_plot=false)
    x_p_inc, y_mat_inc, stats_inc = tunepar(ode_function, y0,tspan,t_stat,p0,p1_high,dp, show_stats=false, show_steps=false)
    x_p_dec, y_mat_dec, stats_dec = tunepar(ode_function, y0,tspan,t_stat,p0,p1_low,-dp, show_stats=false, show_steps=false)
    ind_dp = findall(>(0), broadcast(abs,dp) )
    np = length(dp)
    if np == 3 && length(ind_dp) == 1
      ind = ind_dp[1]
      symbol = ["α","K","n"][ind]
      dp_scalar = dp[ind]
    end
    start = p0[ind]
    stats_plot = Plots.scatter(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec), xlabel=symbol, markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:bottomright)
    vline!([start], label="start")
    display(stats_plot)
    if save_plot == true
      save("2osc_adi_Kuramoto_stats_"*symbol*"_"*string(dp_scalar)*".png", stats_plot)
    end
     x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot
  end


function tunepar_both(y0::Vector{Float64}, tspan::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1_low::Vector{Float64}, p_1_high::Vector{Float64}, dp::Vector{Float64}; save_plot=false)
    x_p_inc, y_mat_inc, stats_inc = tunepar(y0,tspan,t_stat,p_0,p_1_high,dp, show_stats=false, show_steps=false)
    x_p_dec, y_mat_dec, stats_dec = tunepar(y0,tspan,t_stat,p_0,p_1_low,-dp, show_stats=false, show_steps=false)
    ind_dp = findall(>(0), broadcast(abs,dp) )
    np = length(dp)
    if np == 3 && length(ind_dp) == 1
      ind = ind_dp[1]
      symbol = ["α","K","n"][ind]
      dp_scalar = dp[ind]
    end
    start = p_0[ind]
    size_inches = ((3+3/8), 2/3 * (3+3/8))
    size_pt = 72 .* size_inches
    f = Figure(resolution=size_pt, fontsize=12);
    ax = Axis(f[1,1], xlabel=symbol)
    Plots.scatter!(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec),
      markershape=:x, markersize=1,
      labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"])
    vline!([start], label="start")
    display(f)
    if save_plot == true
      save("2osc_adi_harm_simpler_stats_"*symbol*"_"*string(dp_scalar)*".pdf", f)
    end
     x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot
end
"""