"""
functions to...

# generate homogeneous synthetic networks (except for P)
# generate states that are operating states perturbed at single nodes
# simulate trajectories from initial states
# calculate statistics of tails

# phase_amplitude_oscillators.jl contains how to SwingEqLVS, SchifferApprox
# and (tbd) general normal form phase-amplitude PhaseAmplitudeOscillators
"""
#
using SyntheticNetworks
using StatsBase
using LightGraphs
# use this instead once TreeNodeClassification can deal with it using Graphs
using OrdinaryDiffEq
using Plots
using PowerDynamics

include(joinpath(@__DIR__,"phase_amplitude_oscillators.jl"))
include(joinpath(@__DIR__,"helper_functions.jl"))

"""
    random_PD_grid_Schiffer_normal_form

Generates a random power grid of size N with N Schiffer Approximation nodes (w/o slack node) using SyntheticNetworks and then turns it into a PowerDynamics.PowerGrid type.
"""

function random_PD_grid(N::Int, node_type, node_parameters, K; n_attempts=100, growth_parameters=[1, 1/5, 3/10, 1/3, 1/10, 0.0], active_power_distribution="random_half", balancing_method="None", balancing_parameters=[Nothing] )
    
    # decide node type and unpack parameters
    if node_type == SwingEqLVS
        H, P, D, Ω, Γ, V = node_parameters
        node_parameters_dict = Dict(:H=>H, :P=>P, :D=>D, :Ω=>Ω, :Γ=>Γ, :V=>V)
        elseif node_type == SchifferApprox
            τ_P, τ_Q, K_P, K_Q, V_r, P = node_parameters
            # so far :Q=>0 here, but this is changed below if balancing_method == "operationpoint"
            node_parameters_dict = Dict(:τ_P=>τ_P, :τ_Q=>τ_Q, :K_P=>K_P, :K_Q=>K_Q, :V_r=>V_r, :P=>P, :Q=>0, :Y_n=>0)
        else @assert (false) "node_type "*string(node_type)*" not yet implemented."
    end

    if active_power_distribution == "random_half"
        @assert (N%2)==0 "With balancing method random_half, N has to be even to ensure power balance with half of the nodes having power +(-)P." 
        P_vec = ones(N)
        turn_idx = sample(1:N, N÷2, replace=false)
        for n in turn_idx
            P_vec[n] = -1
        end
        P_vec = P .* P_vec
    else
        @assert (false) "Active power distribution "* active_power_distribution * "not yet implemented."
    end
    # P_vec = rand(Uniform(-1, 1), N)...

    n0, p, q, r, s, u = growth_parameters
    counter=1
    # n_attempts to generate a stable grid
    while counter <= n_attempts
        # Generates the random power grid graph using SyntheticNetworks
        # println("run $counter")
        RPG = RandomPowerGrid(N, n0, p, q, r, s, u)  # last parameter u is currently not implemented
        # use 
        pg_graph = graph_convert(generate_graph(RPG))
            # and LightGraphs.edges, LightGraphs.nv etc.
        # or import Graphs and use
        # Graphs.edges, graphs.nv, ...        
        e = LightGraphs.edges(pg_graph)

        # collects all edges and turns them into StaticLines
        from_vec = LightGraphs.src.(e)
        to_vec = LightGraphs.dst.(e)

        lines = Array{Any}(undef, length(e))
        nodes = Array{Any}(undef, LightGraphs.nv(pg_graph))

        for l in 1:length(e)
            lines[l] = StaticLine(from = from_vec[l], to = to_vec[l], Y = -1im*K) 
        end
        # nodes[end] = SlackAlgebraic(U = complex(1.0))
        # @assert sum(P_vec)==0 "power imbalance!"

        if balancing_method == "operationpoint" 
            for n in 1:LightGraphs.nv(pg_graph)
                nodes[n] = PVAlgebraic(P = P_vec[n], V = 1.0)
            end
            # nodes[1] = SlackAlgebraic(U = complex(1.0))
            pg_cons = PowerGrid(nodes, lines)
            op_cons = find_operationpoint(pg_cons, sol_method=:rootfind)#, solve_powerflow = true)
            if node_type == SchifferApprox
                node_parameters_dict = Dict(:τ_P=>τ_P, :τ_Q=>τ_Q, :K_P=>K_P, :K_Q=>K_Q, :V_r=>V_r, :P=>op_cons[n,:p], :Q=>op_cons[n, :q], :Y_n=>0)
            end
            newnodes = Array{Any}(undef, LightGraphs.nv(pg_graph))
            for n in 1:LightGraphs.nv(pg_graph)
                newnodes[n] = node_type(;node_parameters_dict...,P=P_vec[n])
            end
            
        elseif balancing_method == "None"
            for n in 1:LightGraphs.nv(pg_graph)
                nodes[n] = node_type(;node_parameters_dict...,P=P_vec[n])
            end
            newnodes = nodes
        else @assert (false) "Balancing method "* balancing_method * "not yet implemented."
            # newnodes[end] = SlackAlgebraic(U = complex(1.0))
        end

        pg = PowerGrid(newnodes, lines)
        op = find_operationpoint(pg, sol_method=:rootfind)#, solve_powerflow = true)
        
        # println(op_cons[:,:p])
        # println(sum(op_cons[:,:p]))
        # println(op_cons[:,:q])
        # println(sum(op_cons[:,:q]))
        
        # return pg, op
        # max = maximum(broadcast(abs,(op[:,:v].-1)))
        # println("max $max")

        #  ω ≈ 0 condition is new
        if all(isapprox.(op[:, :v], 1.0, atol=0.01)) && all(isapprox.(op[:, :ω], 0, atol=1e-9))
            # println("v is 1")
            # _, stable = check_eigenvalues(pg, op) # check if operation point is linearly stable
            _, stable = check_eigenvalues_MM(pg, op)
            # println("stable: $stable")
            if stable 
                sol = simulate_from_state(pg, op.vec, (0.,25.))
                # println(sol.retcode)
                if sol.retcode == :Success
                    return pg, op
                end
            end
        end
        counter += 1
    end
    print("unsuccessful!")
end 


# op[:] or (any state s) contains [u_r_1, u_i_1, ω_1, u_r_2, ..., ω_N]
# actually, op contains the grid too
# single elements [index k] can be accessed via
# op[1, :var, k]
# op[k, :u_r], op[k, :u_i], op[k, :ω]
# but also op[k, :u] and :v, :φ (varphi), :i, :iabs, :δ, :s, :p, :q
# currently one can only set u and v, the rest is derived (says the docs)
# -> set Δφ by u -> u * e^(im Δφ)


Base.@kwdef struct DummyPerturbation<:AbstractPerturbation
    tspan_fault
end

function (dp::DummyPerturbation)(powergrid)
    powergrid
end

# might not work for Slacks nodes or other types
# """
# single_node_perturbation(state, dvars, node_idx)

# returns a perturbed state "newstate" with perturbed ω and ϕ at node_idx
# """
# function single_node_perturbation(state, dvars, node_idx)
#     dϕ, dω = dvars
#     newstate = copy(state)
#     newstate[node_idx, :u] = state[node_idx, :u] * exp(im * dϕ)
#     newstate[node_idx, :ω] = state[node_idx, :ω] + dω
#     newstate
# end

# """
# single_node_perturbation_ϕ(state, dϕ, node_idx)

# returns a perturbed state "newstate" with perturbed ϕ at node_idx
# """
# function single_node_perturbation_ϕ(state, dϕ, node_idx)
#     newstate = copy(state)
#     newstate[node_idx, :u] = state[node_idx, :u] * exp(im * dϕ)
#     newstate
# end

# """
# single_node_perturbation_ω(state, dω, node_idx)

# returns a perturbed state "newstate" with perturbed ω at node_idx
# """
# function single_node_perturbation_ω(state, dω, node_idx)
#     newstate = copy(state)
#     newstate[node_idx, :ω] = state[node_idx, :ω] + dω
#     newstate
# end

"""
simulate_from_state(pg, state, tspan; abstol=1e-6, reltol=1e-3)
simulates the trajectories of the nodal variables starting from state and returns the solution object
"""
function simulate_from_state(pg, state, tspan; abstol = 1e-6, reltol = 1e-3)
    # abstol and reltol are defaults for odes according to https://diffeq.sciml.ai/dev/basics/common_solver_opts/
    # as opposed to Anna Büttner's code I set force_dtmin = false (default) because I want it exact and compatability
    problem = ODEProblem(rhs(pg), state, tspan)
    solution = solve(problem, Rodas4(), abstol=abstol, reltol=reltol)
    solution
end

# """
# simulate_from_perturbation_ϕ(pg, state, dϕ, node_idx, tspan; abstol=1e-6, reltol=1e-3)

# simulates the trajectories of the nodal variables starting from state with perturbation dϕ at node_idx and returns the solution object
# """
# function simulate_from_perturbation_ϕ(pg, state, dϕ, node_idx, tspan; abstol = 1e-6, reltol = 1e-3)
#     newstate = single_node_perturbation_ϕ(state, dϕ, node_idx)
#     solution = simulate_from_state(pg, newstate, tspan, abstol = abstol, reltol = reltol)
#     solution
# end

# """
# simulate_from_perturbation_ω(pg, state, dω, node_idx, tspan; abstol=1e-6, reltol=1e-3)

# simulates the trajectories of the nodal variables starting from state with perturbation dω at node_idx and returns the solution object
# """
# function simulate_from_perturbation_ω(pg, state, dω, node_idx, tspan; abstol = 1e-6, reltol = 1e-3)
#     newstate = single_node_perturbation_ω(state, dω, node_idx)
#     solution = simulate_from_state(pg, newstate, tspan, abstol = abstol, reltol = reltol)
#     solution
# end

# """
# simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; abstol=1e-6, reltol=1e-3)

# simulates the trajectories of the nodal variables starting from state with perturbation dvar using var_perturber_function at node_idx and returns the solution object
# """
# function simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; abstol = 1e-6, reltol = 1e-3)
#     newstate = var_perturber_function(state, dvar, node_idx)
#     solution = simulate_from_state(pg, newstate, tspan, abstol = abstol, reltol = reltol)
#     solution
# end

### since solve(problem, Rodas4) raises an error of unknown origin now,
### switch to PowerDynamics: simulate (that actually only calls solve...?!)
### use a DummyPerturbation and change the state beforehand...
### ...in order to apply several perturbations at once, e.g. φ and ω

# better to generate a matrix of all perturbations (a bit like dyadic product of dϕ_range and dω_range)
# can't come up with the name, so I used map() twice

#changed name: added _ϕ_ω
# todo write a general function that takes a vector of node_idx, var_symbols and dvars
"""
single_node_perturbation(state, dvars, node_idx)

returns a perturbed state "newstate" with perturbed ω and ϕ at node_idx
"""
function single_node_perturbation_ϕ_ω(state, dvars, node_idx)
    dϕ, dω = dvars
    newstate_ϕ = ChangeInitialConditions(node = node_idx, var = :φ, f = Inc(dϕ))(state)
    newstate_ϕ_ω = ChangeInitialConditions(node = node_idx, var = :ω, f = Inc(dω))(newstate_ϕ)
    newstate_ϕ_ω
end

"""
simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; abstol=1e-6, reltol=1e-3)

simulates the trajectories of the nodal variables starting from state with perturbation dvar using var_perturber_function at node_idx and returns the solution object
"""
# function simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; abstol = 1e-6, reltol = 1e-3)
function simulate_from_perturbation_var(pg, state, dvar, var_perturber_function, node_idx, tspan; kwargs...)
    newstate = var_perturber_function(state, dvar, node_idx)
    # solution = simulate(DummyPerturbation(tspan), pg, newstate, tspan, abstol = abstol, reltol = reltol)
    # solution = simulate(DummyPerturbation(tspan), pg, newstate, tspan; kwargs...)
    problem = ODEProblem(rhs(pg), newstate.vec, tspan)
    solution = solve(problem, Rodas4(); kwargs... )
    solution
end


function calculate_statistics_ω(solution, t_stat_iter)
    mean_ω =  mean(solution(150:0.1:200, idxs=3:3:300).u)
    std_ω = std(solution(150:0.1:200, idxs=3:3:300).u)
    mean_ω, std_ω
end

#function simulate_node(pg, op, perturbation_range_ϕ, perturbation_range_ω, tspan; abstol = 1e-6, reltol = 1e-3)
    
#end


function create_plot(sol)
    generator_indices = findall(bus -> typeof(bus) == SwingEqLVS, pg.nodes)
    labels = reshape(generator_indices,(1,length(generator_indices)))

    pl_v = Plots.plot(sol, generator_indices, :v, legend = (0.8, 0.7), ylabel="V [p.u.]", label=labels)
    pl_p = Plots.plot(sol, generator_indices, :p, legend = (0.8, 0.7), ylabel="p [p.u.]", label=labels)
    pl_q = Plots.plot(sol, generator_indices, :q, legend = (0.8, 0.7), ylabel="q [p.u.]", label=labels)
    pl_ω = Plots.plot(sol, generator_indices, :ω, legend = (0.8, 0.7), ylabel="ω [rad/s]", label=labels)

    pl = Plots.plot(pl_ω, pl_v, pl_p, pl_q;
            layout=(2,2),
            size = (1000, 500),
            lw=3,
            xlabel="t[s]")
end









