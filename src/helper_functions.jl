"""
Helper Functions needed for normal form simulations and network generation.
"""
#
using LinearAlgebra
using OrdinaryDiffEq
using ForwardDiff
using Plots
using Colors, DataFrames, ColorSchemes
using PowerDynamics
using Graphs
using GraphPlot, LightGraphs
using GraphMakie
using CairoMakie
using TreeNodeClassification
# using Plots.Measures

function check_eigenvalues_MM(pg::PowerGrid, s::State)
    rpg = rhs(pg)
    M = rpg.mass_matrix
    if M == true*I
        M = 1
    else M = Array(M)
    end
    f!(dx, x) = rpg(dx, x, nothing, 0.)
    j(x) = (dx = similar(x); ForwardDiff.jacobian(f!, dx, x))
    λ = eigvals(j(s.vec) * pinv(M) * M) .|> real |> extrema
    stable = isapprox(last(λ), 0, atol=1e-8)
    return λ, stable
end

# in Anna Büttners repo this one is in SNBS.jl
# don't use this one, use simulate_from_perturbation_var etc. from synthetic_networks.jl

# function simulate_pertubation(rpg, x0, tspan; tol = false)
#     problem = ODEProblem(rpg, x0, tspan)
#     if tol == false
#         solution = solve(problem, Rodas4(), force_dtmin = true)
#         return solution
#     else
#         solution = solve(problem, Rodas4(), force_dtmin = true, abstol = tol, reltol = tol)
#         return solution
#     end
# end

# there is a specific location pointed to in this fct in the normal-from repo:
#  filename = joinpath("c:/Users/niehu/Dropbox/UNI/Masterarbeit/Julia/normal-form/data/Ensemble/grid_" * string(pg_idx)*".json")
# from now on, pg_idx is not assigned ::Int, so that it can be a string or integer
"""
    find_power_grid(pg_idx)
Load the power grid from the data
"""
function find_power_grid(pg_idx)
    # load the power grid and all related stuff
    # filename = joinpath(@__DIR__,"../data/Ensemble/pg_method_Voltage_pg_idx_" * string(pg_idx))
    filename = joinpath(@__DIR__,"../data/ensemble/grid_" * string(pg_idx)*".json")
    pg = read_powergrid(filename, Json)
    return pg, rhs(pg), find_operationpoint(pg)
end

"""
    find_power_grid_path(ensemble_path, grid_name)
Load the power grid "grid_name" from the location ensemble_path
"""
function find_power_grid_path(ensemble_path, grid_name)
    if typeof(grid_name) != String # make integer arguments possible
        grid_name = "grid_" * string(grid_name)
    end
    # load the power grid and all related stuff
    # filename = joinpath(@__DIR__,"../data/Ensemble/pg_method_Voltage_pg_idx_" * string(pg_idx))
    filename = joinpath(ensemble_path, grid_name * ".json")
    pg = read_powergrid(filename, Json)
    return pg, rhs(pg), find_operationpoint(pg)
end

# this one is in plotting.jl in Anna Büttners repo
"""
    function my_graph_plot(pg::PowerGrid, df::DataFrame, pg_idx::Int, lable_nodes = [])

Using GraphMakie to plot some nice powergrids. Markersize is with respect to the SNBS. 
"""
# function my_graph_plot(pg::PowerGrid, df::DataFrame, pg_idx::Int, lable_nodes = [])
    # df_pg = df[df.PG_IDX .== pg_idx, :] # use only the subset of the data frame
function my_graph_plot(pg::PowerGrid, lable_nodes = [])
    num_nodes = length(pg.nodes)
    pa = palette(:tab10)
    node_color = fill(colorant"red", num_nodes)
    node_marker = fill(:utriangle, num_nodes)

    # Give each Class a specific Color
    pg_graph = graph_convert(pg.graph)
    classes =  full_node_classification(pg_graph, 100, 5)
    for i in 1:num_nodes
        # class = df_pg[i, :NodeClass]
        class = classes[i]
        if class == "Bulk"
            node_color[i] = pa[8]
        elseif class == "Root"
            node_color[i] = pa[6]
        elseif class == "Inner Tree Node"
            node_color[i] = pa[3]
        elseif class == "Proper Leave"
            node_color[i] = pa[9]
        elseif class == "Sparse Sprout"
            node_color[i] = pa[10]
        elseif class == "Dense Sprout"
            node_color[i] = pa[1]
        end
        # if df_pg[i, :Constraint] == 0.0
        #     node_marker[i] = :circle
        # end
        if pg.nodes[i].P < 0 # was == -1 for grid_1
            node_marker[i] = :rect
        elseif pg.nodes[i].P > 0 # was == 1 for grid_1
            node_marker[i] = :circle
        end
    end

    if lable_nodes != []
        node_lable = fill("", num_nodes)
        if length(lable_nodes) == 1
            node_lable[lable_nodes] = string(lable_nodes)
        else    
            map(n -> node_lable[n] = string(n), lable_nodes)
        end
        f, ax, p = graphplot(pg.graph, node_marker = node_marker, nlabels = node_lable, node_color = node_color, node_size = 20)
        # f, ax, p = gplot(pg.graph, node_marker = node_marker, nlabels = node_lable, node_color = node_color, node_size = 20)
    else
        f, ax, p = graphplot(pg.graph, node_marker = node_marker, node_color = node_color, node_size = 20)
        # f, ax, p = gplot(pg.graph, node_marker = node_marker, node_color = node_color, node_size = 20)
    end
    #some error occured here suddenly on Dec 8 2021
    hidedecorations!(ax); hidespines!(ax)
    ax.aspect = DataAspect()
    return f
end 

"""
graph_convert(graph)
converts a Graphs graph into a LightGraphs graph so that TreeNodeClassification works
"""
function graph_convert(graph)
    graph_edges = Graphs.edges(graph)

    light_graph = LightGraphs.SimpleGraphs.SimpleGraph(Graphs.nv(graph))
    for edge in graph_edges
        LightGraphs.add_edge!(light_graph, Graphs.src(edge), Graphs.dst(edge))
    end

    light_graph
end

"""
drop_index(cartesian_index_vector)

returns the cartesian_index_vector but with the last index dropped for each element
"""
# not used so far

function drop_index(cartesian_index_vector)
    len_vector = length(cartesian_index_vector)
    len_cartesian_index = length(cartesian_index_vector[1])
    new_vector = Vector{CartesianIndex{(len_cartesian_index-1)}}(undef, len_vector)
    for vector_idx in eachindex(cartesian_index_vector)
        cartesian_list = Vector{Int64}(undef, len_cartesian_index-1)
        for cartesian_idx in eachindex(cartesian_list)
            cartesian_list[cartesian_idx] = cartesian_index_vector[vector_idx][cartesian_idx]
        end
        new_vector[vector_idx] = CartesianIndex(cartesian_list...)
    end
    new_vector
end

"""
conditional_nodes_indexer(cartesian_idx_vector, stats_array, condition_cartesian_idx_vector, condition_range)

returns a vector where each element is a vector of elements of stats_array that fulfill:
the Cartesian coordinates specified by condition_range are the same for cartesian_idx_vector and condition_cartesian_idx_vector
e.g. find values of duett or solitary frequencies (see Kuramoto_ensemble_analysis.jl)
"""
function conditional_nodes_indexer(cartesian_idx_vector, stats_array, condition_cartesian_idx_vector, condition_range)# or ...condition_vector, condition_fct)
    len_condition_cartesian_idx_vector = length(condition_cartesian_idx_vector)
    out_stats = Vector{Vector{Float64}}(undef, len_condition_cartesian_idx_vector)
    for condition_vector_idx in eachindex(condition_cartesian_idx_vector)
        condition_idx_temp = CartesianIndex(Tuple(condition_cartesian_idx_vector[condition_vector_idx])[condition_range])
        #println(condition_idx_temp)
        out_stats_temp = Vector{Float64}(undef, 0)
        for vector_idx in eachindex(cartesian_idx_vector)
            idx_temp = CartesianIndex(Tuple(cartesian_idx_vector[vector_idx])[condition_range])
            #println(idx_temp)
            if idx_temp == condition_idx_temp
                append!(out_stats_temp, stats_array[cartesian_idx_vector[vector_idx]])
            end
        end
        out_stats[condition_vector_idx] = out_stats_temp
    end
    out_stats
end

"""
conditional_cartesian_idx_vector(cartesian_idx_vector, condition_cartesian_idx_vector, condition_range)

returns a vector where each element is a CartesianIndex that fulfills:
the Cartesian coordinates specified by condition_range are the same for cartesian_idx_vector and condition_cartesian_idx_vector
e.g. find values of duett or solitary frequencies (see Kuramoto_ensemble_analysis.jl)
"""
function conditional_cartesian_idx_vector(cartesian_idx_vector, condition_cartesian_idx_vector, condition_range)# or ...condition_vector, condition_fct)
    len_condition_cartesian_idx_vector = length(condition_cartesian_idx_vector)
    len_cartesian_idx = length(cartesian_idx_vector[1])
    # out_stats = Vector{Vector{Float64}}(undef, len_condition_cartesian_idx_vector)
    out_idx = Vector{Vector{CartesianIndex{len_cartesian_idx}}}(undef, len_condition_cartesian_idx_vector)
    for condition_vector_idx in eachindex(condition_cartesian_idx_vector)
        condition_idx_temp = CartesianIndex(Tuple(condition_cartesian_idx_vector[condition_vector_idx])[condition_range])
        #println(condition_idx_temp)
        # out_stats_temp = Vector{Float64}(undef, 0)
        out_idx_temp = Vector{CartesianIndex{len_cartesian_idx}}(undef, 0)
        for vector_idx in eachindex(cartesian_idx_vector)
            idx_temp = CartesianIndex(Tuple(cartesian_idx_vector[vector_idx])[condition_range])
            if idx_temp == condition_idx_temp
                append!(out_idx_temp, [cartesian_idx_vector[vector_idx]])
                #println(idx_temp)
            end
        end
        out_idx[condition_vector_idx] = out_idx_temp
        # out_stats[condition_vector_idx] = out_stats_temp
    end
    out_idx
end

"""
index_vectors(cartesian_index_vector)
returns the "columns" of the cartesian_index_vector
"""

function index_vectors(cartesian_index_vector)
    len_vector = length(cartesian_index_vector)
    len_cartesian_index = length(cartesian_index_vector[1])
    new_vectors = Vector{Vector{Int64}}(undef, len_cartesian_index)
    for cartesian_idx in 1:len_cartesian_index
        new_vector = Vector{Int64}(undef, len_vector)
        for vector_idx in eachindex(cartesian_index_vector)
            new_vector[vector_idx] = cartesian_index_vector[vector_idx][cartesian_idx]
        end
        new_vectors[cartesian_idx] = new_vector
    end
    new_vectors
end

"""
all_nodes_indexer(cartesian_idx_vector, stats_array)
returns all values of stats_array where all but the last coordinate coincides with all but the last coordinates in cartesian_idx_vector
"""

function all_nodes_indexer(cartesian_idx_vector, stats_array)
    len_vector = length(cartesian_idx_vector)
    len_cartesian_idx = length(cartesian_idx_vector[1])
    out_stats = Array{Float64}(undef, (len_vector, size(stats_array)[end] ) )
    for vector_idx in eachindex(cartesian_idx_vector)
        idx_temp = Vector{Int64}(undef, len_cartesian_idx-1)
        for cartesian_idx in eachindex(idx_temp)
            idx_temp[cartesian_idx] = cartesian_idx_vector[vector_idx][cartesian_idx]
        end
        out_stats[vector_idx,:] = stats_array[idx_temp...,:]
    end
    out_stats
end


"""
color_nodes(pg::PowerGrid)
returns vector of colors according to the topological node classes
The classes are obtained with tree_node_classification
"""
# function that 
function color_nodes(pg::PowerGrid)
    N = length(pg.nodes)
    pg_graph = graph_convert(pg.graph)
    classes =  full_node_classification(pg_graph, 100, 5) # last argument is "<=" threshold for sparse sprouts
    pa = palette(:tab10)
    node_color = fill(colorant"red", N)
    for i in 1:N
        class = classes[i]
        if class == "Bulk"
            node_color[i] = pa[8]
        elseif class == "Root"
            node_color[i] = pa[6]
        elseif class == "Inner Tree Node"
            node_color[i] = pa[3]
        elseif class == "Proper Leave"
            node_color[i] = pa[9]
        elseif class == "Sparse Sprout"
            node_color[i] = pa[10]
        elseif class == "Dense Sprout"
            node_color[i] = pa[1]
        end 
    end
    node_color
end

"""
power_shape_nodes(pg::PowerGrid)
returns vector of shapes according to the nodes being generators or consumers
"""
function power_shape_nodes(pg::PowerGrid)
    N = length(pg.nodes)
    node_marker = fill(:utriangle, N)
    for i in 1:N
        if pg.nodes[i].P < 0 # consumer
            node_marker[i] = :rect
        elseif pg.nodes[i].P > 0 # generator
            node_marker[i] = :circle
        end
    end
    node_marker
end
