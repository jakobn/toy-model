"""
plots of self-consistency function veriosn 221209, with elimination of unknown ⟨p⟩ to get best fit
"""
#
using JLD
using FileIO
using CairoMakie

# test different approximations of Laplacian with static and overrotating coordinates
γ(n, α, Ω, K) = sqrt(1 - (α*Ω/(K*(n+1)))^2)
# ω_cor(N) = N ./ (N.-1)
ω_cor(n, v) = v .* (n.+2) ./ (n.+1)
ZstarbestLgen(n, α, Ω, K, v, m, γ) = α*Ω .- α*v .+ K^2/2 * (-α*ω_cor(n, v)) .* (1 ./((1 .+ n) .* (α^2 *ω_cor(n, v).^2 .+ m^2 *ω_cor(n, v).^4)) .+ (n .+ 1)./((n) .* ((γ * K *(1 .+ n) .- m * ω_cor(n, v).^2).^2 .+ α^2 * ω_cor(n, v).^2))) 
ZstarbestLstat(n, α, Ω, K, v, m) = ZstarbestLgen(n, α, Ω, K, v, m, 1)
ZstarbestLover(n, α, Ω, K, v, m) = ZstarbestLgen(n, α, Ω, K, v, m, γ(n, α, Ω, K))

ZstarbestLstate(n, v) = ZstarbestLstat(n, 0.1, 10, 6, v, 1)
ZstarbestLovere(n, v) = ZstarbestLover(n, 0.1, 10, 6, v, 1) 

#### Plots with data
savename_full = "three_osc_star_full_fullstats_ic_grid"
af_full = load("data//"*savename_full*".jld")["data"]

savename_lin = "three_osc_lin_fullstats_ic_grid"
af_lin = load("data//"*savename_lin*".jld")["data"]

savename_lin_simpler = "three_osc_lin_simpler_fullstats_ic_grid"
af_lin_simpler = load("data//"*savename_lin_simpler*".jld")["data"]

n_initial_ϕ = 10
n_initial_ω = 100

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

alpha = 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)
color5 = (:blue, alpha)
marker1 = :circle
marker3 = :x
marker5 = :+

size1 = 6 *scaler # 9
size3 = 12 *scaler # 18
size5 = 12 *scaler

K = 6

ns = 0.:0.2:30.
d_min = ns[1] # - .5
d_max = ns[end] # + .5
vs_finest = 0.003333:0.03333:10.5 # 0.003333:0.003333:9.999
ns_finest = 0 : 0.03 : 30 # 0 : 0.003 : 30
# ns_finest_nonzero = 1/K + 0.03 : 0.03 : 30 # 0 : 0.003 : 30


### Lstat

Z_star_best_Lstat_matrix = broadcast(ZstarbestLstate, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_star_best_Lstat_matrix = log10.(abs.(Z_star_best_Lstat_matrix))
# make heatmaps for pos and neg part separately
Z_star_best_Lstat_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_best_Lstat_matrix )
Z_star_best_Lstat_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_best_Lstat_matrix )
log_Z_star_best_Lstat_matrix_pos = log10.(Z_star_best_Lstat_matrix_pos)
log_Z_star_best_Lstat_matrix_neg = log10.(-Z_star_best_Lstat_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lstat_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_best_Lstat_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)


## with only full dynamics
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lstat_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full], ["⟨ω₁⟩ simulation"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_best_Lstat_comparison_ic_grid_fullstats"*"_freqs_only_full_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)



### Lover

Z_star_best_Lover_matrix = broadcast(ZstarbestLovere, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_star_best_Lover_matrix = log10.(abs.(Z_star_best_Lover_matrix))
# make heatmaps for pos and neg part separately
Z_star_best_Lover_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_star_best_Lover_matrix )
Z_star_best_Lover_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_star_best_Lover_matrix )
log_Z_star_best_Lover_matrix_pos = log10.(Z_star_best_Lover_matrix_pos)
log_Z_star_best_Lover_matrix_neg = log10.(-Z_star_best_Lover_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lover_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_best_Lover_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)

# no notable difference between the two laplacians

# only full data
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lover_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full], ["⟨ω₁⟩ simulation"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_star_best_Lover_comparison_ic_grid_fullstats"*"_freqs_only_full_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)





### all to all
ZallbestLgen(n, α, Ω, K, v, m, γ) = α*Ω .- α*v .+ K^2/2 * (-α*ω_cor(n, v)) .* (1 ./((1 .+ n) .* (α^2 *ω_cor(n, v).^2 .+ m^2 *ω_cor(n, v).^4)) .+ (n)./((2) .* ((γ * K *(1 .+ n) .- m * ω_cor(n, v).^2).^2 .+ α^2 * ω_cor(n, v).^2))) 
ZallbestLstat(n, α, Ω, K, v, m) = ZallbestLgen(n, α, Ω, K, v, m, 1)
ZallbestLover(n, α, Ω, K, v, m) = ZallbestLgen(n, α, Ω, K, v, m, γ(n, α, Ω, K))

ZallbestLstate(n, v) = ZallbestLstat(n, 0.1, 10, 6, v, 1)
ZallbestLovere(n, v) = ZallbestLover(n, 0.1, 10, 6, v, 1)

### Lstat

Z_all_best_Lstat_matrix = broadcast(ZallbestLstate, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_all_best_Lstat_matrix = log10.(abs.(Z_all_best_Lstat_matrix))
# make heatmaps for pos and neg part separately
Z_all_best_Lstat_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_best_Lstat_matrix )
Z_all_best_Lstat_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_best_Lstat_matrix )
log_Z_all_best_Lstat_matrix_pos = log10.(Z_all_best_Lstat_matrix_pos)
log_Z_all_best_Lstat_matrix_neg = log10.(-Z_all_best_Lstat_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lstat_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_all_best_Lstat_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)


## with only full dynamics
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lstat_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full], ["⟨ω₁⟩ simulation"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_all_best_Lstat_comparison_ic_grid_fullstats"*"_freqs_only_full_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)



### Lover

Z_all_best_Lover_matrix = broadcast(ZallbestLovere, collect(ns_finest), transpose(collect(vs_finest)))
log_Z_all_best_Lover_matrix = log10.(abs.(Z_all_best_Lover_matrix))
# make heatmaps for pos and neg part separately
Z_all_best_Lover_matrix_pos = broadcast( x->( (x >= 0) ? x : (x=missing)), Z_all_best_Lover_matrix )
Z_all_best_Lover_matrix_neg = broadcast( x->( (x < 0) ? x : (x=missing)), Z_all_best_Lover_matrix )
log_Z_all_best_Lover_matrix_pos = log10.(Z_all_best_Lover_matrix_pos)
log_Z_all_best_Lover_matrix_neg = log10.(-Z_all_best_Lover_matrix_neg)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lover_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1]);

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_all_best_Lover_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)

# no notable difference between the two laplacians

# only full data
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")

hm_neg = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lover_matrix_neg, cscale=log10);
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1);
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full], ["⟨ω₁⟩ simulation"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
Colorbar(f_ω[1,2], hm_neg, label=L"\ln (-Z^{-})")

f_ω
save("three_osc_all_best_Lover_comparison_ic_grid_fullstats"*"_freqs_only_full_sprout_self_consistency"*".png", f_ω, px_per_unit = 4)



### comparison

α = 0.1
K = 6.0
Ω = 10.0
v_range = 0.01:0.01:9.99

n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
hm_neg_star = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lstat_matrix_neg, colormap=(:gnuplot,0.8), cscale=log10);
hm_neg_all = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lstat_matrix_neg, colormap=(:viridis,0.6), cscale=log10);
Colorbar(f_ω[1,1], hm_neg_star, label=L"\ln (-Z_*^{-})", halign=0.85, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
Colorbar(f_ω[1,1], hm_neg_all, label=L"\ln (-Z_{all}^{-})", halign=0.65, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3, linestyle=:dash)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3, linestyle=:dash)
Legend(f_ω[1,1], [ω_plus, ω_minus], ["ω₊", "ω₋"], halign=0.45, valign=0.35, tellwidth=false, tellheight=false)
f_ω
save("comparison_self_consistency_different_models_static_v3.png", f_ω, px_per_unit=4)




### with data from three node model
# make alpha smaller
alpha = 0.01 # 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)
color5 = (:blue, alpha)


f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
hm_neg_star = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lstat_matrix_neg, colormap=(:gnuplot,0.8), cscale=log10);
hm_neg_all = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lstat_matrix_neg, colormap=(:viridis,0.6), cscale=log10);
Colorbar(f_ω[1,1], hm_neg_star, label=L"\ln (-Z_*^{-})", halign=0.85, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
Colorbar(f_ω[1,1], hm_neg_all, label=L"\ln (-Z_{all}^{-})", halign=0.65, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3, linestyle=:dash)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3, linestyle=:dash)

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler, ω_plus, ω_minus], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim", "ω₊", "ω₋"],
    bgcolor=false, framevisible=false,
  halign=0.47, valign=0.2, tellwidth=false, tellheight=false)
f_ω
save("comparison_self_consistency_different_models_with_data_v3.png", f_ω, px_per_unit=4)

# only full data
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Makie.Axis(f_ω[1, 1], xlabel=L"$n$", ylabel=L"\omega_s")
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
hm_neg_star = Makie.heatmap!(ns_finest, vs_finest, log_Z_star_best_Lstat_matrix_neg, colormap=(:gnuplot,0.8), cscale=log10);
hm_neg_all = Makie.heatmap!(ns_finest, vs_finest, log_Z_all_best_Lstat_matrix_neg, colormap=(:viridis,0.6), cscale=log10);
Colorbar(f_ω[1,1], hm_neg_star, label=L"\ln (-Z_*^{-})", halign=0.85, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
Colorbar(f_ω[1,1], hm_neg_all, label=L"\ln (-Z_{all}^{-})", halign=0.65, valign=0.35, height=size_pt[2]*0.5, tellwidth=false, tellheight=false)
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3, linestyle=:dash)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3, linestyle=:dash)

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

Legend(f_ω[1,1], [mean_ω_1_full, ω_plus, ω_minus], ["⟨ω₁⟩ simul.", "ω₊", "ω₋"],# bgcolor=false, framevisible=false,
  halign=0.45, valign=0.2, tellwidth=false, tellheight=false)
f_ω
save("comparison_self_consistency_different_models_with_data_only_full_v3.png", f_ω, px_per_unit=4)
