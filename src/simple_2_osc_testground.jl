"""
Testground for two-oscillator system.
Simulation and Plotting.
"""
##

using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings

##


function two_osc!(dy, y, p, t)
  α, K, n = p
  @views begin
    ϕ = y[1:2]
    ω = y[3:4]
    dϕ = dy[1:2]
    dω = dy[3:4]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
  dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2])
  nothing
end

##

"""
These should not be used anymore, instead use tools (write them if necessary)


function simplot(y0, tspan, p; saveit=true)
  prob = ODEProblem(two_osc!, y0, tspan, p)
  sol = solve(prob, Tsit5())
  plt1 = Plots.plot(sol,vars=(0,1))
  plt2 = Plots.plot(sol,vars=(0,2))
  plt3 = Plots.plot(sol,vars=(0,3))
  plt4 = Plots.plot(sol,vars=(0,4))
  plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
  if saveit == true
    α, K, n = p
    name = "2osc_Kuramoto_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
    save(name, plt)
  end
  plt
end

##

function tunepar(y_0::Vector{Float64}, t_span::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1::Vector{Float64}, dp::Vector{Float64}; show_steps=false, save_steps=false, show_stats=true, save_stats= true, save_stats_name="") 
  @assert t_stat <= t_span[2] - t_span[1]
  np = length(p_0)
  @assert np == 3 "np!=3 is not implemented concerning plot axis"
  @assert (broadcast(abs, p_1-(p_0+dp)) .<= broadcast(abs, p_1-p_0)) == ones(np) "dp does not bring p_0 closer to p_1"
  where_dp = (dp .!= 0)
  runs_vec = broadcast( x -> ceil(Int,x), (p_1[where_dp] - p_0[where_dp]) ./ dp[where_dp])
  @assert runs_vec == ones(length(runs_vec))*runs_vec[1] "not all parameters require the same amount of steps of dp to reach p_1"
  runs = runs_vec[1] + 1 # for the zeroth calculation

  p = p_0
  
  y_mat = zeros(runs+1, length(y_0))
  y_mat[1,:] = y_0
  stats = zeros(runs, 4)
  p_mat = zeros(runs,np)

  t_end = t_span[2]
  t_start = t_end - t_stat
  t_step = t_stat/1000

  run = 1
  # delta = p_1[where_dp]./dp[where_dp] * 10 * eps(Float64) # to stop p is below p1 by rounding error
  # while ( broadcast(abs, p_1+2*dp-p-delta) .> broadcast(abs, dp) ) != zeros(np)
  # does not work: delta has wrong dimensions... (take min or max?)
    # +dp to be sure to overshoot target a little if (p_1-p_0)/dp is not a vector filled with the same int;
    # +dp to actually execute the loop after overshooting for the first time
  while run <= runs
    # print(runs)
    print("\r $run of $runs")
    # print(p)
    y = y_mat[run,:]
    prob = ODEProblem(two_osc!, y, t_span, p)
    sol = solve(prob, Tsit5())
    y_mat[run+1,:] = sol.u[end]
 
    stats[run,:] = [mean(sol(t_start:t_step:t_end, idxs=3).u), std(sol(t_start:t_step:t_end, idxs=3).u), mean(sol(t_start:t_step:t_end, idxs=4).u), std(sol(t_start:t_step:t_end, idxs=4).u)]
    p_mat[run,:] = p

    if show_steps == true
      plt1 = Plots.plot(sol,vars=(0,1))
      plt2 = Plots.plot(sol,vars=(0,2))
      plt3 = Plots.plot(sol,vars=(0,3))
      plt4 = Plots.plot(sol,vars=(0,4))
      plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
      if save_steps == true
        α, K, n = p
        name = "2osc_adi_Kuramoto_"*string(run)*"_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
        save(name, plt)
      end
    end
    # display(plt)

    p += dp
    run += 1
  end

  dir = ""
  ind_dp = findall(>(0), broadcast(abs,dp) )
  if np == 3 && length(ind_dp) == 1
    ind = ind_dp[1]
    symbol = ["α","K","n"][ind]
    dp_scalar = dp[ind]
    if dp_scalar > 0
      dir = symbol*"_inc"
    elseif dp_scalar < 0
      dir = symbol*"_dec"
    end
    

    x_p = p_mat[:,ind]
  else
    symbol = "# sample"
    x_p = 1:runs
  end
  if show_stats == true
    stats_plot = Plots.scatter(x_p, stats, xlabel=symbol, markershape=:x, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"])

    if save_stats_name != ""
      save_stats_name = "_"*save_stats_name
    end

    if save_stats == true
      save("2osc_adi_Kuramoto_stats_"*dir*"_"*string(dp_scalar)*save_stats_name*".png", stats_plot)
    end
    display(stats_plot)
  end
  x_p, y_mat, stats

end

# x_p_K_inc, y_mat_K_inc, stats_K_inc = tunepar([0.0,0.0,5.0,0.0],(0.0,200.0),50.0,[0.1,6.0,5.0],[0.1,30.0,5.0],[0.0,0.05,0.0],save_stats=false)
# x_p_K_dec, y_mat_K_dec, stats_K_dec = tunepar([0.0,0.0,5.0,0.0],(0.0,200.0),50.0,[0.1,6.0,5.0],[0.1,0.0,5.0],[0.0,-0.05,0.0],save_stats=false)
# stats_plot_K = Plots.scatter(vcat(x_p_K_inc, x_p_K_dec), vcat(stats_K_inc, stats_K_dec), xlabel=L"$K$", markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:bottomright)
# vline!([6], label="start")
# save("2osc_adi_Kuramoto_stats_K_0.05.png", stats_plot_K)

##
function tunepar_both(y_0::Vector{Float64}, t_span::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1_low::Vector{Float64}, p_1_high::Vector{Float64}, dp::Vector{Float64}; save_plot=false)
  x_p_inc, y_mat_inc, stats_inc = tunepar(y_0,t_span,t_stat,p_0,p_1_high,dp, show_stats=false, show_steps=false)
  x_p_dec, y_mat_dec, stats_dec = tunepar(y_0,t_span,t_stat,p_0,p_1_low,-dp, show_stats=false, show_steps=false)
  ind_dp = findall(>(0), broadcast(abs,dp) )
  np = length(dp)
  if np == 3 && length(ind_dp) == 1
    ind = ind_dp[1]
    symbol = ["α","K","n"][ind]
    dp_scalar = dp[ind]
  end
  start = p_0[ind]
  stats_plot = Plots.scatter(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec), xlabel=symbol, markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:bottomright)
  vline!([start], label="start")
  display(stats_plot)
  if save_plot == true
    save("2osc_adi_Kuramoto_stats_"*symbol*"_"*string(dp_scalar)*".png", stats_plot)
  end
   x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot
end

#######

# sep stands for seperated: you can tune the parameters before the 1-2-coupling and 2-3-couling seperately

function two_osc_sep!(dy, y, p, t)
  α, K, Kn = p
  @views begin
    ϕ = y[1:2]
    ω = y[3:4]
    dϕ = dy[1:2]
    dω = dy[3:4]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
  dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - Kn * sin(ϕ[2])
  nothing
end

##

function simplot_sep(y0, tspan, p; saveit=true)
  prob = ODEProblem(two_osc_sep!, y0, tspan, p)
  sol = solve(prob, Tsit5())
  plt1 = Plots.plot(sol,vars=(0,1))
  plt2 = Plots.plot(sol,vars=(0,2))
  plt3 = Plots.plot(sol,vars=(0,3))
  plt4 = Plots.plot(sol,vars=(0,4))
  plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
  if saveit == true
    α, K, n = p
    name = "2osc_Kuramoto_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
    save(name, plt)
  end
  plt
end

##

function tunepar_sep(y_0::Vector{Float64}, t_span::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1::Vector{Float64}, dp::Vector{Float64}; show_steps=false, save_steps=false, show_stats=true, save_stats= true, save_stats_name="") 
  @assert t_stat <= t_span[2] - t_span[1]
  np = length(p_0)
  @assert np == 3 "np!=3 is not implemented concerning plot axis"
  @assert (broadcast(abs, p_1-(p_0+dp)) .<= broadcast(abs, p_1-p_0)) == ones(np) "dp does not bring p_0 closer to p_1"
  where_dp = (dp .!= 0)
  runs_vec = broadcast( x -> ceil(Int,x), (p_1[where_dp] - p_0[where_dp]) ./ dp[where_dp])
  @assert runs_vec == ones(length(runs_vec))*runs_vec[1] "not all parameters require the same amount of steps of dp to reach p_1"
  runs = runs_vec[1] + 1 # for the zeroth calculation

  p = p_0
  
  y_mat = zeros(runs+1, length(y_0))
  y_mat[1,:] = y_0
  stats = zeros(runs, 4)
  p_mat = zeros(runs,np)

  t_end = t_span[2]
  t_start = t_end - t_stat
  t_step = t_stat/1000

  run = 1
  # delta = p_1[where_dp]./dp[where_dp] * 10 * eps(Float64) # to stop p is below p1 by rounding error
  # while ( broadcast(abs, p_1+2*dp-p-delta) .> broadcast(abs, dp) ) != zeros(np)
  # does not work: delta has wrong dimensions... (take min or max?)
    # +dp to be sure to overshoot target a little if (p_1-p_0)/dp is not a vector filled with the same int;
    # +dp to actually execute the loop after overshooting for the first time
  while run <= runs
    # print(runs)
    print("\r $run of $runs")
    # print(p)
    y = y_mat[run,:]
    prob = ODEProblem(two_osc_sep!, y, t_span, p)
    sol = solve(prob, Tsit5())
    y_mat[run+1,:] = sol.u[end]
 
    stats[run,:] = [mean(sol(t_start:t_step:t_end, idxs=3).u), std(sol(t_start:t_step:t_end, idxs=3).u), mean(sol(t_start:t_step:t_end, idxs=4).u), std(sol(t_start:t_step:t_end, idxs=4).u)]
    p_mat[run,:] = p

    if show_steps == true
      plt1 = Plots.plot(sol,vars=(0,1))
      plt2 = Plots.plot(sol,vars=(0,2))
      plt3 = Plots.plot(sol,vars=(0,3))
      plt4 = Plots.plot(sol,vars=(0,4))
      plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
      if save_steps == true
        α, K, n = p
        name = "2osc_adi_Kuramoto_"*string(run)*"_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
        save(name, plt)
      end
    end
    # display(plt)

    p += dp
    run += 1
  end

  dir = ""
  ind_dp = findall(>(0), broadcast(abs,dp) )
  if np == 3 && length(ind_dp) == 1
    ind = ind_dp[1]
    symbol = ["α","K","n"][ind]
    dp_scalar = dp[ind]
    if dp_scalar > 0
      dir = symbol*"_inc"
    elseif dp_scalar < 0
      dir = symbol*"_dec"
    end
    

    x_p = p_mat[:,ind]
  else
    symbol = "# sample"
    x_p = 1:runs
  end
  if show_stats == true
    stats_plot = Plots.scatter(x_p, stats, xlabel=symbol, markershape=:x, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"])

    if save_stats_name != ""
      save_stats_name = "_"*save_stats_name
    end

    if save_stats == true
      save("2osc_adi_Kuramoto_stats_"*dir*"_"*string(dp_scalar)*save_stats_name*".png", stats_plot)
    end
    display(stats_plot)
  end
  x_p, y_mat, stats

end

##

function tunepar_both_sep(y_0::Vector{Float64}, t_span::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1_low::Vector{Float64}, p_1_high::Vector{Float64}, dp::Vector{Float64}; save_plot=false)
  x_p_inc, y_mat_inc, stats_inc = tunepar_sep(y_0,t_span,t_stat,p_0,p_1_high,dp, show_stats=false, show_steps=false)
  x_p_dec, y_mat_dec, stats_dec = tunepar_sep(y_0,t_span,t_stat,p_0,p_1_low,-dp, show_stats=false, show_steps=false)
  ind_dp = findall(>(0), broadcast(abs,dp) )
  np = length(dp)
  if np == 3 && length(ind_dp) == 1
    ind = ind_dp[1]
    symbol = ["α","K","Kn"][ind]
    dp_scalar = dp[ind]
  end
  start = p_0[ind]
  stats_plot = Plots.scatter(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec), xlabel=symbol, markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:topright)
  vline!([start], label="start")
  display(stats_plot)
  if save_plot == true
    save("2osc_sep_adi_Kuramoto_stats_"*symbol*"_"*string(dp_scalar)*".png", stats_plot)
  end
   x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot
end

#######



#######

# ext stands for extended: you can tune the parameter P now

function two_osc_ext!(dy, y, p, t)
  α, K, n, P = p
  @views begin
    ϕ = y[1:2]
    ω = y[3:4]
    dϕ = dy[1:2]
    dω = dy[3:4]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dω[1] = P - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
  dω[2] = -P - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2])
  nothing
end

##

function simplot_ext(y0, tspan, p; saveit=true)
  prob = ODEProblem(two_osc_ext!, y0, tspan, p)
  sol = solve(prob, Tsit5())
  plt1 = Plots.plot(sol,vars=(0,1))
  plt2 = Plots.plot(sol,vars=(0,2))
  plt3 = Plots.plot(sol,vars=(0,3))
  plt4 = Plots.plot(sol,vars=(0,4))
  plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
  if saveit == true
    α, K, n, P = p
    name = "2osc_Kuramoto_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
    save(name, plt)
  end
  plt
end

##

function tunepar_ext(y_0::Vector{Float64}, t_span::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1::Vector{Float64}, dp::Vector{Float64}; show_steps=false, save_steps=false, show_stats=true, save_stats= true, save_stats_name="") 
  @assert t_stat <= t_span[2] - t_span[1]
  np = length(p_0)
  @assert np == 4 "np!=4 is the wrong dimension"
  @assert (broadcast(abs, p_1-(p_0+dp)) .<= broadcast(abs, p_1-p_0)) == ones(np) "dp does not bring p_0 closer to p_1"
  where_dp = (dp .!= 0)
  runs_vec = broadcast( x -> ceil(Int,x), (p_1[where_dp] - p_0[where_dp]) ./ dp[where_dp])
  @assert runs_vec == ones(length(runs_vec))*runs_vec[1] "not all parameters require the same amount of steps of dp to reach p_1"
  runs = runs_vec[1] + 1 # for the zeroth calculation

  p = p_0
  
  y_mat = zeros(runs+1, length(y_0))
  y_mat[1,:] = y_0
  stats = zeros(runs, 4)
  p_mat = zeros(runs,np)

  t_end = t_span[2]
  t_start = t_end - t_stat
  t_step = t_stat/1000

  run = 1
  # delta = p_1[where_dp]./dp[where_dp] * 10 * eps(Float64) # to stop p is below p1 by rounding error
  # while ( broadcast(abs, p_1+2*dp-p-delta) .> broadcast(abs, dp) ) != zeros(np)
  # does not work: delta has wrong dimensions... (take min or max?)
    # +dp to be sure to overshoot target a little if (p_1-p_0)/dp is not a vector filled with the same int;
    # +dp to actually execute the loop after overshooting for the first time
  while run <= runs
    # print(runs)
    print("\r $run of $runs")
    # print(p)
    y = y_mat[run,:]
    prob = ODEProblem(two_osc_ext!, y, t_span, p)
    sol = solve(prob, Tsit5())
    y_mat[run+1,:] = sol.u[end]
 
    stats[run,:] = [mean(sol(t_start:t_step:t_end, idxs=3).u), std(sol(t_start:t_step:t_end, idxs=3).u), mean(sol(t_start:t_step:t_end, idxs=4).u), std(sol(t_start:t_step:t_end, idxs=4).u)]
    p_mat[run,:] = p

    if show_steps == true
      plt1 = Plots.plot(sol,vars=(0,1))
      plt2 = Plots.plot(sol,vars=(0,2))
      plt3 = Plots.plot(sol,vars=(0,3))
      plt4 = Plots.plot(sol,vars=(0,4))
      plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
      if save_steps == true
        α, K, n, P = p
        name = "2osc_adi_Kuramoto_"*string(run)*"_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
        save(name, plt)
      end
      display(plt)
    end

    p += dp
    run += 1
  end

  dir = ""
  ind_dp = findall(>(0), broadcast(abs,dp) )
  if np == 4 && length(ind_dp) == 1
    ind = ind_dp[1]
    symbol = ["α","K","n","P"][ind]
    dp_scalar = dp[ind]
    if dp_scalar > 0
      dir = symbol*"_inc"
    elseif dp_scalar < 0
      dir = symbol*"_dec"
    end
    

    x_p = p_mat[:,ind]
  else
    symbol = "# sample"
    x_p = 1:runs
  end
  if show_stats == true
    stats_plot = Plots.scatter(x_p, stats, xlabel=symbol, markershape=:x, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"])

    if save_stats_name != ""
      save_stats_name = "_"*save_stats_name
    end

    if save_stats == true
      save("2osc_adi_Kuramoto_stats_"*dir*"_"*string(dp_scalar)*save_stats_name*".png", stats_plot)
    end
    display(stats_plot)
  end
  x_p, y_mat, stats

end

##

function tunepar_both_ext(y_0::Vector{Float64}, t_span::Tuple{Float64, Float64}, t_stat::Float64, p_0::Vector{Float64}, p_1_low::Vector{Float64}, p_1_high::Vector{Float64}, dp::Vector{Float64}; save_plot=false)
  x_p_inc, y_mat_inc, stats_inc = tunepar_ext(y_0,t_span,t_stat,p_0,p_1_high,dp, show_stats=false, show_steps=false)
  x_p_dec, y_mat_dec, stats_dec = tunepar_ext(y_0,t_span,t_stat,p_0,p_1_low,-dp, show_stats=false, show_steps=false)
  ind_dp = findall(>(0), broadcast(abs,dp) )
  np = length(dp)
  if np == 4 && length(ind_dp) == 1
    ind = ind_dp[1]
    symbol = ["α","K","n","P"][ind]
    dp_scalar = dp[ind]
  end
  start = p_0[ind]
  stats_plot = Plots.scatter(vcat(x_p_inc, x_p_dec), vcat(stats_inc, stats_dec), xlabel=symbol, markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:topright)
  vline!([start], label="start")
  display(stats_plot)
  if save_plot == true
    save("2osc_ext_adi_Kuramoto_stats_"*symbol*"_"*string(dp_scalar)*"_"*string(start)*".png", stats_plot)
  end
   x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot
end


"""



#######

function sample_asymptotic_frequencies(p)
  # y0 = 5* randn(4)
  y0 = vcat( 2*pi*rand(2), 20*rand()-5, -(20*rand()-5))
  prob = ODEProblem(two_osc!,y0,(0., 1000.),p)
  sol = solve(prob, Tsit5())
  mean(sol(700.:0.1:1000., idxs=3).u), mean(sol(700.:0.1:1000., idxs=4).u)
end

##

function asymptotic_freqs(ns, N_per_n, α, K)
  af = zeros(2, length(ns), N_per_n)
  print("\n Simulating")
  for (i_n, n) in enumerate(ns)
    print("\r Simulating $n ")
    for i_N in 1:N_per_n
      af[1, i_n, i_N], af[2, i_n, i_N] = sample_asymptotic_frequencies([α, K, n])  
    end
  end
  af
end

##

α = 0.1
K = 6.0
ns = 0.:0.2:30.
N_per_n = 100
af = asymptotic_freqs(ns, N_per_n, α, K)

##

using CairoMakie
using LaTeXStrings

##

# Can't make alpha to small or it will round to zero when cast to RGBA.
alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
color = (:black, alpha)

marker_size = 2

size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
# half the size does not work, looks stupid
size_pt = 72 .* size_inches
f = Figure(resolution=size_pt, fontsize=12);
# f = Figure(resolution=(1200, 800))
Axis(f[1, 1], ylabel=L"$\langle\omega_1\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  # Makie.scatter!(ns, af[1,:,i_N]; color = color, legend = false)
  Makie.scatter!(ns, af[1,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[2, 1], ylabel=L"$\langle\omega_2\rangle$", xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[2,:,i_N]; color = color, markersize = marker_size)
end

f

save("bif_2osc_Jakob.png", f, px_per_unit = 5)
# size 120 KB
# save("bif_3osc.pdf", f, pt_per_unit = 1)

##

#f_3d = Figure(resolution=(1200, 800), title="Osillators 3d")
f_3d = Figure(resolution=size_pt, fontsize=12);
# ax = Axis3(f_3d[1, 1], title = "Oscillators 3d", xlabel="n", ylabel = "Mean ω Oscillator 1", zlabel = "Mean ω Oscillator 2")
ax = Axis3(f_3d[1, 1], xlabel="n", ylabel = "⟨ω₁⟩", zlabel = "⟨ω₂⟩  ")
# LaTeXStrings does not work on z_axis


# ax = Axis3(f_3d[1, 1], xlabel=L"$n$", ylabel = L"$\langle \omega_1 \rangle$", zlabel = L"$\langle \omega_2 \rangle$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[1,:,i_N], af[2,:,i_N]; color = color, markersize = marker_size)
end

f_3d

save("bif_2osc_3d_Jakob.png", f_3d, px_per_unit = 5)



##
