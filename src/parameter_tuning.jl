"""
Explore parameter ranges in the toy model in which the resonant solitary states appear by tuning parameters.
"""
#
include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))
include(joinpath(@__DIR__,"parameter_tuning_tools.jl"))

fontsize = 12
size_inches = ((3+3/8), 2/3 * (3+3/8))
size_pt = 72 .* size_inches
save_type = "pdf"
legend_position = :topright # or :outerright

ode_function = two_osc_harm_simpler_parameter_tuning!
y0 = [0,0,5,0]
tspan = (0, 1000)
t_stat = 300
# standard values
α = 0.1
P = 1.0
K = 6.0
n = 6.0

p0 = [P, α, K, K*n]

## P
legend_position = false
p1_low = [0.0, α, K, n]
p1_high = [3.0, α, K, n]
dp = [0.01, 0, 0, 0]
p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, plt = tunepar_both_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp, save_type, size_inches, legend_position=legend_position)

## alpha
legend_position = :topright
p1_low = [P, 0.05, K, n]
p1_high = [P, 0.35, K, n]
dp = [0, 0.001, 0, 0]

p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, plt = tunepar_both_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp, save_type, size_inches, legend_position=legend_position)

## K
legend_position = false
p1_low = [P, α, 0, n]
p1_high = [P, α, 10, n]
dp = [0, 0, 0.02, 0]

p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, plt = tunepar_both_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp, save_type, size_inches, legend_position=legend_position)

## Kn 

## K with also K in K n
add_name = "_only_K"
ode_function = two_osc_harm_simpler_parameter_tuning_only_K!
legend_position = false
p0 = [P, α, K, n]
p1_low = [P, α, 0.05, n]
p1_high = [P, α, 30, n]
dp = [0, 0, 0.05, 0]

p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, plt = tunepar_both_statsplot_save(ode_function, y0, tspan, t_stat, p0, p1_low, p1_high, dp, save_type, size_inches, legend_position=legend_position, add_name=add_name)
