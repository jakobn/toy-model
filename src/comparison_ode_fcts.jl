"""
plots (and some generation) to compare full and linearized + simplified toy models
also plot self-consistency for poster and talks
"""
# might be needed
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings
using JLD
using ImplicitEquations
using FileIO
using MakieTeX
#

include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

##### Compare phase diagrams of full and linearized equations

α = 0.1
K = 6.0
ns = 0.:0.2:30.
# N_per_n = 20 # only needed for alpha

### use new data to make it comparable (generation below)
savename_full = "two_osc_full_fullstats_ic_grid"
af_full = load("data//"*savename_full*".jld")["data"]

savename_harm_simpler = "two_osc_harm_simpler_fullstats_ic_grid"
af_simpler = load("data//"*savename_harm_simpler*".jld")["data"] # is harm_simpler

n_initial_ϕ = 10
n_initial_ω = 100
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

#new data generation code is down at the poster/talk plot section (...ic_grid...)
""" old data
# I lack a way of using specific initial conditions in the sampling, yet

Random.seed!(21)
savename = "two_osc_harm_simpler_fullstats"
af_simpler, ic_dump_simpler = asymptotic_statistics(ns, N_per_n, α, K)
JLD.save("data//"*savename*".jld", "data", af_simpler, "ic_dump", ic_dump_simpler)
load("data//"*savename*".jld")
af_simpler = load("data//"*savename*".jld")["data"]
ic_dump_simpler = load("data//"*savename*".jld")["ic_dump"]

# remember to change ode fct.!

Random.seed!(21)
savename = "two_osc_full_fullstats"
af_full, ic_dump_full = asymptotic_statistics(ns, N_per_n, α, K)
JLD.save("data//"*savename*".jld", "data", af_full, "ic_dump", ic_dump_full)
load("data//"*savename*".jld")
af_full = load("data//"*savename*".jld")["data"]
ic_dump_full = load("data//"*savename*".jld")["ic_dump"]

##

savename = "two_osc_harm_simpler_fullstats"
af_simpler = load("data//"*savename*".jld")["data"]

savename = "two_osc_full_fullstats"
af_full = load("data//"*savename*".jld")["data"]
"""
# N_per_n = n_initial_ω*n_initial_ϕ
# alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02

alpha=0.01

color1 = (:red, alpha) #:orange
color2 = (:orange, alpha) #:red
color3 = (:black, alpha)
color4 = (:blue, alpha)

marker1 = :circle
marker2 = :rect
marker3 = :x
marker4 = :+

size1 = 6 *scaler # 9
size2 = 6 *scaler # 9
size3 = 12 *scaler # 18
size4 = 12 *scaler # 18

size_inches = (2* (3+3/8), 0.8*(3+3/8)) # size_inches = (2* (3+3/8), 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

f_ϕ = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))

ax = Axis(f_ϕ[1, 1], xticklabelsvisible=false, xticksvisible=false,yticks=([-.5,0,.5,1],["-0.50", "0.00", "0.50", "1.00"])) #xlabel=L"$n$",

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  # Makie.scatter!(ns[2:end], af_full[1,2:end,i_N], marker=marker1, markersize=size1, color = color1)
  Makie.scatter!(ns[2:end], af_full[1,2:end,dϕ_idx,  dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ϕ_2_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels="⟨φ₂⟩ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[2,2:end,dϕ_idx,  dω_idx], marker=:rect, markersize=size2, color = color2)
end
std_ϕ_2_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels="Σ₂ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[1,2:end,dϕ_idx,  dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ϕ_2_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels="⟨φ₂⟩ lin+sim")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[2,2:end,dϕ_idx,  dω_idx]; marker=marker4, markersize=size4, color = color4)
end
std_ϕ_2_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker4, markersize=size4, color = color4[1], labels="Σ₂ lin+sim")

Makie.ylims!(ax, -0.5, 1.3)
leg = Legend( f_ϕ[1,1], [mean_ϕ_2_full, std_ϕ_2_full, mean_ϕ_2_simpler, std_ϕ_2_simpler] , ["⟨φ₂⟩ full", "Σ₂ full", "⟨φ₂⟩ lin+sim", "Σ₂ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:top) # , alpha=1 argument stopped working?!

#f_ϕ[1,1] = leg

f_ϕ

# save("two_osc_comparison_fullstats"*"_angles_v2"*".png", f_ϕ, px_per_unit=8)
save("two_osc_comparison_fullstats"*"_angles_ic_grid_v2"*".png", f_ϕ, px_per_unit=8)

## freqs (now seperately)

size_inches = (2* (3+3/8), 0.8*(3+3/8)) # size_inches = (2* (3+3/8), 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

f_ω_1 = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 1600))???
ax1 = Axis(f_ω_1[1, 1],  xticklabelsvisible=false, xticksvisible=false, yticks=([0,5,10],["0.0","5.0","10.0"])) #xlabel=L"$n$"

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[3,2:end,dϕ_idx,  dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[4,2:end,dϕ_idx,  dω_idx], marker=:rect, markersize=size2, color = color2)
end
std_ω_1_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[3,2:end,dϕ_idx,  dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[4,2:end,dϕ_idx,  dω_idx]; marker=marker4, markersize=size4, color = color4)
end
std_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker4, markersize=size4, color = color4[1], labels=L"$\Sigma\varphi_2$ lin")

Legend(f_ω_1[1,1], [mean_ω_1_full, std_ω_1_full, mean_ω_1_simpler, std_ω_1_simpler], ["⟨ω₁⟩ full", "σ₁ full", "⟨ω₁⟩ lin+sim", "σ₁ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) # , alpha=1not supported argument

f_ω_1
save("two_osc_comparison_fullstats"*"_omega_1_ic_grid"*".png", f_ω_1, px_per_unit=8)

size_inches = (2* (3+3/8), 1*(3+3/8)) # size_inches = (2* (3+3/8), 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches


f_ω_2 = Figure(resolution=size_pt, fontsize=scaler*12);
ax2 = Axis(f_ω_2[1, 1], xlabel=L"$n$", yticks=([-10,-5,-0,5],["-10.0","-5.0","0.0","5.0"]))

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[5,2:end,dϕ_idx,  dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_2_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[6,2:end,dϕ_idx,  dω_idx], marker=:rect, markersize=size2, color = color2)
end
std_ω_2_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[5,2:end,dϕ_idx,  dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_2_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[6,2:end,dϕ_idx,  dω_idx]; marker=marker4, markersize=size4, color = color4)
end
std_ω_2_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker4, markersize=size4, color = color4[1], labels=L"$\Sigma\varphi_2$ lin")

Legend(f_ω_2[1,1], [mean_ω_2_full, std_ω_2_full, mean_ω_2_simpler, std_ω_2_simpler], ["⟨ω₂⟩ full", "σ₂ full", "⟨ω₂⟩ lin+sim", "σ₂ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:bottom) # , alpha=1 not supported anymore as argument

f_ω_2

# save("two_osc_comparison_fullstats"*"_freqs_v2"*".png", f_ω, px_per_unit=8)
save("two_osc_comparison_fullstats"*"_omega_2_ic_grid"*".png", f_ω_2, px_per_unit=8)




######## Compare full and linearized-only dynamics
α = 0.1
K = 6.0
ns = 0.:0.2:30.

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]

n_initial_ϕ = 10
n_initial_ω = 100

N_per_n = n_initial_ϕ * n_initial_ω

ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

ode_function = two_osc_harm!

af_harm = asymptotic_statistics(ode_function, ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
savename_harm =  "two_osc_harm_fullstats_ic_grid"

JLD.save("data//"*savename_harm*".jld", "data", af_harm)

size_inches = (2* (3+3/8), 0.8*(3+3/8)) # size_inches = (2* (3+3/8), 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

alpha=0.01

color1 = (:red, alpha) #:orange
color2 = (:orange, alpha) #:red
color3 = (:black, alpha)
color4 = (:blue, alpha)

marker1 = :circle
marker2 = :rect
marker3 = :x
marker4 = :+

size1 = 6 *scaler # 9
size2 = 6 *scaler # 9
size3 = 12 *scaler # 18
size4 = 12 *scaler # 18

f_ϕ = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))

ax = Axis(f_ϕ[1, 1], xticklabelsvisible=false, xticksvisible=false,yticks=([-.5,0,.5,1],["-0.50", "0.00", "0.50", "1.00"])) #xlabel=L"$n$",

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  # Makie.scatter!(ns[2:end], af_full[1,2:end,i_N], marker=marker1, markersize=size1, color = color1)
  Makie.scatter!(ns[2:end], af_full[1,2:end,dϕ_idx,  dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ϕ_2_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels="⟨φ₂⟩ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[2,2:end,dϕ_idx,  dω_idx], marker=:rect, markersize=size2, color = color2)
end
std_ϕ_2_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels="Σ₂ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_harm[1,2:end,dϕ_idx,  dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ϕ_2_harm = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels="⟨φ₂⟩ lin+sim")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_harm[2,2:end,dϕ_idx,  dω_idx]; marker=marker4, markersize=size4, color = color4)
end
std_ϕ_2_harm = Makie.scatter!(empty([0]), empty([0]); marker=marker4, markersize=size4, color = color4[1], labels="Σ₂ lin+sim")

Makie.ylims!(ax, -0.5, 1.3)
leg = Legend( f_ϕ[1,1], [mean_ϕ_2_full, std_ϕ_2_full, mean_ϕ_2_harm, std_ϕ_2_harm] , ["⟨φ₂⟩ full", "Σ₂ full", "⟨φ₂⟩ lin+sim", "Σ₂ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:top) # , alpha=1 argument stopped working?!

#f_ϕ[1,1] = leg

f_ϕ

# save("two_osc_comparison_fullstats"*"_angles_v2"*".png", f_ϕ, px_per_unit=8)
save("two_osc_harm_comparison_fullstats"*"_angles_ic_grid"*".png", f_ϕ, px_per_unit=8)

## freqs (now seperately)

size_inches = (2* (3+3/8), 0.8*(3+3/8)) # size_inches = (2* (3+3/8), 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

f_ω_1 = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 1600))???
ax1 = Axis(f_ω_1[1, 1],  xticklabelsvisible=false, xticksvisible=false, yticks=([0,5,10],["0.0","5.0","10.0"])) #xlabel=L"$n$"

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[3,2:end,dϕ_idx,  dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[4,2:end,dϕ_idx,  dω_idx], marker=:rect, markersize=size2, color = color2)
end
std_ω_1_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_harm[3,2:end,dϕ_idx,  dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_harm = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_harm[4,2:end,dϕ_idx,  dω_idx]; marker=marker4, markersize=size4, color = color4)
end
std_ω_1_harm = Makie.scatter!(empty([0]), empty([0]);
  # ylims=(-0.1),
  marker=marker4, markersize=size4, color = color4[1], labels=L"$\Sigma\varphi_2$ lin")

Legend(f_ω_1[1,1], [mean_ω_1_full, std_ω_1_full, mean_ω_1_harm, std_ω_1_harm], ["⟨ω₁⟩ full", "σ₁ full", "⟨ω₁⟩ lin+sim", "σ₁ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) # , alpha=1not supported argument

Makie.ylims!(ax1, -5/7,10+5/7)
f_ω_1
save("two_osc_harm_comparison_fullstats"*"_omega_1_ic_grid"*".png", f_ω_1, px_per_unit=8)


size_inches = (2* (3+3/8), 1*(3+3/8)) # size_inches = (2* (3+3/8), 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches


f_ω_2 = Figure(resolution=size_pt, fontsize=scaler*12);
ax2 = Axis(f_ω_2[1, 1], xlabel=L"$n$", yticks=([-10,-5,-0,5],["-10.0","-5.0","0.0","5.0"]))

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[5,2:end,dϕ_idx,  dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_2_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[6,2:end,dϕ_idx,  dω_idx], marker=:rect, markersize=size2, color = color2)
end
std_ω_2_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_harm[5,2:end,dϕ_idx,  dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_2_harm = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range)) # for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_harm[6,2:end,dϕ_idx,  dω_idx]; marker=marker4, markersize=size4, color = color4)
end
std_ω_2_harm = Makie.scatter!(empty([0]), empty([0]); marker=marker4, markersize=size4, color = color4[1], labels=L"$\Sigma\varphi_2$ lin")

Legend(f_ω_2[1,1], [mean_ω_2_full, std_ω_2_full, mean_ω_2_harm, std_ω_2_harm], ["⟨ω₂⟩ full", "σ₂ full", "⟨ω₂⟩ lin+sim", "σ₂ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:bottom) # , alpha=1 not supported anymore as argument

Makie.ylims!(ax2, -11, 6.5)
f_ω_2

# save("two_osc_comparison_fullstats"*"_freqs_v2"*".png", f_ω, px_per_unit=8)
save("two_osc_harm_comparison_fullstats"*"_omega_2_ic_grid"*".png", f_ω_2, px_per_unit=8)






#######
# figure for poster: frequencies of root for full and linearized equations + self-consistency

savename_harm_simpler = "two_osc_harm_simpler_fullstats"
af_simpler = load("data//"*savename_harm_simpler*".jld")["data"]

savename_full = "two_osc_full_fullstats"
af_full = load("data//"*savename_full*".jld")["data"]

ns = 0.:0.2:30.
N_per_n = 20

n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02

color1 = (:red, alpha) # :orange
color2 = (:orange, alpha) # :red
color3 = (:black, alpha)
color4 = (:blue, alpha)

marker1 = :circle
marker2 = :rect
marker3 = :x
marker4 = :+

size1 = 6 *scaler # 9
size2 = 6 *scaler # 9
size3 = 12 *scaler # 18
size4 = 12 *scaler # 18

# Can't make alpha to small or it will round to zero when cast to RGBA.
# alpha = 2. / N > 0.02 ? 2. / N : 0.02
# alpha = .2
# color = (:black, alpha)
# marker_size = 2

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[3,2:end,i_N], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[4,2:end,i_N], marker=:rect, markersize=size2, color = color2)
end
std_ω_1_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[3,2:end,i_N]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_simpler[4,2:end,i_N]; marker=marker4, markersize=size4, color = color4)
end
std_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker4, markersize=size4, color = color4[1], labels=L"$\Sigma\varphi_2$ lin")

v_range = 0.01:0.01:9.99
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler)
d_min = ns[1] - .5
d_max = ns[end] + .5
Makie.xlims!(ax1, [d_min, d_max])

# Legend(f_ω[1,2], [mean_ω_1_full, std_ω_1_full, ω_plus],  [L"$\sigma_2$ lin", L"ω₊", L"$\omega_-$"])

Legend(f_ω[1,2], [mean_ω_1_full, std_ω_1_full, mean_ω_1_simpler, std_ω_1_simpler, ω_plus, ω_minus], ["⟨ω₂⟩ full", "σ₂ full", "⟨ω₂⟩ lin", "σ₂ lin", "ω₊", "ω₋"]) # , width=10 # alpha=1 stopped working ?!

f_ω

save("two_osc_comparison_fullstats"*"_freqs_sprout_self_consistency"*".png", f_ω, px_per_unit = 8)
# pdf is too large: 1.2 MB




##### DDays poster/talk plot with more points and ic_grid
α = 0.1
K = 6.0
ns = 0.:0.2:30.

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

n_initial_ϕ = 10
n_initial_ω = 100
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

savename_full = "two_osc_full_fullstats_ic_grid"
@time af_full = asymptotic_statistics(two_osc! , ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
JLD.save("data//"*savename_full*".jld", "data", af_full)
af_full = load("data//"*savename_full*".jld")["data"]

savename_harm_simpler = "two_osc_harm_simpler_fullstats_ic_grid"
af_harm_simpler = asymptotic_statistics(two_osc_harm_simpler! , ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
JLD.save("data//"*savename_harm_simpler*".jld", "data", af_harm_simpler)
af_harm_simpler = load("data//"*savename_harm_simpler*".jld")["data"]

n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

# alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
alpha = 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)

marker1 = :circle
marker3 = :x

size1 = 6 *scaler # 9
size3 = 12 *scaler # 18

f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[3,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_harm_simpler[3,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

v_range = 0.01:0.01:9.99
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3)
d_min = ns[1] - .5
d_max = ns[end] + .5
n_range_fine = 0:0.01:d_max
K=6.
Ω = 10.
sqrtKn = Makie.lines!(n_range_fine, sqrt.(K .*n_range_fine), label=L"\sqrt{K_r n}")
Ω_horizontal =  Makie.lines!(n_range_fine, Ω.* ones(length(n_range_fine)), label=L"$\Omega_s$", linecolor=:purple) #, linecolor=:blue
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [-0.5, 11.])
Legend(f_ω[1,2], [mean_ω_1_full, mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["⟨ωₛ⟩ full", "⟨ωₛ⟩ lin", "ω₊", "ω₋", L"\sqrt{K_r n}", "Ωₛ"]) # , width=10 # alpha=1 stopped working ?!
Legend(f_ω[1,2], [ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["ω₊", "ω₋", L"\sqrt{K_r n}", "Ωₛ"]) # , width=10 # alpha=1 stopped working ?!
Legend(f_ω[1,2], [mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["⟨ωₛ⟩ lin", "ω₊", "ω₋", L"\sqrt{K_r n}", "Ωₛ"]) # , width=10 # alpha=1 stopped working ?!


f_ω

# made thicker lines for Z, is in talk, not on poster!
# made higher alpha to compare: there is a discrepancy to previous results: where are the newstates and why are there other weird states?
save("two_osc_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_thicker_lines_harm_simpler"*".png", f_ω, px_per_unit = 8)
# pdf is too large: 1.2 MB





##### remake plots with labels 1 and 2 instead of s and r
# remake with slightly higher layout so that legend can fit inside
savename_full = "two_osc_full_fullstats_ic_grid"
savename_harm_simpler = "two_osc_harm_simpler_fullstats_ic_grid"
af_full = load("data//"*savename_full*".jld")["data"]
af_harm_simpler = load("data//"*savename_harm_simpler*".jld")["data"]

α = 0.1
K = 6.0
ns = 0.:0.2:30.

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

n_initial_ϕ = 10
n_initial_ω = 100
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

n_self(α, Ω, K, v, σ) = v.^2 ./K .+ σ .* sqrt.(v ./(2 .* (Ω .- v)) - α^2 .* v.^2 ./ K^2)

size_inches = (2* (3+3/8), 0.6 * 2 * (3+3/8))
# half the size does not work, looks stupid
scaler = 1 # 4 * for normal, 8 for poster 
# but better for proportions to choose 1 either pdf save or if too large use px_per_unit in save option for png 
size_pt = scaler .* 72 .* size_inches
# size_pt = (1200, 800)

# alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
alpha = 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)

marker1 = :circle
marker3 = :x

size1 = 6 *scaler # 9
size3 = 12 *scaler # 18

## onlz Z
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

v_range = 0.01:0.01:9.99
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3)
d_min = ns[1] - .5
d_max = ns[end] + .5
n_range_fine = 0:0.01:d_max
K=6.
Ω = 10.
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [-0.5, 11.])

Legend(f_ω[1,1], [ω_plus, ω_minus], ["ω₊", "ω₋"], tellwidth=false, tellheight=false, halign=:right, valign=:center) # , width=10 # alpha=1 stopped working ?!

f_ω
save("two_osc_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_thicker_lines_harm_simpler_labels12_onlyZ_higher"*".png", f_ω, px_per_unit = 8)

## Z with comments
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

v_range = 0.01:0.01:9.99
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3)
d_min = ns[1] - .5
d_max = ns[end] + .5
n_range_fine = 0:0.01:d_max
K=6.
Ω = 10.
sqrtKn = Makie.lines!(n_range_fine, sqrt.(K .*n_range_fine), label=L"\sqrt{K_r n}")
Ω_horizontal =  Makie.lines!(n_range_fine, Ω.* ones(length(n_range_fine)), label=L"$\Omega_s$", linecolor=:purple) #, linecolor=:blue
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [-0.5, 11.])

Legend(f_ω[1,1], [ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["ω₊", "ω₋", L"\sqrt{K_2 n}", "Ω₁"], tellwidth=false, tellheight=false, halign=:right, valign=:center) # , width=10 # alpha=1 stopped working ?!

f_ω
save("two_osc_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_thicker_lines_harm_simpler_labels12_onlycomm_higher"*".png", f_ω, px_per_unit = 8)



## Z with comments and lin+simpler data
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_harm_simpler[3,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

v_range = 0.01:0.01:9.99
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3)
d_min = ns[1] - .5
d_max = ns[end] + .5
n_range_fine = 0:0.01:d_max
K=6.
Ω = 10.
sqrtKn = Makie.lines!(n_range_fine, sqrt.(K .*n_range_fine), label=L"\sqrt{K_r n}")
Ω_horizontal =  Makie.lines!(n_range_fine, Ω.* ones(length(n_range_fine)), label=L"$\Omega_s$", linecolor=:purple) #, linecolor=:blue
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [-0.5, 11.])

Legend(f_ω[1,1], [mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["⟨ω₁⟩ lin+sim", "ω₊", "ω₋", L"\sqrt{K_2 n}", "Ω₁"], tellwidth=false, tellheight=false, halign=:right, valign=:center) # , width=10 # alpha=1 stopped working ?!

f_ω
save("two_osc_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_thicker_lines_harm_simpler_labels12_onlylin_higher"*".png", f_ω, px_per_unit = 8)



## Z with comments and lin+simpler data and full data
## now: also with updated labels Ω_s -> Ω₁ and K_2 -> K_3
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[3,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_harm_simpler[3,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1], labels=L"$\langle\varphi_2\rangle$ lin")

v_range = 0.01:0.01:9.99
ω_plus  = Makie.lines!(n_self(.1, 10., 6., v_range, 1), v_range, linewidth = scaler*3)
ω_minus = Makie.lines!(n_self(.1, 10., 6., v_range, -1), v_range, linewidth = scaler*3)
d_min = ns[1] - .5
d_max = ns[end] + .5
n_range_fine = 0:0.01:d_max
K=6.
Ω = 10.
sqrtKn = Makie.lines!(n_range_fine, sqrt.(K .*n_range_fine), label=L"\sqrt{K_r n}")
Ω_horizontal =  Makie.lines!(n_range_fine, Ω.* ones(length(n_range_fine)), label=L"$\Omega_s$", linecolor=:purple) #, linecolor=:blue
Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [-0.5, 11.])
# Legend(f_ω[1,2], [mean_ω_1_full, mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], [L"$\langle\omega_1\rangle$ full", L"⟨$\omega_1$⟩ lin+sim", L"\omega_+", L"\omega_-", L"\sqrt{K_2 n}", L"\Omega_s"]) # looks weird
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim", "ω₊", "ω₋", L"\sqrt{K_3 n}", "Ω₁"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #
# Legend(f_ω[1,2], [mean_ω_1_full, mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim", "ω₊", "ω₋", "√(K₂n)", "Ωₛ"]) # looks weird
# Legend(f_ω[1,2], [ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["ω₊", "ω₋", L"\sqrt{K_2 n}", "Ω₁"]) # , width=10 # alpha=1 stopped working ?!
# Legend(f_ω[1,2], [mean_ω_1_simpler, ω_plus, ω_minus, sqrtKn, Ω_horizontal], ["⟨ω₁⟩ lin+sim", "ω₊", "ω₋", L"\sqrt{K_2 n}", "Ω₁"]) # , width=10 # alpha=1 stopped working ?!

f_ω
save("two_osc_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_thicker_lines_harm_simpler_labels12_all_higher_v2"*".png", f_ω, px_per_unit = 8)






##### comparison of ode fcts for 3 node system with ic grid
savename_full = "three_osc_star_full_fullstats_ic_grid"
af_full = load("data//"*savename_full*".jld")["data"]

savename_lin = "three_osc_lin_fullstats_ic_grid"
af_lin = load("data//"*savename_lin*".jld")["data"]

savename_lin_simpler = "three_osc_lin_simpler_fullstats_ic_grid"
af_lin_simpler = load("data//"*savename_lin_simpler*".jld")["data"]
af_lin_simpler[5,:,:,:]

α = 0.1
K = 6.0
ns = 0.:0.2:30.

t_sim = (0.,200.)
t_stat = 20. # how long the end of the tail should be
t_stat_iter = t_sim[2] - t_stat : t_stat/997 : t_sim[2]
abstol = 1e-3
reltol = 1e-3

n_initial_ϕ = 10
n_initial_ω = 100
ϕ_min = 0.
ϕ_max_excl = 2*pi
ω_min = -4.
ω_max_incl = 5.

perturbation_range_ϕ = ϕ_min : ϕ_max_excl /(n_initial_ϕ) : ϕ_max_excl * (n_initial_ϕ -1) /n_initial_ϕ
perturbation_range_ω = ω_min : (ω_max_incl - ω_min) /(n_initial_ω -1)  : ω_max_incl

dϕ_idx_range = 1:n_initial_ϕ
dω_idx_range = 1:n_initial_ω

size_inches = (2* (3+3/8), 1/2 * 2 * (3+3/8))
scaler = 1
size_pt = scaler .* 72 .* size_inches

alpha = 0.02

color1 = (:red, alpha)
color3 = (:black, alpha)
color5 = (:blue, alpha)
marker1 = :circle
marker3 = :x
marker5 = :+

size1 = 6 *scaler # 9
size3 = 12 *scaler # 18
size5 = 12 *scaler

ns = 0.:0.2:30.
d_min = ns[1] # - .5
d_max = ns[end] # + .5
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_full[5,2:end, dϕ_idx, dω_idx], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1])

for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin_simpler[5,2:end, dϕ_idx, dω_idx]; marker=marker3, markersize=size3, color = color3)
end
mean_ω_1_lin_simpler = Makie.scatter!(empty([0]), empty([0]); marker=marker3, markersize=size3, color = color3[1])

Makie.xlims!(ax1, [d_min, d_max])
Makie.ylims!(ax1, [0, 10.5])
Legend(f_ω[1,1], [mean_ω_1_full, mean_ω_1_lin_simpler], ["⟨ω₁⟩ full", "⟨ω₁⟩ lin+sim"], tellwidth=false, tellheight=false, halign=:right, valign=:center) #

f_ω
# save("two_osc_comparison_ic_grid_fullstats"*"_freqs_sprout_self_consistency_thicker_lines_harm_simpler_labels12_all_higher"*".png", f_ω, px_per_unit = 8)

# mean omega 1
# no natural phase velocity??? Reason could be initial conditions: ic_grid only perturbs node 1,
# while the others are 0 (close to operating state)
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$", ylabel="⟨ω₁⟩")
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin[5,2:end, dϕ_idx, dω_idx]; marker=marker5, markersize=size5, color = :blue) #color5
end
mean_ω_1_lin = Makie.scatter!(empty([0]), empty([0]); marker=marker5, markersize=size5, color = color5[1]);
f_ω

# mean omega 2
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$", ylabel="⟨ω₂⟩")
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin[7,2:end, dϕ_idx, dω_idx]; marker=marker5, markersize=size5, color = :blue) #color5
end
mean_ω_1_lin = Makie.scatter!(empty([0]), empty([0]); marker=marker5, markersize=size5, color = color5[1]);
f_ω

# mean omega 3
f_ω = Figure(resolution=size_pt, fontsize=scaler*12); # Figure(resolution=(1200, 800))
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$", ylabel="⟨ω₃⟩")
for (dϕ_idx,  dω_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range))
  Makie.scatter!(ns[2:end], af_lin[9,2:end, dϕ_idx, dω_idx]; marker=marker5, markersize=size5, color = :blue) #color5
end
mean_ω_1_lin = Makie.scatter!(empty([0]), empty([0]); marker=marker5, markersize=size5, color = color5[1]);
f_ω


# I do not understand why the linearized system only has the steady state for ic grid (noise around it? Have not tried alpha=1 in plots).
# I do not understand why the linearized system only has the steady state for random ic (and some noise around it visible with high alpha).
# I do not understand why the linearized and simplified system has noisy plots around the steady state for random ic.
# I only have to show the full data in the Z plots, because the prediction matches them better anyway.