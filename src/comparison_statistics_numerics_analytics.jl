"""
Plots to compare statistics from the numerical simulations with the theoretical predictions.
"""
# might be needed
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings
using JLD
using ImplicitEquations
using FileIO
#


include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))
include(joinpath(@__DIR__,"parameter_tuning_tools.jl"))

#####
# compare amplitudes in simulation and approximate solution

y_0 = [0.,0.,5.,0.]
t_span = (0., 1000.)
t_stat = 300. # last 300 time units
p_0 = [0.1, 6., 6.] # α, K , n
p_1_high = [.1, 6., 30.]
p_1_low = [.1, 6., 1.]
dp = [0., 0., 0.1]
ind = 3

p_mat_inc, p_mat_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot = tunepar_both_angle(y_0, t_span, t_stat, p_0, p_1_low, p_1_high, dp, save_plot=false, legend_position=:right)
x_p_inc = p_mat_inc[:,ind]
x_p_dec = p_mat_dec[:,ind]

# stats includes mean ϕ_2, std ϕ_2, mean ω_1, std ω_1, mean ω_2, mean ω_2

# predicition:
np(v) = (
 2 * P .* v.^2 .- 2* a .* v.^3 .+ 
  sqrt(2) .* sqrt.(a .* v .* (P .- a .* v) .* (K^2 .+ 2 .* a .* v .* (-P .+ a .* v))))./(
 2 * K .* (P .- a .* v))

 nm(v) = (
 2 * P .* v.^2 .- 2* a .* v.^3 .- 
  sqrt(2) .* sqrt.(a .* v .* (P .- a .* v) .* (K^2 .+ 2 .* a .* v .* (-P .+ a .* v))))./(
 2 * K .* (P .- a .* v))

x0(v, n) = K ./ sqrt((K .* n .- v.^2).^2 .+ a^2 .* v^2)

x0p(v) = x0(v, np(v))

x0m(v) = x0(v, nm(v))

Std_2(v) = x0p(v)./sqrt(2)
# frequency std # careful I shifted the factor sqrt(2) araound since the old plot!
ampeqp(v) = v .* x0p(v) ./ sqrt(2)

ampeqm(v) = v .* x0m(v) ./ sqrt(2)

idv(v) = v

# standard deviation of of \dot f_1
fstdp(v) = sqrt.(1/2 .* (K^2/(v.^2 + a^2) .* (P^2 ./(K^2 .* np(v).^2) .+ 1) .+ K^2 .* x0p(v).^2 ./ (4 .* (4 .* v.^2 .+ a^2))))

P = 1.; K = 6.; a = 0.1;

plt = Plots.plot(np, idv, 0, 9.99, xlims=(-5,30), linestyle=:dash, linecolor=1, labels=L"$v_+$");
Plots.plot!(nm, idv, 0, 9.99, linestyle=:dash, linecolor=2, labels=L"$v_-$");
Plots.plot!(np, ampeqp, 0, 9.99, xlims=(-5,30), linestyle=:dot, linecolor=1, labels="amp +");
Plots.plot!(nm, ampeqm, 0, 9.99, linestyle=:dot, linecolor=2, labels="amp -");
symbol = "n";
Plots.scatter!(vcat(x_p_inc, x_p_dec), vcat(stats_inc[:,1], stats_dec[:,1]), xlabel=symbol, markershape=:+, markersize=2, markercolor=3, markeralpha=0.5, labels=L"$\langle \omega_1 \rangle$");
Plots.scatter!(vcat(x_p_inc, x_p_dec), sqrt(2) .* vcat(stats_inc[:,4], stats_dec[:,4]), xlabel=symbol, markershape=:+, markersize=2, markercolor=4, markeralpha=0.5, labels=L"$\sqrt{2}\sigma_2$", legend=:right);

plt

save("comparison_prediction_frequency_statistics.pdf", plt)

# now the angles:

x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot = tunepar_both_angle(y_0, t_span, t_stat, p_0, p_1_low, p_1_high, dp, save_plot=false)

mean_ϕ_2(n) = -P/(K*n)

plt = Plots.plot(idv, mean_ϕ_2, 0, 30, linestyle=:dash, labels=L"P_2/K n");
Plots.plot!(np, x0p, 0, 9.99, xlims=(-5,30), ylims=(-.1,1.4), linestyle=:dash, labels=L"$x_{0+}$");
Plots.plot!(nm, x0m, 0, 9.99, ylims=(-.1,1.4), linestyle=:dash, labels=L"$x_{0-}$");
Plots.scatter!(vcat(x_p_inc, x_p_dec), vcat(stats_inc[:,1], stats_dec[:,1]), xlabel=symbol, markershape=:x, markersize=2, markercolor=4, markeralpha = 0.5, labels=L"$\langle \varphi_2 \rangle$");
Plots.scatter!(vcat(x_p_inc, x_p_dec), sqrt(2) .* vcat(stats_inc[:,2], stats_dec[:,2]), xlabel=symbol, markershape=:+, markersize=2, markercolor=5, markeralpha=0.5, labels=L"$\sqrt{2}\Sigma_2$", legend=:right);

plt

save("comparison_prediction_angle_statistics.pdf", plt)


### old stuff

# x_p_K_inc, y_mat_K_inc, stats_K_inc = tunepar([0.0,0.0,5.0,0.0],(0.0,200.0),50.0,[0.1,6.0,5.0],[0.1,30.0,5.0],[0.0,0.05,0.0],save_stats=false)
# x_p_K_dec, y_mat_K_dec, stats_K_dec = tunepar([0.0,0.0,5.0,0.0],(0.0,200.0),50.0,[0.1,6.0,5.0],[0.1,0.0,5.0],[0.0,-0.05,0.0],save_stats=false)
# stats_plot_K = Plots.scatter(vcat(x_p_K_inc, x_p_K_dec), vcat(stats_K_inc, stats_K_dec), xlabel=L"$K$", markershape=:x, markersize=1, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:bottomright)
# vline!([6], label="start")
# save("2osc_adi_Kuramoto_stats_K_0.05.png", stats_plot_K)

# x_p_n_inc, y_mat_n_inc, stats_n_inc = tunepar([0.0,0.0,10.0,0.0],(0.0,200.0),50.0,[0.1,6.0,0.0],[0.1,6.0,13.0],[0.0,0.0,0.1],show_stats=true,save_stats=true, save_stats_name="_Om1")
# x_p_n_dec, y_mat_n_dec, stats_n_dec = tunepar([0.0,0.0,8.0,0.0],(0.0,200.0),50.0,[0.1,6.0,13.0],[0.1,6.0,0.1],[0.0,0.0,-0.1],show_stats=true,save_stats=true, save_stats_name="_SESS")
# Z(n, α, Ω, K, v) = v - Ω + v * K^2 /2 /((K * n - v^2)^2 + α^2 * v^2)
# Ze(n, v) = Z(n, 0.1, 10, 6, v)

# stats_plot_n = Plots.scatter(vcat(x_p_n_inc, x_p_n_dec), vcat(stats_n_inc, stats_n_dec), xlabel=L"$K$", markershape=:o, markersize=2, markeralpha=0.5, markerstrokewidth=0, labels=[L"$\langle \omega_1 \rangle$" L"$\sigma_1$" L"$\langle \omega_2 \rangle$" L"$\sigma_2$"], legend=:bottomright, size=(600,400));
# Plots.plot!(Eq(Ze,0), xlim=(-1,13), ylim=(-1,10), label=L"$Z=0$", linecolor=:red, fillcolor=:red, legend=:right, xaxis=L"$n$", yaxis=L"$\langle \omega \rangle$")
# save("2osc_adi_Kuramoto_stats_n_0.05_Om1_SESS_vsZ.png", stats_plot_n)

# why does this not work??? When I hover over save(...) it suggests the method below...
#FileIO.save("2osc_adi_Kuramoto_stats_n_0.05_Om1_SESS_vsZ.png", stats_plot_n, resolution=size(stats_plots_n))
#



##### make nice new plot for thesis: 
# first: ⟨ϕ_2⟩, var ϕ_2

mean_ϕ_2(n) = -P/(K*n)
minus_mean_ϕ_2(n) = P/(K*n)
P = 1.; K = 6.; a = 0.1;
symbol = "n";


scaler = 2
markersize = scaler*4
linewidth = scaler *2
fontsize = scaler *12
size_inches = scaler.*(2* (3+3/8),2* 1/3 *  (3+3/8)) # made it half height, full width
size_pt = 72 .* size_inches
legend_position = :outerright
x_lims = (0,20)
y_lims = (0.007,1.5)

plt = Plots.plot(idv, minus_mean_ϕ_2, 0, 30,
    linewidth = linewidth, linestyle=:dash, labels=L"-P_2/K n",
    xaxis=L"$n$", yaxis=:log,
    xlims=x_lims, ylims=y_lims,
    label="simulation",
    legendfontsize= fontsize, labelfontsize=fontsize,
    xtickfontsize=fontsize, ytickfontsize = fontsize,
    size=100 .*size_inches, legend_position=legend_position, margin=5Plots.mm);

# here no visible xlabel(margin cuts it) because lower plot has # edit: annotated it here too

Plots.plot!(np, Std_2, 0, 9.99, linewidth=linewidth, linestyle=:dash, linecolor=3, labels=L"a/\sqrt{2}");

Plots.scatter!(vcat(x_p_inc, x_p_dec), -vcat(stats_inc[:,1], stats_dec[:,1]), xlabel=symbol, markershape=:x, markersize=markersize, markercolor=4, markeralpha=0.5, labels=L"$-\langle \varphi_2 \rangle$");
Plots.scatter!(vcat(x_p_inc, x_p_dec), vcat(stats_inc[:,2], stats_dec[:,2]), xlabel=symbol, markershape=:+, markersize=markersize, markercolor=5, markeralpha=0.5, labels=L"$\Sigma_2$");
Plots.vline!([p_0[ind]], label="start", linewidth=linewidth)
Plots.annotate!((22,0.004,("n"),fontsize)) #-0.1 -0.33 * 1.1/3

plt

save("comparison_prediction_numerics_angles_toy_model_log.pdf", plt) # png is large


# now:  var ω_1
legend_position = :outerright
x_lims = (0,20)
y_lims = (0,3)

plt = Plots.plot(np, fstdp, 0, 10,
    linewidth = linewidth, linestyle=:dash, labels=L"\sqrt{\langle\dot{f}_1^2 \rangle}",
    xaxis=L"$n$",
    xlims=x_lims, ylims=y_lims,
    label="simulation",
    legendfontsize= fontsize, labelfontsize=fontsize,
    xtickfontsize=fontsize, ytickfontsize = fontsize,
    size=100 .*size_inches, legend_position=legend_position, margin=5Plots.mm);

# Axis label made with annotate!

Plots.scatter!(vcat(x_p_inc, x_p_dec), vcat(stats_inc[:,4], stats_dec[:,4]), xlabel=symbol, markershape=:+, markersize=markersize, markercolor=4, markeralpha=0.5, labels=L"$\sigma_1$");
Plots.vline!([p_0[ind]], label="start", linewidth=linewidth, linecolor=5);
Plots.yticks!([0,1,2,3],["0.00","1.00","2.00","3.00"]);
Plots.annotate!((22,-0.33,("n"),fontsize))
plt
save("comparison_prediction_numerics_frequency_toy_model.pdf", plt) # png is large


### now plot Delta
Δ(α, K, n, v) = (v.^2 .- K * n) ./(α * sqrt.(K * n))
# Delta plus easy
Δpe(v) = Δ(0.1, 6, np(v), v)
# Delta minus easy
Δme(v) = Δ(0.1, 6, nm(v), v)

ϵΔpe(v) = v.^2 ./ (6. * np(v)) .- 1

ϵΔme(v) = v.^2 ./ (6. * nm(v)) .- 1

vrange = 0.001: 0.001 : 9.99
vrangepos = 1.298 : 0.001 : 9.981# necessary so that nm is never negative and the squareroot is never complex
nm(vrangepos)

np(vrange)
nm(vrange)

# to know which end belongs to which part of the branch
Δme(1.298)
Δme(9.981)
# Delta can get very large at the minus branch, especially the lower part
Plots.plot(np(vrange), Δpe(vrange))
Plots.plot!(nm(vrangepos), Δme(vrangepos))

# to know which end belongs to which part of the branch
ϵΔme(1.298)
ϵΔme(9.981)
Plots.plot(np(vrange), ϵΔpe(vrange))
Plots.plot!(nm(vrangepos), ϵΔme(vrangepos))
Plots.ylims!(-3,3)

### make nice plot
legend_position = :outerright
x_lims = (0,20)
# y_lims = (-10,10)
plt = Plots.plot(np(vrange), -Δpe(vrange),
    linewidth = linewidth, labels=L"-\Delta_+",
    xaxis=L"$n$",
    xlims=x_lims, #ylims=y_lims,
    legendfontsize= fontsize, labelfontsize=fontsize,
    xtickfontsize=fontsize, ytickfontsize = fontsize,
    size=100 .*size_inches, legend_position=legend_position, margin=5Plots.mm);

Plots.plot!(nm(vrangepos), Δme(vrangepos),
    linewidth = linewidth, labels=L"+\Delta_-");
Plots.yaxis!(:log);
Plots.annotate!((22,0.9,("n"),fontsize));
plt

save("prediction_Delta.pdf", plt)