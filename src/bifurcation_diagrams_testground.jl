"""
Testground for bifurcation diagrams.
"""
# might be needed
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings
using JLD
using ImplicitEquations
using FileIO
#

include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))


α = 0.1
K = 6.0
ns = 0.:0.2:30.
N_per_n = 20

Random.seed!(21)
savename = "two_osc_harm_simpler_newstates"
af, ic_dump = asymptotic_freqs(ns, N_per_n, α, K)
JLD.save("data//"*savename*".jld", "data", af, "ic_dump", ic_dump)
load("data//"*savename*".jld")
af = load("data//"*savename*".jld")["data"]
ic_dump = load("data//"*savename*".jld")["ic_dump"]
##
  
# Can't make alpha to small or it will round to zero when cast to RGBA.
alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
color = (:black, alpha)


f = Figure(resolution=(1200, 800));
Axis(f[1, 1], xlabel=L"$n$", ylabel=L"$\langle\omega_1\rangle$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[1,:,i_N]; color = color, legend = false)
end
  
Axis(f[2, 1], xlabel=L"$n$", ylabel=L"$\langle\omega_2\rangle$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[2,:,i_N]; color = color, legend = false)
end

save(savename*".png", f)

f

##

f_3d = Figure(resolution=(1200, 800), title="Osillators 3d")
ax = Axis3(f_3d[1, 1], title = "Oscillators 3d", xlabel="k", ylabel = "Mean ω Oscillator 1", zlabel = "Mean ω Oscillator 2")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[1,:,i_N], af[2,:,i_N]; color = color)
end

save("bif_2osc_3d.png", f_3d)

f_3d

#######
# find in between states that appear in the harmonic system

where_between = findall(i->(i<5.5 && i > 4),af)
Random.seed!(21)
# turns out ic != ic_dump. Maybe randn is used by the solvers or so
ic = 5 .* randn(4, 151, 20)

pos = 3
ind = where_between[pos]

simplot(ic_dump[:,ind[2],ind[3]], (0.,100.),[0.1, 6.0, ns[ind[2]]])
simplot([0.,0.,7., 0.], (0.,50.),[0.1, 6.0, ns[ind[2]]])

x_p_inc, x_p_dec, y_mat_inc, y_mat_dec, stats_inc, stats_dec, stats_plot = tunepar_both[ic_dump[:,ind[2],ind[3]], (0., 200.), 50., [.1, 6., ns[ind[2]]], [.1, 6., 30.], [.1, 6., 1.], [0., 0., 0.1], save_plot=true ]

y_0 = ic_dump[:,ind[2],ind[3]]
t_span = (0., 200.)
t_stat = 50.
p_0 = [.1, 6., ns[ind[2]]]
p_1_high = [.1, 6., 30.]
p_1_low = [.1, 6., 1.]
dp = [0., 0., 0.1]

tunepar_both(y_0, t_span, t_stat, p_0, p_1_low, p_1_high, dp, save_plot=true)

# Plot(K / Sqrt((K* n - v^2)), ) ...?

#####
#more samples on full equations

α = 0.1
K = 6.0
ns = 0.:0.2:30.
N_per_n = 200

Random.seed!(21)
savename = "two_osc_full_fullstats_200"
af_full, ic_dump_full = asymptotic_statistics(ns, N_per_n, α, K)
JLD.save("data//"*savename*".jld", "data", af_full, "ic_dump", ic_dump_full)
load("data//"*savename*".jld")
af_full = load("data//"*savename*".jld")["data"]
ic_dump_full = load("data//"*savename*".jld")["ic_dump"]

# Can't make alpha to small or it will round to zero when cast to RGBA.
alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
color = (:black, alpha)

alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02

color1 = (:orange, alpha)
color2 = (:red, alpha)
color3 = (:black, alpha)
color4 = (:blue, alpha)

marker1 = :circle
marker2 = :rect
marker3 = :x
marker4 = :+

size1 = 9
size2 = 9
size3 = 18
size4 = 18


f_ϕ = Figure(resolution=(1200, 800));
ax = Axis(f_ϕ[1, 1], xlabel=L"$n$", ylabel=L"$\langle\omega_1\rangle$")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[1,2:end,i_N], marker=marker1, markersize=size1, color = color1)
end
mean_ϕ_2_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[2,2:end,i_N], marker=:rect, markersize=size2, color = color2)
end
std_ϕ_2_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

Makie.ylims!(ax, -1, 2)
Legend(f_ϕ[1,2], [mean_ϕ_2_full, std_ϕ_2_full], [L"$\langle\varphi_2\rangle$ full", L"$\Sigma_2$ full"])

save("two_osc_fullstats_200"*"_angles"*".png", f_ϕ)

f_ϕ

##

f_ω = Figure(resolution=(1200, 800));
ax1 = Axis(f_ω[1, 1], xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[3,2:end,i_N], marker=marker1, markersize=size1, color = color1)
end
mean_ω_1_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[4,2:end,i_N], marker=:rect, markersize=size2, color = color2)
end
std_ω_1_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

#Makie.ylims!(ax, -0.5, 1.3)
Legend(f_ω[1,2], [mean_ω_1_full, std_ω_1_full], [L"$\langle\omega_1\rangle$ full", L"$\sigma_1$ full"])

ax2 = Axis(f_ω[2, 1], xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[5,2:end,i_N], marker=marker1, markersize=size1, color = color1)
end
mean_ω_2_full =  Makie.scatter!(empty([0]), empty([0]), marker=marker1, markersize=size1, color = color1[1], labels=L"$\langle\varphi_2\rangle$ full")

for i_N in 1:N_per_n
  Makie.scatter!(ns[2:end], af_full[6,2:end,i_N], marker=:rect, markersize=size2, color = color2)
end
std_ω_2_full = Makie.scatter!(empty([0]), empty([0]), marker=marker2, markersize=size2, color = color2[1], labels=L"$\Sigma\varphi_2$ full")

Legend(f_ω[2,2], [mean_ω_2_full, std_ω_2_full], [L"$\langle\omega_2\rangle$ full", L"$\sigma_2$ full"])

save("two_osc_fullstats_200"*"_freqs"*".png", f_ω)

f_ω