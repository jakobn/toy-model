"""
Animations of the toy model oscillators as masses moving on a circle.
Animations of the trajectories of phase velocity and mean power flow as plots.
"""
#
using Plots
using LaTeXStrings
using Images

include(joinpath(@__DIR__,"harmonic_osc.jl"))
include(joinpath(@__DIR__,"simulation_plotting_sampling_tools.jl"))

### make ring inbackground


scaler = 2
fontsize = scaler * 12
# size_inches = scaler .* (2*(3+3/8),  2*(3+3/8))
# size_pt = Int.(72 .* size_inches)
# size_pt = size_pt .- mod.(size_pt,2) .+ (1,0)# to ensure even width and height for mp4
size_pt = (970,970)
# check which values work (annoying!)
# (970,970) works and (1942,970)
# for i in -2:2, j in -2:2
# size_pt = (972+i,972+j)
# save("test_"*"$(size_pt[1])"*"x"*"$(size_pt[2])"*".png",Plots.plot(1, size=size_pt))
# # end
# for i in -2:2, j in -2:2
#     size_pt = (2*972+i,972 + j)
#     save("test_"*"$(size_pt[1])"*"x"*"$(size_pt[2])"*".png",Plots.plot(1, size=size_pt))
# end

### start off with minimal two node toy model (harm simpler)
ode_function = two_osc_harm_simpler!
α = 0.1
K = 6.0
n = 6.0
P1 = 1.0
p = [α, K , n]
# initial conditions; fix phi_0 to 0


### We need higher sampling rate of the angles to make it smoother
## make new data, also with longer t_end so transient is over
ϕ_1_0 = asin(P1/K)
y0_sync = [ϕ_1_0,0.,0.,0]
fps = 24
resolution_factor = 10
t_step_1 = 1/(fps*resolution_factor) # 1 time unit gets this amount of frames to become 1 second
time_ratio = 10/10 # 40 time units become 10 seconds
t_start = 0
t_pert_reso = 10
t_pert_natu = 50
t_end = 200
t_span_sync = t_start : t_step_1 * time_ratio : t_pert_reso
t_span_reso = t_pert_reso: t_step_1 * time_ratio : t_pert_natu
t_span_natu = t_pert_natu : t_step_1 * time_ratio : t_end

prob_sync = ODEProblem(ode_function, y0_sync, (t_start, t_pert_reso), p)
sol_sync = solve(prob_sync, Tsit5(), tstops = t_span_sync, dt = t_step_1)
# solplot(sol_sync, (t_start, t_pert_reso))

y0_reso = sol_sync(t_pert_reso)
y0_reso[3] = 5.0 # the perturbation
y0_reso

prob_reso = ODEProblem(ode_function, y0_reso, (t_pert_reso, t_pert_natu), p)
sol_reso = solve(prob_reso, Tsit5(), tstops = t_span_reso, dt = t_step_1)
# solplot(sol_reso, (t_pert_reso, t_pert_natu))

y0_natu = sol_reso(t_pert_natu)
y0_natu[3] = 8.0 # the perturbation

prob_natu = ODEProblem(ode_function, y0_natu, (t_pert_natu, t_end), p)
sol_natu = solve(prob_natu, Tsit5(), tstops = t_span_natu, dt = t_step_1)
# solplot(sol_natu, (t_pert_natu, t_end))

sol_sync(t_span_sync, idxs=3).u
ϕ_1_trajectory = vcat(sol_sync(t_span_sync, idxs=1).u, sol_reso(t_span_reso, idxs=1).u, sol_natu(t_span_natu, idxs=1).u)
ϕ_2_trajectory = vcat(sol_sync(t_span_sync, idxs=2).u, sol_reso(t_span_reso, idxs=2).u, sol_natu(t_span_natu, idxs=2).u)
ω_1_trajectory = vcat(sol_sync(t_span_sync, idxs=3).u, sol_reso(t_span_reso, idxs=3).u, sol_natu(t_span_natu, idxs=3).u)
time_span_all = vcat(sol_sync(t_span_sync).t, sol_reso(t_span_reso).t, sol_natu(t_span_natu).t )

x_1_trajectory = cos.(ϕ_1_trajectory)
y_1_trajectory = sin.(ϕ_1_trajectory)
x_2_trajectory = cos.(ϕ_2_trajectory)
y_2_trajectory = sin.(ϕ_2_trajectory)

p12_trajectory = K * sin.(ϕ_1_trajectory .- ϕ_2_trajectory)
p12_trajectory_lin = K * (sin.(ϕ_1_trajectory) .- cos.(ϕ_1_trajectory).* ϕ_2_trajectory)

moving_average(vs,n) = [sum(@view vs[i:(i+n-1)])/n for i in 1:(length(vs)-(n-1))]

# mean over 7.5 seconds
averaging_points = Int(7.5/(t_step_1*time_ratio))
p12_mean = moving_average(p12_trajectory, averaging_points)
p12_mean_lin = moving_average(p12_trajectory_lin, averaging_points)

Plots.plot(time_span_all, ω_1_trajectory, xlabel=L"t", ylabel=L"\omega_1", legend=false)

p12_plt = Plots.plot(time_span_all, p12_trajectory, xlabel=L"t", ylabel=L"p_{12}", label="full");
Plots.plot!(time_span_all, p12_trajectory_lin, linestyle=:dash, label="linearized")

p12_mean_plt = Plots.plot(time_span_all[(averaging_points):end], p12_mean, xlabel="t", ylabel=L"\langle p_{12}\rangle", label="full");
Plots.plot!(time_span_all[(averaging_points):end], p12_mean_lin, linestyle=:dash, label="linearized")

#fix p12 series by repeating first element
p12_mean_lin_longer = vcat(repeat([p12_mean_lin[1]],averaging_points-1), p12_mean_lin)
Plots.plot(time_span_all, p12_mean_lin_longer, xlabel=L"t", ylabel=L"\langle p_{12}\rangle", label=false, size=size_pt, fontsize=fontsize)
# mind that full power flow is not what is in the ode_function here.
# Better use linearized.



## make animation of phases on circle
n_points_shown = 96
seriesalpha =  range(0, 1, length = n_points_shown)
linewidth = range(0, 10*scaler, length = n_points_shown)

show_time = 4 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time
ϕ = 0 : 2*pi/1000 : 2*pi

# pa = palette(:tab10)
# pa[1] #sprout
# pa[6] #root
dense_sprout_color = RGB(0.1216,0.4667,0.7059);
root_color = RGB(0.549,0.3373,0.2941);

anim_circle_smoother = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    x_1_trajectory_plot = @view x_1_trajectory[index:index+n_points_shown-1]
    y_1_trajectory_plot = @view y_1_trajectory[index:index+n_points_shown-1]
    x_2_trajectory_plot = @view x_2_trajectory[index:index+n_points_shown-1]
    y_2_trajectory_plot = @view y_2_trajectory[index:index+n_points_shown-1]
    
    # make gray ring in background
    Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt)

    Plots.plot!(x_1_trajectory_plot, y_1_trajectory_plot, color=dense_sprout_color,
        seriesalpha=seriesalpha, linewidth=linewidth, label=false);
    Plots.plot!(1,color=dense_sprout_color,label=L"\varphi_1");
    Plots.plot!(x_2_trajectory_plot, y_2_trajectory_plot, color=root_color,
        seriesalpha=seriesalpha, linewidth=linewidth,
        aspect_ratio=1, label=false);
    Plots.plot!(1,color=root_color,label=L"\varphi_2")
    Plots.annotate!(0,0,raw"$t = " * string(round(value, digits=1)) * raw"$")
    # should make it: t_pert_reso <= value < etc.
    if t_pert_reso  < value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_1 = 5.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  < value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_1 = 8.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
print("\r"*string(value))
end every (resolution_factor)

mp4(anim_circle_smoother, "animation_minimal_toy_phases_circle_200t_smoother_96.mp4", fps = 24)

# gif(anim_circle_smoother, "animation_minimal_toy_phases_circle_200t_smoother_96.gif", fps = 24)

# gif(anim_circle_smoother, "animation_minimal_toy_phases_circle_200t_smoother_96_60fps.gif", fps = 60)


##### make omega_1 and power flow plot

xlims = (time_span_all[1], time_span_all[end])
ylims_ω_1 = (min(-0.5, ω_1_trajectory...), max(ω_1_trajectory...))
ylims_p12 = (max(min(p12_mean...),-0.3), max(p12_mean...))
anim = @animate for (index, value) in enumerate(time_span_all)
    time_span_plot = @view time_span_all[1:index]
    ω_1_trajectory_plot = @view ω_1_trajectory[1:index]
    p12_mean_lin_longer_plot = @view p12_mean_lin_longer[1:index]

    plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, color=3,
        xticks=false, xlims=xlims, ylims=ylims_ω_1, #xlabel=L"t", 
        ylabel=L"\omega_1", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    plt_p12_mean = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, color=6,
        xlims=xlims, ylims=ylims_p12,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"\langle p_{12}\rangle_{7.5}", legend=:topright);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    Plots.annotate!(-15,1.3,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.3,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.3,("natural",fontsize), :red);
    end
    plt_ω_1_p12 = Plots.plot(plt_ω_1, plt_p12_mean, layout=(2,1), size=size_pt,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)
    
    print("\r"*string(value))
end every (resolution_factor)

mp4(anim, "animation_minimal_toy_omega_1_p12.mp4", fps = 24)

# gif(anim, "animation_minimal_toy_omega_1_p12.gif", fps = 24)

"""
# half long
anim_half_long = @animate for (index, value) in enumerate(time_span_all)
    time_span_plot = @view time_span_all[1:index]
    ω_1_trajectory_plot = @view ω_1_trajectory[1:index]
    p12_mean_lin_longer_plot = @view p12_mean_lin_longer[1:index]
    plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, xlims=xlims, ylims=ylims_ω_1,
        xlabel=L"t", ylabel=L"\omega_1", label=false, legend=:bottomright)
    Plots.vline!([t_pert_reso], color=2, label=L"set $\omega_1 = 5.0$")
    Plots.vline!([t_pert_natu], color=3, label=L"set $\omega_1 = 8.0$")
    plt_p12_mean = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, xlims=xlims, ylims=ylims_p12,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]),
        xlabel=L"t", ylabel=L"\langle p_{12}\rangle_{7.5}", legend=false)
    Plots.vline!([t_pert_reso], color=2, label=L"set $\omega_1 = 5.0$")
    Plots.vline!([t_pert_natu], color=3, label=L"set $\omega_1 = 8.0$")
    Plots.annotate!(5,1.6,"sync");
    Plots.annotate!(30,1.6,"resonance");
    Plots.annotate!(75,1.6,"natural");
    plt_ω_1_p12 = Plots.plot(plt_ω_1, plt_p12_mean, layout=(2,1), size=size_pt, fontsize=fontsize)
end every 2
gif(anim_half_long, "animation_minimal_toy_omega_1_p12_half_long.gif", fps = 24)
# there seems to be a bug with the LaTeXStrings if several labels are supplied in vline!

# taking every step makes the file huge: 7 MB instead of 100 KB.
# half the fps
gif(anim_half_long, "animation_minimal_toy_omega_1_p12_12fps.gif", fps = 12)
"""
#

## to try things before doing whole animation
time_span_plot = time_span_all
ω_1_trajectory_plot = ω_1_trajectory
p12_mean_lin_longer_plot = p12_mean_lin_longer
value = t_end

plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, color=3,
        xticks=false, xlims=xlims, ylims=ylims_ω_1, #xlabel=L"t", 
        ylabel=L"\omega_1", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
plt_p12_mean_wo_circ = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, color=6,
        xlims=xlims, ylims=ylims_p12,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"\langle p_{12}\rangle_{7.5}", legend=:topright);
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
Plots.annotate!(-15,1.3,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.3,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.3,("natural",fontsize), :red);
    end
plt_ω_1_p12 = Plots.plot(plt_ω_1, plt_p12_mean_wo_circ, layout=(2,1), size=size_pt,
    labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)






##### circle plot together with line plots
l = @layout [
    [a{0.5h}; b{0.4h}] c
]

# size_inches_all = scaler .* (2*2*(3+3/8),  2*(3+3/8))
# size_pt_all = Int.(72 .* size_inches_all)
# size_pt_all = size_pt_all .- mod.(size_pt_all,2) .+ (1,0)
size_pt_all = (1942,970)
resolution_factor # as above
# prerequisits for line plots
    # have to rename xlims for one of the plots:
    # annotate "sync" "resonance" "natural"  at higher point because margin is higher in composite plot of all plots
xlims_ω_1_p12 = (time_span_all[1], time_span_all[end])
ylims_ω_1 = (min(-0.5, ω_1_trajectory...), max(ω_1_trajectory...))
ylims_p12 = (max(min(p12_mean...),-0.3), max(p12_mean...))

# prerequisits for circle plot
n_points_shown = 96
seriesalpha =  range(0, 1, length = n_points_shown)
linewidth_circ = range(0, 10*scaler, length = n_points_shown)

show_time = 4 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time
ϕ = 0 : 2*pi/1000 : 2*pi

dense_sprout_color = RGB(0.1216,0.4667,0.7059);
root_color = RGB(0.549,0.3373,0.2941);

# animation
anim_all = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    ω_1_trajectory_plot = @view ω_1_trajectory[1:index]
    p12_mean_lin_longer_plot = @view p12_mean_lin_longer[1:index]

    plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, color=3,
        xticks=false, xlims=xlims_ω_1_p12, ylims=ylims_ω_1, #xlabel=L"t", 
        ylabel=L"\omega_1", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    plt_p12_mean_for_all = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, color=6,
        xlims=xlims_ω_1_p12, ylims=ylims_p12,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"\langle p_{12}\rangle_{7.5}", legend=:topright);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    Plots.annotate!(-15,1.5,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.5,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.5,("natural",fontsize), :red);
    end
    plt_ω_1_p12 = Plots.plot(plt_ω_1, plt_p12_mean_for_all, layout=(2,1), size=size_pt,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)
    ### circle
    time_span_plot = @view time_span_all[1:index]
    x_1_trajectory_plot = @view x_1_trajectory[index:index+n_points_shown-1]
    y_1_trajectory_plot = @view y_1_trajectory[index:index+n_points_shown-1]
    x_2_trajectory_plot = @view x_2_trajectory[index:index+n_points_shown-1]
    y_2_trajectory_plot = @view y_2_trajectory[index:index+n_points_shown-1]
    # make gray ring in background
    plt_circ = Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt)
    Plots.plot!(x_1_trajectory_plot, y_1_trajectory_plot, color=dense_sprout_color,
        seriesalpha=seriesalpha, linewidth=linewidth_circ, label=false);
    Plots.plot!(1,color=dense_sprout_color,label=L"\varphi_1");
    Plots.plot!(x_2_trajectory_plot, y_2_trajectory_plot, color=root_color,
        seriesalpha=seriesalpha, linewidth=linewidth_circ,
        aspect_ratio=1, label=false);
    Plots.plot!(1,color=root_color,label=L"\varphi_2")
    Plots.annotate!(0,0,raw"$t = " * string(round(value, digits=1)) * raw"$")
    # should make it: t_pert_reso <= value < etc.
    if t_pert_reso  < value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_1 = 5.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  < value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_1 = 8.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    plt_all = Plots.plot(plt_ω_1, plt_p12_mean_for_all, plt_circ, layout=l, size=size_pt_all,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize,
        leftmargin=[10Plots.mm 10Plots.mm 30Plots.mm],
        topmargin=[0Plots.mm 0Plots.mm 0Plots.mm])
    print("\r"*string(value))
end every (resolution_factor)

mp4(anim_all, "animation_toy_model.mp4", fps=24)







       
        

#same 
# plt_all_nested = Plots.plot(plt_ω_1_p12, plt_circ, layout=(1,2), size=size_pt_all,
# labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize,
# leftmargin=10Plots.mm)


"""
# old data
ϕ_1_0 = asin(P1/K)
y0_sync = [ϕ_1_0,0.,0.,0]
fps = 24
t_step_1 = 1/(fps) # 1 time unit gets this amount of frames to become 1 second
time_ratio = 20/10 # 40 time units become 10 seconds
t_start = 0
t_pert_reso = 10
t_pert_natu = 50
t_end = 100
t_span_sync = t_start : t_step_1 * time_ratio : t_pert_reso
t_span_reso = t_pert_reso: t_step_1 * time_ratio : t_pert_natu
t_span_natu = t_pert_natu : t_step_1 * time_ratio : t_end

prob_sync = ODEProblem(ode_function, y0_sync, (t_start, t_pert_reso), p)
sol_sync = solve(prob_sync, Tsit5(), tstops = t_span_sync, dt = t_step_1)
solplot(sol_sync, (t_start, t_pert_reso))

y0_reso = sol_sync(t_pert_reso)
y0_reso[3] = 5.0 # the perturbation
y0_reso

prob_reso = ODEProblem(ode_function, y0_reso, (t_pert_reso, t_pert_natu), p)
sol_reso = solve(prob_reso, Tsit5(), tstops = t_span_reso, dt = t_step_1)
solplot(sol_reso, (t_pert_reso, t_pert_natu))

y0_natu = sol_reso(t_pert_natu)
y0_natu[3] = 8.0 # the perturbation

prob_natu = ODEProblem(ode_function, y0_natu, (t_pert_natu, t_end), p)
sol_natu = solve(prob_natu, Tsit5(), tstops = t_span_natu, dt = t_step_1)
solplot(sol_natu, (t_pert_natu, t_end))

t_end_longer = 200
t_span_natu_longer = t_end : t_step_1 * time_ratio : t_end_longer
prob_natu_longer = ODEProblem(ode_function, sol_natu[end], (t_end, t_end_longer), p)
sol_natu_longer = solve(prob_natu_longer, Tsit5(), tstops = t_span_natu_longer, dt = t_step)
solplot(sol_natu_longer, (t_end, t_end_longer))

sol_sync(t_span_sync, idxs=3).u
ϕ_1_trajectory = vcat(sol_sync(t_span_sync, idxs=1).u, sol_reso(t_span_reso, idxs=1).u, sol_natu(t_span_natu, idxs=1).u)
ϕ_2_trajectory = vcat(sol_sync(t_span_sync, idxs=2).u, sol_reso(t_span_reso, idxs=2).u, sol_natu(t_span_natu, idxs=2).u)
"""

"""
# from tutorial, does not work :(
@userplot Omega1Power12
@Plots.recipe function f(OP::Omega1Power12)
    time_span_all, ω_1_trajectory, p12_mean, size_pt, fontsize = OP.args
    xlims = (time_span_all[1], time_span_all[end])
    ylims_ω_1 = (min(ω_1_trajectory...), max(ω_1_trajectory...))
    ylims_p12 = (min(p12_mean...), max(p12_mean...))
    plt_ω_1 =  Plots.plot(time_span_all, ω_1_trajectory, xlims=xlims, ylims=ylims_ω_1, xlabel=L"t", ylabel=L"\omega_1", legend=false)
    plt_p12_mean = Plots.plot(time_span_all, p12_mean, xlims=xlims, ylims=ylims_p12, xlabel=L"t", ylabel=L"\langle p_{12}\rangle", label=false)

    layout --> (2,1)
    size --> size_pt
    fontsize --> fontsize
    plt_ω_1, plt_p12_mean
end
   
omega1power12(time_span_all, ω_1_trajectory, p12_mean_lin_longer, size_pt, fontsize)(
"""


"""
##### old circular plot of phase angles
x_1_trajectory = cos.(ϕ_1_trajectory)
y_1_trajectory = sin.(ϕ_1_trajectory)
x_2_trajectory = cos.(ϕ_2_trajectory)
y_2_trajectory = sin.(ϕ_2_trajectory)

n_points_shown = 12
seriesalpha =  range(0, 1, length = n_points_shown)
linewidth = range(0, 10, length = n_points_shown)

show_time = 4 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time

anim_circle = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    x_1_trajectory_plot = @view x_1_trajectory[index:index+n_points_shown-1]
    y_1_trajectory_plot = @view y_1_trajectory[index:index+n_points_shown-1]
    x_2_trajectory_plot = @view x_2_trajectory[index:index+n_points_shown-1]
    y_2_trajectory_plot = @view y_2_trajectory[index:index+n_points_shown-1]
    Plots.plot(x_1_trajectory_plot, y_1_trajectory_plot,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=seriesalpha, linewidth=linewidth,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt);
    Plots.plot!(1,color=1,label=L"\varphi_1");
    Plots.plot!(x_2_trajectory_plot, y_2_trajectory_plot,
        color=2, seriesalpha=seriesalpha, linewidth=linewidth,
        aspect_ratio=1, label=false);
    Plots.plot!(1,color=2,label=L"\varphi_2")
    Plots.annotate!(0,0,raw"$t = " * string(round(value, digits=1)) * raw"$")
    
    if t_pert_reso  < value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_1 = 5.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  < value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_1 = 8.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
end every 2
gif(anim_circle, "animation_minimal_toy_phases_circle.gif", fps = 12)
gif(anim_circle, "animation_minimal_toy_phases_circle_half_long.gif", fps = 24)


## to try things before doing the whole animation
index=300
value = time_span_all[index]
time_span_plot = @view time_span_all[1:index]
x_1_trajectory_plot = @view x_1_trajectory[index:index+n_points_shown-1]
y_1_trajectory_plot = @view y_1_trajectory[index:index+n_points_shown-1]
x_2_trajectory_plot = @view x_2_trajectory[index:index+n_points_shown-1]
y_2_trajectory_plot = @view y_2_trajectory[index:index+n_points_shown-1]

Plots.plot(x_1_trajectory_plot, y_1_trajectory_plot, xlims=(-1.1,1.2), ylims=(-1.1,1.2),
    seriesalpha=seriesalpha, linewidth=linewidth,
    xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
    aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
    tickfontsize=fontsize, size=size_pt);
Plots.plot!(1,color=1,label=L"\varphi_1");
Plots.plot!(x_2_trajectory_plot, y_2_trajectory_plot,
    color=2, seriesalpha=seriesalpha, linewidth=linewidth,
    aspect_ratio=1, label=false);
Plots.plot!(1,color=2,label=L"\varphi_2");
Plots.annotate!(0,0,string(round(value, digits=1)));
Plots.annotate!(0,0.5,Plots.text(L"set $\omega_1 = 5.0$", color=RGBA(0,0,0,0.5)))

Plots.annotate!(-1,1,raw"$t = " * string(round(value, digits=1)) * raw"$")
"""








##### make shorter : 125s
scaler = 2
fontsize = scaler * 12
# size_inches = scaler .* (2*(3+3/8),  2*(3+3/8))
# size_pt = Int.(72 .* size_inches)
# size_pt = size_pt .- mod.(size_pt,2) .+ (1,0)# to ensure even width and height for mp4
size_pt = (970,970)
# check which values work (annoying!)
# (970,970) works and (1942,970)
# for i in -2:2, j in -2:2
# size_pt = (972+i,972+j)
# save("test_"*"$(size_pt[1])"*"x"*"$(size_pt[2])"*".png",Plots.plot(1, size=size_pt))
# # end
# for i in -2:2, j in -2:2
#     size_pt = (2*972+i,972 + j)
#     save("test_"*"$(size_pt[1])"*"x"*"$(size_pt[2])"*".png",Plots.plot(1, size=size_pt))
# end

### start off with minimal two node toy model (harm simpler)
ode_function = two_osc_harm_simpler!
α = 0.1
K = 6.0
n = 6.0
P1 = 1.0
p = [α, K , n]
# initial conditions; fix phi_0 to 0


### We need higher sampling rate of the angles to make it smoother
## make new data, also with longer t_end so transient is over
ϕ_1_0 = asin(P1/K)
y0_sync = [ϕ_1_0,0.,0.,0]
fps = 24
resolution_factor = 10
t_step_1 = 1/(fps*resolution_factor) # 1 time unit gets this amount of frames to become 1 second
time_ratio = 10/10 # 40 time units become 10 seconds
t_start = 0
t_pert_reso = 10
t_pert_natu = 50
t_end = 125
t_span_sync = t_start : t_step_1 * time_ratio : t_pert_reso
t_span_reso = t_pert_reso: t_step_1 * time_ratio : t_pert_natu
t_span_natu = t_pert_natu : t_step_1 * time_ratio : t_end

prob_sync = ODEProblem(ode_function, y0_sync, (t_start, t_pert_reso), p)
sol_sync = solve(prob_sync, Tsit5(), tstops = t_span_sync, dt = t_step_1)
# solplot(sol_sync, (t_start, t_pert_reso))

y0_reso = sol_sync(t_pert_reso)
y0_reso[3] = 5.0 # the perturbation
y0_reso

prob_reso = ODEProblem(ode_function, y0_reso, (t_pert_reso, t_pert_natu), p)
sol_reso = solve(prob_reso, Tsit5(), tstops = t_span_reso, dt = t_step_1)
# solplot(sol_reso, (t_pert_reso, t_pert_natu))

y0_natu = sol_reso(t_pert_natu)
y0_natu[3] = 8.0 # the perturbation

prob_natu = ODEProblem(ode_function, y0_natu, (t_pert_natu, t_end), p)
sol_natu = solve(prob_natu, Tsit5(), tstops = t_span_natu, dt = t_step_1)
# solplot(sol_natu, (t_pert_natu, t_end))

sol_sync(t_span_sync, idxs=3).u
ϕ_1_trajectory = vcat(sol_sync(t_span_sync, idxs=1).u, sol_reso(t_span_reso, idxs=1).u, sol_natu(t_span_natu, idxs=1).u)
ϕ_2_trajectory = vcat(sol_sync(t_span_sync, idxs=2).u, sol_reso(t_span_reso, idxs=2).u, sol_natu(t_span_natu, idxs=2).u)
ω_1_trajectory = vcat(sol_sync(t_span_sync, idxs=3).u, sol_reso(t_span_reso, idxs=3).u, sol_natu(t_span_natu, idxs=3).u)
time_span_all = vcat(sol_sync(t_span_sync).t, sol_reso(t_span_reso).t, sol_natu(t_span_natu).t )

x_1_trajectory = cos.(ϕ_1_trajectory)
y_1_trajectory = sin.(ϕ_1_trajectory)
x_2_trajectory = cos.(ϕ_2_trajectory)
y_2_trajectory = sin.(ϕ_2_trajectory)

p12_trajectory = K * sin.(ϕ_1_trajectory .- ϕ_2_trajectory)
p12_trajectory_lin = K * (sin.(ϕ_1_trajectory) .- cos.(ϕ_1_trajectory).* ϕ_2_trajectory)

moving_average(vs,n) = [sum(@view vs[i:(i+n-1)])/n for i in 1:(length(vs)-(n-1))]

# mean over 7.5 seconds
averaging_points = Int(7.5/(t_step_1*time_ratio))
p12_mean = moving_average(p12_trajectory, averaging_points)
p12_mean_lin = moving_average(p12_trajectory_lin, averaging_points)

Plots.plot(time_span_all, ω_1_trajectory, xlabel=L"t", ylabel=L"\omega_1", legend=false)

p12_plt = Plots.plot(time_span_all, p12_trajectory, xlabel=L"t", ylabel=L"p_{12}", label="full");
Plots.plot!(time_span_all, p12_trajectory_lin, linestyle=:dash, label="linearized")

p12_mean_plt = Plots.plot(time_span_all[(averaging_points):end], p12_mean, xlabel="t", ylabel=L"\langle p_{12}\rangle", label="full");
Plots.plot!(time_span_all[(averaging_points):end], p12_mean_lin, linestyle=:dash, label="linearized")

#fix p12 series by repeating first element
p12_mean_lin_longer = vcat(repeat([p12_mean_lin[1]],averaging_points-1), p12_mean_lin)
Plots.plot(time_span_all, p12_mean_lin_longer, xlabel=L"t", ylabel=L"\langle p_{12}\rangle", label=false, size=size_pt, fontsize=fontsize)
# mind that full power flow is not what is in the ode_function here.
# Better use linearized.



## make animation of phases on circle
n_points_shown = 96
seriesalpha =  range(0, 1, length = n_points_shown)
linewidth = range(0, 10*scaler, length = n_points_shown)

show_time = 4 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time
ϕ = 0 : 2*pi/1000 : 2*pi

# pa = palette(:tab10)
# pa[1] #sprout
# pa[6] #root
dense_sprout_color = RGB(0.1216,0.4667,0.7059);
root_color = RGB(0.549,0.3373,0.2941);

anim_circle_smoother = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    x_1_trajectory_plot = @view x_1_trajectory[index:index+n_points_shown-1]
    y_1_trajectory_plot = @view y_1_trajectory[index:index+n_points_shown-1]
    x_2_trajectory_plot = @view x_2_trajectory[index:index+n_points_shown-1]
    y_2_trajectory_plot = @view y_2_trajectory[index:index+n_points_shown-1]
    
    # make gray ring in background
    Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt)

    Plots.plot!(x_1_trajectory_plot, y_1_trajectory_plot, color=dense_sprout_color,
        seriesalpha=seriesalpha, linewidth=linewidth, label=false);
    Plots.plot!(1,color=dense_sprout_color,label=L"\varphi_1");
    Plots.plot!(x_2_trajectory_plot, y_2_trajectory_plot, color=root_color,
        seriesalpha=seriesalpha, linewidth=linewidth,
        aspect_ratio=1, label=false);
    Plots.plot!(1,color=root_color,label=L"\varphi_2")
    Plots.annotate!(0,0,raw"$t = " * string(round(value, digits=1)) * raw"$")
    # should make it: t_pert_reso <= value < etc.
    if t_pert_reso  < value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_1 = 5.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  < value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_1 = 8.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
print("\r"*string(value))
end every (resolution_factor)

mp4(anim_circle_smoother, "animation_minimal_toy_phases_circle_125t_smoother_96.mp4", fps = 24)

##### make omega_1 and power flow plot

### make final frame as plot
xlims = (time_span_all[1], time_span_all[end])
ylims_ω_1 = (min(-0.5, ω_1_trajectory...), max(ω_1_trajectory...))
ylims_p12 = (max(min(p12_mean...),-0.3), max(p12_mean...))
index = length(time_span_all)
value = time_span_all[end]
time_span_plot = @view time_span_all[1:index]
ω_1_trajectory_plot = @view ω_1_trajectory[1:index]
p12_mean_lin_longer_plot = @view p12_mean_lin_longer[1:index]

plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, color=3,
    xticks=false, xlims=xlims, ylims=ylims_ω_1, #xlabel=L"t", 
    ylabel=L"\omega_1", label=false, legend=:bottomright);
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
plt_p12_mean = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, color=6,
    xlims=xlims, ylims=ylims_p12,
    xticks=false, yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
    ylabel=L"\langle p_{12}\rangle_{7.5}", legend=false); #corrected legend position from :topright here to :bottomright in upper plot (ω₁)
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
Plots.annotate!(-15,1.3,("sync",fontsize,:green));
if value > t_pert_reso
    Plots.annotate!(30,1.3,("resonance",fontsize, :orange));
end
if value > t_pert_natu
    Plots.annotate!(75,1.3,("natural",fontsize), :red);
end
# add power flow
p12_plt = Plots.plot(time_span_all, p12_trajectory, xlims=xlims, xlabel=L"t", ylabel=L"p_{12}", label="full");
Plots.plot!(time_span_all, p12_trajectory_lin, linestyle=:dash, label="linearized");
Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=false);
Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=false);

p12_plt
# resize
# size_pt_last_frame = (2*970,3*970) # did not use
plt_ω_1_p12_mean_and_time = Plots.plot(plt_ω_1, plt_p12_mean, p12_plt, layout=(3,1), size=size_pt,
    labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)

# save("two_node_toy_omega_mean_and_time_power_flow_trajectories.pdf",plt_ω_1_p12_mean_and_time) # 500 kB
save("two_node_toy_omega_mean_and_time_power_flow_trajectories.png",plt_ω_1_p12_mean_and_time) #250 kB
### make animation

xlims = (time_span_all[1], time_span_all[end])
ylims_ω_1 = (min(-0.5, ω_1_trajectory...), max(ω_1_trajectory...))
ylims_p12 = (max(min(p12_mean...),-0.3), max(p12_mean...))
anim = @animate for (index, value) in enumerate(time_span_all)
    time_span_plot = @view time_span_all[1:index]
    ω_1_trajectory_plot = @view ω_1_trajectory[1:index]
    p12_mean_lin_longer_plot = @view p12_mean_lin_longer[1:index]

    plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, color=3,
        xticks=false, xlims=xlims, ylims=ylims_ω_1, #xlabel=L"t", 
        ylabel=L"\omega_1", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    plt_p12_mean = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, color=6,
        xlims=xlims, ylims=ylims_p12,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"\langle p_{12}\rangle_{7.5}", legend=:topright);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    Plots.annotate!(-15,1.3,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.3,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.3,("natural",fontsize), :red);
    end
    plt_ω_1_p12 = Plots.plot(plt_ω_1, plt_p12_mean, layout=(2,1), size=size_pt,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)
    
    print("\r"*string(value))
end every (resolution_factor)

mp4(anim, "animation_minimal_toy_t125_omega_1_p12.mp4", fps = 24)


##### circle plot together with line plots
l = @layout [
    [a{0.5h}; b{0.4h}] c
]

# size_inches_all = scaler .* (2*2*(3+3/8),  2*(3+3/8))
# size_pt_all = Int.(72 .* size_inches_all)
# size_pt_all = size_pt_all .- mod.(size_pt_all,2) .+ (1,0)
size_pt_all = (1942,970)
resolution_factor # as above
# prerequisits for line plots
    # have to rename xlims for one of the plots:
    # annotate "sync" "resonance" "natural"  at higher point because margin is higher in composite plot of all plots
xlims_ω_1_p12 = (time_span_all[1], time_span_all[end])
ylims_ω_1 = (min(-0.5, ω_1_trajectory...), max(ω_1_trajectory...))
ylims_p12 = (max(min(p12_mean...),-0.3), max(p12_mean...))

# prerequisits for circle plot
n_points_shown = 96
seriesalpha =  range(0, 1, length = n_points_shown)
linewidth_circ = range(0, 10*scaler, length = n_points_shown)

show_time = 4 #seconds at 24 fps, or at 12 if every 2nd is shown
show_frame_width = time_ratio * show_time
ϕ = 0 : 2*pi/1000 : 2*pi

dense_sprout_color = RGB(0.1216,0.4667,0.7059);
root_color = RGB(0.549,0.3373,0.2941);

# animation
anim_all = @animate for (index, value) in enumerate(time_span_all[1:end-n_points_shown])
    time_span_plot = @view time_span_all[1:index]
    ω_1_trajectory_plot = @view ω_1_trajectory[1:index]
    p12_mean_lin_longer_plot = @view p12_mean_lin_longer[1:index]

    plt_ω_1 =  Plots.plot(time_span_plot, ω_1_trajectory_plot, color=3,
        xticks=false, xlims=xlims_ω_1_p12, ylims=ylims_ω_1, #xlabel=L"t", 
        ylabel=L"\omega_1", label=false, legend=false);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    plt_p12_mean_for_all = Plots.plot(time_span_plot, p12_mean_lin_longer_plot, color=6,
        xlims=xlims_ω_1_p12, ylims=ylims_p12,
        yticks = ([0,0.5,1.0],["0.0","0.5","1.0"]), label=false,
        xlabel=L"t", ylabel=L"\langle p_{12}\rangle_{7.5}", legend=:topright);
    Plots.vline!([t_pert_reso], color=4, linewidth=scaler, label=L"set $\omega_1 = 5.0$");
    Plots.vline!([t_pert_natu], color=5, linewidth=scaler, label=L"set $\omega_1 = 8.0$");
    Plots.annotate!(-15,1.5,("sync",fontsize,:green));
    if value > t_pert_reso
        Plots.annotate!(30,1.5,("resonance",fontsize, :orange));
    end
    if value > t_pert_natu
        Plots.annotate!(75,1.5,("natural",fontsize), :red);
    end
    plt_ω_1_p12 = Plots.plot(plt_ω_1, plt_p12_mean_for_all, layout=(2,1), size=size_pt,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize, rightmargin=7Plots.mm)
    ### circle
    time_span_plot = @view time_span_all[1:index]
    x_1_trajectory_plot = @view x_1_trajectory[index:index+n_points_shown-1]
    y_1_trajectory_plot = @view y_1_trajectory[index:index+n_points_shown-1]
    x_2_trajectory_plot = @view x_2_trajectory[index:index+n_points_shown-1]
    y_2_trajectory_plot = @view y_2_trajectory[index:index+n_points_shown-1]
    # make gray ring in background
    plt_circ = Plots.plot(cos.(ϕ),sin.(ϕ) ,xlims=(-1.1,1.2), ylims=(-1.1,1.2),
        seriesalpha=0.5, linewidth= scaler, color=:gray,
        xlabel=L"\cos\varphi_i", ylabel=L"\sin\varphi_i",
        aspect_ratio=1, label=false, labelfontsize=fontsize, legendfontsize=fontsize,
        tickfontsize=fontsize, size=size_pt)
    Plots.plot!(x_1_trajectory_plot, y_1_trajectory_plot, color=dense_sprout_color,
        seriesalpha=seriesalpha, linewidth=linewidth_circ, label=false);
    Plots.plot!(1,color=dense_sprout_color,label=L"\varphi_1");
    Plots.plot!(x_2_trajectory_plot, y_2_trajectory_plot, color=root_color,
        seriesalpha=seriesalpha, linewidth=linewidth_circ,
        aspect_ratio=1, label=false);
    Plots.plot!(1,color=root_color,label=L"\varphi_2")
    Plots.annotate!(0,0,raw"$t = " * string(round(value, digits=1)) * raw"$")
    # should make it: t_pert_reso <= value < etc.
    if t_pert_reso  < value < t_pert_reso + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_reso)/show_frame_width
        Plots.annotate!(0,0.5,Plots.text(L"set $\omega_1 = 5.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    if t_pert_natu  < value < t_pert_natu + show_frame_width
        annotate_alpha = 1 - abs(value - t_pert_natu)/show_frame_width
        Plots.annotate!(0,-0.5,Plots.text(L"set $\omega_1 = 8.0$", color=RGBA(0,0,0,annotate_alpha)))
    end
    plt_all = Plots.plot(plt_ω_1, plt_p12_mean_for_all, plt_circ, layout=l, size=size_pt_all,
        labelfontsize=fontsize, legendfontsize=fontsize, tickfontsize=fontsize,
        leftmargin=[10Plots.mm 10Plots.mm 30Plots.mm],
        topmargin=[0Plots.mm 0Plots.mm 0Plots.mm])
    print("\r"*string(value))
end every (resolution_factor)

mp4(anim_all, "animation_toy_model_t125.mp4", fps=24)








####### complex network


























