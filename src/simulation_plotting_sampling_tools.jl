"""
Tools for Simulation, Plotting, Sampling Statistics form Data.
"""
#
"""
simulation(ode_function, y0, tspan, p; saveit=true)
simulates ode_function
with parameters p
and initial value y0
for time interval tspan

kwargs... can be dt, relot, absol, tstops, dtmin, dtmax etc.
e.g. tstops = 0. : 0.001 : 500.

returns solution sol
"""

function simulation(ode_function, y0, tspan, p; solver_kwargs...)
    prob = ODEProblem(ode_function, y0, tspan, p)
    sol = solve(prob, Tsit5(); solver_kwargs...)
end

"""
solplot(sol, plot_span)
plots trajectories of sol, assuming 4 variables (ϕ_1, ϕ_2, ω_1, ω_2)
for plot_span as plt
returns plt
"""
function solplot(sol, plot_span)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  plt1 = Plots.plot(t, sol(plot_range, idxs=1), ylabel=L"$\varphi_1$")
  plt2 = Plots.plot(t, sol(plot_range, idxs=2), ylabel=L"$\varphi_2$")
  plt3 = Plots.plot(t, sol(plot_range, idxs=3), ylabel=L"$\omega_1$")
  plt4 = Plots.plot(t, sol(plot_range, idxs=4), ylabel=L"$\omega_2$")
  plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis=L"t", legend=false) # xlims=plot_span
  # display(plt) #avoid double showing
  plt
end
# old with ugly legends instead of y-axis labels
#function solplot(sol, plot_span)
#    plt1 = Plots.plot(sol,vars=(0,1), labels=L"$\varphi_1$")
#    plt2 = Plots.plot(sol,vars=(0,2), labels=L"$\varphi_2$")
#    plt3 = Plots.plot(sol,vars=(0,3), labels=L"$\omega_1$")
#    plt4 = Plots.plot(sol,vars=(0,4), labels=L"$\omega_2$")
#    plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t", xlims=plot_span)
#    display(plt)
#    plt
#end
"""
solplot_omegas(sol, plot_span)
plots trajectories of all omega in sol, assuming variables (ϕ_1,..., ϕ_N, ω_1,...,ω_N)
all in one plot with a legend
for plot_span as plt
returns plt
"""
function solplot_omegas(sol, plot_span)
  t_step = (plot_span[2] - plot_span[1]) / 10000 
  plot_range = plot_span[1] : t_step : plot_span[2]
  t = sol(plot_range).t
  n_omegas = Int(length(sol.u[1])/2)
  label = raw"$\omega_1$"
  plt = Plots.plot(t, sol(plot_range, idxs=n_omegas+1),
    label=label ,xaxis="t", size = (700,500))
  for i in (n_omegas+2) : (2*n_omegas)
    label = raw"$\omega_{"* string(i - n_omegas) *  raw"}$"
    Plots.plot!(t, sol(plot_range, idxs=i), label=label)
  end
  plt
end
"""
saveplot saves the plot plt as name.pdf
the name is automatically generated using the name of ode_function and parameters assuming the form p =  [α, K , n]
optional argument add_name can be added to the end
file type is save_type, e.g. "pdf", "png"

for custom_name use save(custom_name, plt)
custom_name has to end with file type, e.g. ".pdf", ".png"
"""
function saveplot(plt, ode_function, p, save_type; add_name="")
    if add_name != ""
        add_name = "_"*add_name
    end
    function_name = strip(string(ode_function),'!') # has to be single quotation marks for strip()
    α, K, n = p
    plot_name = function_name*"_"*string(α)*"_"*string(K)*"_"*string(n)*add_name*"."*save_type
    save(plot_name, plt)
end

"""
simplotsave(ode_function, y0, tspan, p; save_it=false, custom_name="", add_name="")
simulates ode_function
with parameters p
and initial value y0
for time interval tspan

plots trajectories as in solplot() as plt

if save_type == "" (default): no saving
else: saves plt as type save_type
with custom_name
if custom_name = "", saves with automated name as in saveplot() with optional argument add_name

returns solution sol and plot plt
"""
function simplotsave(ode_function, y0, tspan, p, plot_span; save_type="", custom_name="", add_name="", solverkwargs...)
    sol = simulation(ode_function, y0, tspan, p; solverkwargs...)
    plt = solplot(sol, plot_span)
    if save_type != ""
        if custom_name != ""
            save(custom_name*"."*save_type, plt)
        else
            saveplot(plt, ode_function, p, save_type; add_name=add_name)
        end
    end
    return sol, plt
end


##

# generates Gaussian initial conditions
function generate_initial_conditions(dimension, ns, N_per_n, seednr, scaling)
  Random.seed!(seednr)
  ic = scaling .* randn(dimension, length(ns), N_per_n)
end





#####

# old
# function simplot(y0, tspan, p; saveit=true)
#     prob = ODEProblem(two_osc_harm_simpler!, y0, tspan, p)
#     sol = solve(prob, Tsit5())
#     plt1 = Plots.plot(sol,vars=(0,1))
#     plt2 = Plots.plot(sol,vars=(0,2))
#     plt3 = Plots.plot(sol,vars=(0,3))
#     plt4 = Plots.plot(sol,vars=(0,4))
#     plt = Plots.plot(plt1, plt2, plt3, plt4; layout=(2,2), size=(700,500), xaxis="t")
#     if saveit == true
#       α, K, n = p
#       name = "2osc_harm_simpler_"*string(α)*"_"*string(K)*"_"*string(n)*".png"
#       save(name, plt)
#     end
#     plt
# end

# ## be careful with the system dimension



function sample_asymptotic_frequencies(p)
    y0 = 5. * randn(4)
    prob = ODEProblem(two_osc_harm_simpler!, y0, (0., 500.),p)
    sol = solve(prob, Tsit5())
    mean(sol(200.:0.1:500., idxs=3).u), mean(sol(200.:0.1:500., idxs=4).u), y0
end

# ##

# #function sample_asymptotic_frequencies_2(p)
# #    to see the effect of phase shifts only
# #    y0 = [5 * randn(),0., 5 * randn(), 0]
# #    prob = ODEProblem(two_osc_harm_woP2_wodamp2_simpler!,y0,(0., 500.),p)
# #    sol = solve(prob, Tsit5())
# #    mean(sol(200.:0.1:500., idxs=3).u), mean(sol(200.:0.1:500., idxs=4).u)
# #end

# ##

# function sample_asymptotic_statistics(p)
#   y0 = 5. * randn(4)
#   prob = ODEProblem(two_osc!, y0, (0., 500.),p)
#   sol = solve(prob, Tsit5())
#   mean(sol(200.:0.1:500., idxs=2).u), std(sol(200.:0.1:500., idxs=2).u), mean(sol(200.:0.1:500., idxs=3).u), std(sol(200.:0.1:500., idxs=3).u), mean(sol(200.:0.1:500., idxs=4).u), std(sol(200.:0.1:500., idxs=4).u), y0
# end

# ##

function asymptotic_freqs(ns, N_per_n, α, K)
    af = zeros(2, length(ns), N_per_n)
    ic_dump = zeros(4, length(ns), N_per_n)
    print("\n Simulating")
    for (i_n, n) in enumerate(ns)
      print("\r Simulating $n ")
      for i_N in 1:N_per_n
        af[1, i_n, i_N], af[2, i_n, i_N], ic_dump[:, i_n, i_N] = sample_asymptotic_frequencies([α, K, n])  
      end
    end
    af, ic_dump
end

# ##

# function asymptotic_statistics(ns, N_per_n, α, K)
#   af = zeros(6, length(ns), N_per_n)
#   ic_dump = zeros(4, length(ns), N_per_n)
#   print("\n Simulating")
#   for (i_n, n) in enumerate(ns)
#     print("\r Simulating $n ")
#     for i_N in 1:N_per_n
#       af[1, i_n, i_N], af[2, i_n, i_N], af[3, i_n, i_N], af[4, i_n, i_N], af[5, i_n, i_N], af[6, i_n, i_N], ic_dump[:, i_n, i_N] = sample_asymptotic_statistics([α, K, n])  
#     end
#   end
#   af, ic_dump
# end

# now with useful initial conditions
# only single node perturbations

function sample_asymptotic_statistics(ode_function, p, y0, t_sim, t_stat_iter)
  prob = ODEProblem(ode_function, y0, t_sim, p)
  sol = solve(prob, Tsit5())
  # don't return y0 here
  mean(sol(t_stat_iter, idxs=2).u), std(sol(t_stat_iter, idxs=2).u), mean(sol(t_stat_iter, idxs=3).u), std(sol(t_stat_iter, idxs=3).u), mean(sol(t_stat_iter, idxs=4).u), std(sol(t_stat_iter, idxs=4).u)# , y0
end

function asymptotic_statistics(ode_function, ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
    n_initial_ϕ = length(perturbation_range_ϕ)
    n_initial_ω = length(perturbation_range_ω)
    dϕ_idx_range = 1:n_initial_ϕ
    dω_idx_range = 1:n_initial_ω
    n_idx_range = 1:length(ns)
    af = zeros(6, length(ns), n_initial_ϕ, n_initial_ω)
  # ic_dump = zeros(4, length(ns), N_per_n)
  # print("\n Simulating")
  for (dϕ_idx,  dω_idx, n_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, n_idx_range))
    n = ns[n_idx]
    dϕ = perturbation_range_ϕ[dϕ_idx]
    dω = perturbation_range_ω[dω_idx] + 0.5 * 10 # this is node 1 toy model specific
    print("\r Simulating $n ")
    y0 = [dϕ, 0, dω, 0]
    af[1, n_idx, dϕ_idx,  dω_idx], af[2, n_idx, dϕ_idx,  dω_idx], af[3, n_idx, dϕ_idx,  dω_idx], af[4, n_idx, dϕ_idx,  dω_idx], af[5, n_idx, dϕ_idx,  dω_idx], af[6, n_idx, dϕ_idx,  dω_idx] = sample_asymptotic_statistics(ode_function, [α, K, n], y0, t_sim, t_stat_iter)
  end
  af
end


# for systems with three Oscillators

function sample_asymptotic_statistics_3(ode_function, p, y0, t_sim, t_stat_iter)
  prob = ODEProblem(ode_function, y0, t_sim, p)
  sol = solve(prob, Tsit5())
  # don't return y0 here
  mean(sol(t_stat_iter, idxs=2).u), std(sol(t_stat_iter, idxs=2).u), mean(sol(t_stat_iter, idxs=3).u), std(sol(t_stat_iter, idxs=3).u), mean(sol(t_stat_iter, idxs=4).u), std(sol(t_stat_iter, idxs=4).u),  mean(sol(t_stat_iter, idxs=5).u), std(sol(t_stat_iter, idxs=5).u),  mean(sol(t_stat_iter, idxs=6).u), std(sol(t_stat_iter, idxs=6).u)# , y0
end

function asymptotic_statistics_3(ode_function, ns, α, K, perturbation_range_ϕ, perturbation_range_ω, t_sim, t_stat_iter)
    n_initial_ϕ = length(perturbation_range_ϕ)
    n_initial_ω = length(perturbation_range_ω)
    dϕ_idx_range = 1:n_initial_ϕ
    dω_idx_range = 1:n_initial_ω
    n_idx_range = 1:length(ns)
    af = zeros(10, length(ns), n_initial_ϕ, n_initial_ω)
  # ic_dump = zeros(4, length(ns), N_per_n)
  # print("\n Simulating")
  for (dϕ_idx,  dω_idx, n_idx) in collect(Iterators.product(dϕ_idx_range, dω_idx_range, n_idx_range))
    n = ns[n_idx]
    dϕ = perturbation_range_ϕ[dϕ_idx]
    dω = perturbation_range_ω[dω_idx] + 0.5 * 10 # this is node 1 toy model specific
    print("\r Simulating $n ")
    y0 = [dϕ, 0, 0, dω, 0, 0]
    af[1, n_idx, dϕ_idx,  dω_idx], af[2, n_idx, dϕ_idx,  dω_idx], af[3, n_idx, dϕ_idx,  dω_idx], af[4, n_idx, dϕ_idx,  dω_idx], af[5, n_idx, dϕ_idx,  dω_idx], af[6, n_idx, dϕ_idx,  dω_idx], af[7, n_idx, dϕ_idx,  dω_idx], af[8, n_idx, dϕ_idx,  dω_idx], af[9, n_idx, dϕ_idx,  dω_idx], af[10, n_idx, dϕ_idx,  dω_idx] = sample_asymptotic_statistics_3(ode_function, [α, K, n], y0, t_sim, t_stat_iter)
  end
  af
end
