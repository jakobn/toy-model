"""
Testground for three-oscillator system.
Simulation and Plotting.
"""
#
using OrdinaryDiffEq
using Random
using Statistics
using Plots
using CairoMakie
using LaTeXStrings
using JLD

function three_osc!(dy, y, p, t)
    α, K, n = p
    @views begin
      ϕ = y[1:3]
      ω = y[4:6]
      dϕ = dy[1:3]
      dω = dy[4:6]
    end
    dϕ[1] = ω[1]
    dϕ[2] = ω[2]
    dϕ[3] = ω[3]
    dω[1] = 1. - α * ω[1] - K * sin(ϕ[1] - ϕ[2])
    dω[2] = -1. - α * ω[2] - K * sin(ϕ[2] - ϕ[1]) - K * n * sin(ϕ[2] - ϕ[3])
    dω[3] = 0. - α * ω[3] - K * sin(ϕ[3] - ϕ[2])
    nothing
end

function three_osc_lin!(dy, y, p, t)
  α, K, n = p
  @views begin
    ϕ = y[1:3]
    ω = y[4:6]
    dϕ = dy[1:3]
    dω = dy[4:6]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dϕ[3] = ω[3]
  dω[1] = 1. - α * ω[1] - K * sin(ϕ[1]) + K * cos(ϕ[1]) * ϕ[2]
  dω[2] = -1. - α * ω[2] + K * sin(ϕ[1]) - K * cos(ϕ[1]) * ϕ[2] - K * n * (ϕ[2] - ϕ[3])
  dω[3] = 0. - α * ω[3] - K * (ϕ[3] - ϕ[2])
  nothing
end

function three_osc_lin_simpler!(dy, y, p, t)
  α, K, n = p
  @views begin
    ϕ = y[1:3]
    ω = y[4:6]
    dϕ = dy[1:3]
    dω = dy[4:6]
  end
  dϕ[1] = ω[1]
  dϕ[2] = ω[2]
  dϕ[3] = ω[3]
  dω[1] = 1. - α * ω[1] - K * sin(ϕ[1]) + K * cos(ϕ[1]) * ϕ[2]
  dω[2] = -1. - α * ω[2] + K * sin(ϕ[1]) - K * n * (ϕ[2] - ϕ[3])
  dω[3] = 0. - α * ω[3] - K * (ϕ[3] - ϕ[2])
  nothing
end

##

function sample_asymptotic_frequencies(p)
  # y0 = 5* randn(6)
  y0 = vcat( 2*pi*rand(3), 20*rand()-5, -(20*rand()-5), 20*rand()-10 ) 
  prob = ODEProblem(three_osc!, y0 ,(0., 1000.),p)
  sol = solve(prob, Tsit5())
  [mean(sol(700.:0.1:1000., idxs=4).u), mean(sol(700.:0.1:1000., idxs=5).u), mean(sol(700.:0.1:1000., idxs=6).u)]
end
  
##

function asymptotic_freqs(ns, N_per_n, α, K)
  af = zeros(3, length(ns), N_per_n)
  print("\n Simulating")
  for (i_n, n) in enumerate(ns)
    print("\r Simulating $n ")
    for i_N in 1:N_per_n
      af[1:3, i_n, i_N] = sample_asymptotic_frequencies([α, K, n])  
    end
  end
  af
end

##

α = 0.1
K = 6.0
ns = 0.:0.2:30.
N_per_n = 100
af = asymptotic_freqs(ns, N_per_n, α, K)
data_path = "C:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/3_osc_af"
JLD.save(joinpath(data_path, "3_osc_af.jld"), "af", af)
af = JLD.load(joinpath(data_path, "3_osc_af.jld"))["af"]



##

using CairoMakie
using LaTeXStrings
##

# column width is 3 + 3/8 in or 8.6 cm according to
# https://journals.aps.org/prl/authors#techformat
# reprint revtex 4.2 has 17.92 cm textwidth (single pagewide columns)
# preprint revtex 4.2 has 16.45 cm textwidth

# bitmap (png...): standard is 300 DPI
# ->~ (6+6/8) * 300 = 2025 horizontal
# 2/3 * (6+6/8) * 300 = 1350 vertical
# Frank used 1200 and 800

# vector graphics e.g. pdf: 1in = 72 pt.

# Can't make alpha to small or it will round to zero when cast to RGBA.
alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
color = (:black, alpha)
marker_size = 2

size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
# half the size does not work, looks stupid
size_pt = 72 .* size_inches
f = Figure(resolution=size_pt, fontsize=12);

Axis(f[1, 1], ylabel=L"$\langle\omega_1\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  # Makie.scatter!(ns, af[1,:,i_N]; color = color, legend = false)
  Makie.scatter!(ns, af[1,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[2, 1], ylabel=L"$\langle\omega_2\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[2,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[3, 1], ylabel=L"$\langle\omega_3\rangle$", xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[3,:,i_N]; color = color, markersize = marker_size)
end

f

# save("bif_3osc.png", f, px_per_unit = 5)
# size 60 KB
# save("bif_3osc.pdf", f, pt_per_unit = 1)
# size 800 KB


### for linearized dynamics

function sample_asymptotic_frequencies(p)
  # y0 = 5* randn(6)
  y0 = vcat( 2*pi*rand(3), 20*rand()-5, -(20*rand()-5), 20*rand()-10 ) 
  prob = ODEProblem(three_osc_lin!, y0 ,(0., 1000.),p)
  sol = solve(prob, Tsit5())
  [mean(sol(700.:0.1:1000., idxs=4).u), mean(sol(700.:0.1:1000., idxs=5).u), mean(sol(700.:0.1:1000., idxs=6).u)]
end
  
##

function asymptotic_freqs(ns, N_per_n, α, K)
  af = zeros(3, length(ns), N_per_n)
  print("\n Simulating")
  for (i_n, n) in enumerate(ns)
    print("\r Simulating $n ")
    for i_N in 1:N_per_n
      af[1:3, i_n, i_N] = sample_asymptotic_frequencies([α, K, n])  
    end
  end
  af
end


α = 0.1
K = 6.0
ns = 0.:0.2:30.
N_per_n = 100
af = asymptotic_freqs(ns, N_per_n, α, K)
data_path = "C:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/3_osc_af"
JLD.save(joinpath(data_path, "3_osc_lin_af.jld"), "af", af)
af = JLD.load(joinpath(data_path, "3_osc_lin_af.jld"))["af"]

color_alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
color = (:black, color_alpha)
marker_size = 2

size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
# half the size does not work, looks stupid
size_pt = 72 .* size_inches
f = Figure(resolution=size_pt, fontsize=12);

Axis(f[1, 1], ylabel=L"$\langle\omega_1\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  # Makie.scatter!(ns, af[1,:,i_N]; color = color, legend = false)
  Makie.scatter!(ns, af[1,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[2, 1], ylabel=L"$\langle\omega_2\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[2,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[3, 1], ylabel=L"$\langle\omega_3\rangle$", xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[3,:,i_N]; color = color, markersize = marker_size)
end

f
save("bif_3osc_harm.png", f, px_per_unit = 5)



### for linearized and simplified dynamics

function sample_asymptotic_frequencies(p)
  # y0 = 5* randn(6)
  y0 = vcat( 2*pi*rand(3), 20*rand()-5, -(20*rand()-5), 20*rand()-10 ) 
  prob = ODEProblem(three_osc_lin_simpler!, y0 ,(0., 1000.),p)
  sol = solve(prob, Tsit5())
  [mean(sol(700.:0.1:1000., idxs=4).u), mean(sol(700.:0.1:1000., idxs=5).u), mean(sol(700.:0.1:1000., idxs=6).u)]
end
  
##

function asymptotic_freqs(ns, N_per_n, α, K)
  af = zeros(3, length(ns), N_per_n)
  print("\n Simulating")
  for (i_n, n) in enumerate(ns)
    print("\r Simulating $n ")
    for i_N in 1:N_per_n
      af[1:3, i_n, i_N] = sample_asymptotic_frequencies([α, K, n])  
    end
  end
  af
end


α = 0.1
K = 6.0
ns = 0.:0.2:30.
N_per_n = 100
af = asymptotic_freqs(ns, N_per_n, α, K)
data_path = "C:/Users/niehu/Documents/UNI_surf/Masterarbeit_surf/toy-model/data/3_osc_af"
JLD.save(joinpath(data_path, "3_osc_lin_simpler_af.jld"), "af", af)
af = JLD.load(joinpath(data_path, "3_osc_lin_simpler_af.jld"))["af"]

color_alpha = 2. / N_per_n > 0.02 ? 2. / N_per_n : 0.02
color = (:black, color_alpha)
marker_size = 2

size_inches = (2* (3+3/8), 2/3 * 2 * (3+3/8))
# half the size does not work, looks stupid
size_pt = 72 .* size_inches
f = Figure(resolution=size_pt, fontsize=12);

Axis(f[1, 1], ylabel=L"$\langle\omega_1\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  # Makie.scatter!(ns, af[1,:,i_N]; color = color, legend = false)
  Makie.scatter!(ns, af[1,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[2, 1], ylabel=L"$\langle\omega_2\rangle$", xticklabelsvisible=false, xticksvisible=false)

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[2,:,i_N]; color = color, markersize = marker_size)
end

Axis(f[3, 1], ylabel=L"$\langle\omega_3\rangle$", xlabel=L"$n$")

for i_N in 1:N_per_n
  Makie.scatter!(ns, af[3,:,i_N]; color = color, markersize = marker_size)
end

f
save("bif_3osc_harm_simpler.png", f, px_per_unit = 5)