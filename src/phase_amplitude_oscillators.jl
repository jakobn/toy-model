"""
Oscillator models for normal form simulations in PowerDynamics.jl
"""
#
using NetworkDynamics: ODEVertex
using PowerDynamics
import PowerDynamics: dimension, symbolsof, construct_vertex 
import PowerDynamics: showdefinition

# note that this is not the convention of the paper,
# where the coefficients stand in front of the deviations, e.g. δp = p - P
# set τ_P = m/α, K_P = 1/α, τ_Q = 1/Γ to match SwingEqLVS
# new feature here is K_Q
@DynamicNode SchifferApprox(τ_P, τ_Q, K_P, K_Q, V_r, P, Q) begin
    Aᵤ = (V_r + 2 * K_Q * Q) / (2 * τ_Q * V_r)
    Bᵤ = 1im 
    Cᵤ = - 1 / (2 * τ_Q * V_r^2)
    Gᵤ = 0
    Hᵤ = - K_Q / (τ_Q * V_r)
    Mₓ = τ_P
    Aₓ = K_P * P 
    Bₓ = - 1 
    Cₓ = 0
    Gₓ = - K_P 
    Hₓ = 0
    @assert isreal(Mₓ) && Mₓ > 0
    @assert isreal(Aₓ)
    @assert isreal(Bₓ)
    @assert isreal(Cₓ)
    @assert isreal(Gₓ)
    @assert isreal(Hₓ)
end [[ω, dω]] begin
    s = u * conj(i)
    v2 = abs2(u)
    dω = ( Aₓ + Bₓ * ω + Cₓ * v2 + Gₓ * real(s) + Hₓ * imag(s) ) / Mₓ
    du = ( Aᵤ + Bᵤ * ω + Cᵤ * v2 + Gᵤ * real(s) + Hᵤ * imag(s) ) * u
end

# SwingEqLVS(;H, P, D, Ω, Γ, V)
# choose H=1/2, Ω=1, s.t. m= 2H/Ω = 1; D=α, V=1

"""
todo: write DynamicNode that takes the normal form parameters A...H as input

@DynamicNode phase_amplitude_oscillator(τ_P, τ_Q, K_P, K_Q, V_r, P, Q) begin
    Aᵤ = (V_r + 2 * K_Q * Q) / (2 * τ_Q * V_r)
    Bᵤ = 1im 
    Cᵤ = - 1 / (2 * τ_Q * V_r^2)
    Gᵤ = 0
    Hᵤ = - K_Q / (τ_Q * V_r)
    Mₓ = τ_P
    Aₓ = K_P * P 
    Bₓ = - 1 
    Cₓ = 0
    Gₓ = - K_P 
    Hₓ = 0
    @assert isreal(Mₓ) && Mₓ > 0
    @assert isreal(Aₓ)
    @assert isreal(Bₓ)
    @assert isreal(Cₓ)
    @assert isreal(Gₓ)
    @assert isreal(Hₓ)
end [[ω, dω]] begin
    s = u * conj(i)
    v2 = abs2(u)
    dω = ( Aₓ + Bₓ * ω + Cₓ * v2 + Gₓ * real(s) + Hₓ * imag(s) ) / Mₓ
    du = ( Aᵤ + Bᵤ * ω + Cᵤ * v2 + Gᵤ * real(s) + Hᵤ * imag(s) ) * u
end
"""